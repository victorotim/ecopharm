<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />


<table class='normal-table filter-container'>
<tr><td><input type='text' id='phrase' name='phrase' placeholder='Product title' data-final='phrase' value='<?php echo $this->native_session->get('catalog__phrase');?>' style='width:100%;'/></td></tr>


<tr><td><button type="button" id="applyfilterbtn" name="applyfilterbtn" class="btn blue" onClick="applyFilter('catalog')" style="width:100%;">Apply Filter</button>
<input name="layerid" id="layerid" type="hidden" value="" />
<?php 
if(!empty($listtype)){
	echo "<input name='listtype' id='listtype' data-final='listtype' type='hidden' value='".$listtype."' />";
}
if(!empty($t)){ 
	echo "<input name='area' id='area' data-final='area' type='hidden' value='".$t."' />";
}?>
</td></tr>
</table>
<?php echo minify_js('catalog__list_filter', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'jquery.datepick.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.fileform.js', 'ecopharm.pagination.js'));?>
