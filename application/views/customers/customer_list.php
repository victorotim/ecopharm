<?php 
$stopHtml = "<input name='paginationdiv__user_stop' id='paginationdiv__user_stop' type='hidden' value='1' />";
$listCount = count($list);
$i = 0;

echo "<table>";

if(!empty($list)){
	echo "<tr><th style='width:1%;'>&nbsp;</th><th>Name</th><th>Email</th><th>Telephone</th><th>Status</th><th>Order Count</th><th>Joined</th></tr>";


	foreach($list AS $row) {
		$i++;
		echo "<tr> 
		<td>".(!($this->native_session->get('__user_id') == $row['customer_id'])? "<input id='select_".$row['customer_id']."' name='selectall[]' type='checkbox' value='".$row['customer_id']."' class='bigcheckbox'><label for='select_".$row['customer_id']."'></label>": "&nbsp;")."</td>
		<td>"."<span class='edit-item btn' data-rel='users/add/d/".$row['customer_id']."'>".$row['first_name']." ".$row['last_name']."</span>"."</td>
		<td>".$row['email_address']."</td>
		<td>".$row['telephone']."</td>
		<td>".strtoupper($row['status'])."</td>
		<td>".strtoupper($row['num_of_orders'])."</td>
		<td>".date(FULL_DATE_FORMAT, strtotime($row['date_entered']));
		
		 # Check whether you need to stop the loading of the next pages
		if($i == $listCount && ((!empty($n) && $listCount < $n) || (empty($n) && $listCount < NUM_OF_ROWS_PER_PAGE))){
		 echo $stopHtml;
		}
		  echo "</td>
		</tr>";
	}
}

else {
	echo "<tr><td>".format_notice($this, 'WARNING: There are no training activities in this list.').$stopHtml."</td></tr>";
}

echo "</table>";
?>
