<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL;?>favicon.ico">
	<title><?php echo SITE_TITLE.': Change password';?></title>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.form.css" type="text/css" media="screen" />
</head>

<body>
<table class='body-table water-mark-bg'>
<?php 
$this->load->view('addons/secure_header', array('__page'=>'Change password' ));
$this->load->view('addons/'.$this->native_session->get('__user_type').'_top_menu', array('__page'=>'change_password' ));
$groupList = $this->native_session->get('__user_type').'groups';
?>

<tr>
  <td>&nbsp;</td>
  <td class='one-column body-form-area microform ignoreclear'>

<table>
    <tr><td class='label' width="1%">Current password</td><td><input type='password' id='current-password' name='current_password' placeholder='Enter your current passord' value=''/></td></tr>

    <tr><td class='label'>New password</td><td><input type='password' id='new-password' name='new_password' placeholder='Choose a new Password (Min 8 Characters with a number)' value=''/></td></tr>

    <tr><td class='label'>Confirm new password</td><td><input type='password' id='confirm-new-password' name='confirm_new_password' placeholder='Confirm the new password' value=''/></td></tr>

<tr><td>&nbsp;</td><td style="text-align:right; padding-right:0px;padding-top:30px; padding-bottom:20px;"><button type="button" id="save" name="save" class="btn green submitmicrobtn" style='width: calc(100% + 10px);'>Save</button>
    <input type='hidden' id='action' name='action' value='<?php echo base_url().'users/change_password';?>' />
    <input type='hidden' id='redirectaction' name='redirectaction' value='<?php echo base_url().'users/settings/view/Y';?>' />
    <input type='hidden' id='resultsdiv' name='resultsdiv' value='' />
</td></tr>
</table>







</td>
<td>&nbsp;</td>
</tr>

<?php $this->load->view('addons/secure_footer');?>

</table>
<?php echo minify_js('users__change_password', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.datepick.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
</html>