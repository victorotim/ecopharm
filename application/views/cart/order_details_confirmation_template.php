<table cellpadding="8" cellspacing="0" border="0" style="border:1px #CCCCCC solid">
	<tbody>
    	<?php
			if(!empty($order_details)):
				
				$order_total = 0;
				
				foreach($order_details as $order_detail):
					
					$order_total += ($order_detail['price'] * $order_detail['quantity']);
					
					$img_url = (!empty($order_detail['photo_url'])? BASE_URL .'assets/uploads/thumb_' . $order_detail['photo_url']: IMAGE_URL .'no-image-found.jpg');
		?>
		<tr>
        	<td>
            	<img style="width: 150px; height: auto" src="<?=$img_url?>" />
            </td>
            <td>
            	<p><?=$order_detail['product_title']?></p>
                <p><?=$order_detail['quantity'] .' x '. format_number($order_detail['price'])?>&nbsp;UgX</p>
            </td>
            <td align="right"><?=format_number($order_detail['price'] * $order_detail['quantity'])?>&nbsp;UgX</td>
        </tr>
        <?php
				endforeach;
			endif;
		?>
        <tr>
            <td colspan="2" align="right">Subtotal</td>
            <td align="right"><?=format_number($order_total)?>&nbsp;UgX</td>
        </tr>
        <tr>
            <td colspan="2" align="right">Delivery</td>
            <td align="right">2,000 UgX</td>
        </tr>
        <tr>
            <td colspan="2" align="right">
            	<b style="font-size:16px">Total</b>
            </td>
            <td align="right"><b style="font-size:16px"><?=format_number($order_total + 2000)?>&nbsp;UgX</b></td>
        </tr>
        <tr>
            <td colspan="3" align="left">
            	<b style="font-size:16px">Payment Method</b><br/><span><?=$order_summary['payment_method']?></span>
            </td>
        </tr>
	</tbody>
</table>