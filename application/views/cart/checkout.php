<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': '.(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li>Checkout</li>
                    </ul>
                    <h1>Checkout <span><i class="icofont-close-line"></i>Cancel</span></h1>
                    <? if(!empty($msg)):?>
                    <div class="row d-flex mb-4">
                    	<div class="col-md-12">
                        	<?=format_notice($this, $msg)?>
                        </div>
                    </div>
                    <? endif; ?>
                </div>
            </section>

            <section class="section-pad shop-cart">
                <div class="container">
                    <div class="row" id="checkout-form">
                        <article class="col-lg-9 col-md-12">
                        	<?php
								if(!empty($cart_items)):
									
									$total_amount = 0;
									
									foreach($cart_items as $cart_item):
										
										$img_url = (!empty($cart_item['photo_url'])? BASE_URL .'assets/uploads/' . $cart_item['photo_url']: IMAGE_URL .'no-image-found.jpg');
			
										$total_amount += ($cart_item['price'] * $cart_item['qty']);
							?>
                            <div class="row bg-white px-2 py-3 rounded" id="product-row-<?=encrypt_value($cart_item['product_id'])?>">
                                <div class="col-md-2 col-4 ">
                                    <div class="product-box">
                                        <img src="<?=$img_url?>">
                                    </div>
                                </div>
                                <div class="col-md-3 col-4 align-self-center">
                                    <h2 class="product-title"><?=$cart_item['product_title']?></h2>
                                    <h2 class="product-price d-block d-md-none"> Ush <?=format_number($cart_item['price'] * $cart_item['qty'])?></h2>
                                </div>
                                <div class="col-md-2 align-self-center d-none d-md-block">
                                    <h2 class="product-price"> Ush <?=format_number($cart_item['price'])?></h2>
                                </div>
                                <div class="col-md-2 col-4 align-self-center">
                                    <div class="product-quantity">
                                        <span class="dec qtybutton" role="button">-</span>
                                        <input type="text" value="<?=format_number($cart_item['qty'])?>">
                                        <span class="inc qtybutton" role="button">+</span>
                                    </div>    
                                </div>
                                <div class="col-md-2 d-none d-md-block align-self-center">
                                    <h2 class="product-price"> Ush <?=format_number($cart_item['price'] * $cart_item['qty'])?></h2>
                                </div>
                                <div class="col-md-1 align-self-center d-none d-md-block">
                                    <i class="icofont-close-line"></i>
                                </div>
                            </div>
                            <?php endforeach; ?>
                            
                            <div class="row bg-white px-2 py-3 rounded mb-4" id="checkout-totals">
                                <div class="col-md-7 col-4 ">
                                    &nbsp;
                                </div>
                                <div class="col-md-4 d-none d-md-block text-right">
                                    <h1 class="order-total" style="text-align:right"> <b>Ush <?=format_number($total_amount)?></b></h1>
                                </div>
                                <div class="col-md-1 align-self-center d-none d-md-block">
                                    &nbsp;
                                </div>
                            </div>   
                            
                            <div class="row bg-white px-2 py-3 rounded mb-4" id="delivery-address">
                                <div class="col-md-12 align-items-center">
                                    <form method="post" id="frm-delivery-address" action="<?=base_url().'cart/place_order'?>" class="simplevalidator">
                                    	<h4>Billing information</h4>
                                        <!-- 2 column grid layout with text inputs for the first and last names -->
                                        <div class="row mb-4">
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="first-name">First Name*</label>
                                                    <input type="text" id="first-name" name="first_name" class="form-control" value="<?=$this->native_session->get('__first_name')?>" placeholder="First Name" />
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="first-name">Last Name*</label>
                                                    <input type="text" id="last-name" name="last_name" class="form-control" value="<?=$this->native_session->get('__last_name')?>" placeholder="Last Name" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- email and mobile number input -->
                                        <div class="row mb-4">
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="telephone-number">Mobile Phone Number*</label>
                                                    <input type="text" id="telephone-number" name="telephone_number" class="form-control" value="<?=(!empty($formdata['telephone_number']))? $formdata['telephone_number'] : $this->native_session->get('__telephone')?>" placeholder="07XXXXXXXX" />
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-outline">
                                                    <label for="email-address">Email Address*</label>
                                                    <input type="text" id="email-address" name="email_address" class="form-control" value="<?=$this->native_session->get('__email_address')?>" placeholder="Email address" />
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Region and Town -->
                                        <div class="row mb-4">
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="region">Region*</label>
                                                    <select class="form-control" id="region" name="region">
                                                      	<?=get_option_list($this, 'regions')?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="select-town">Town*</label>
                                                    <select class="form-control" id="select-town" name="town">
                                                      <option selected value="">-Select Town-</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                      
                                        <!-- Address -->
                                        <div class="row mb-4">
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="select-town">Address*</label>
                                                    <textarea class="form-control" id="address-line-1" name="address_line_1" rows="4" placeholder="Address"><?=(!empty($formdata['address_line_1']))? $formdata['address_line_1'] : $this->native_session->get('__address_line_1')?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <!-- Additional info -->
                                        <div class="row mb-4">
                                            <div class="col">
                                                <div class="form-outline">
                                                	<label for="address-line-2">Additional Address Information</label>
                                                    <textarea class="form-control optional" id="address-line-2" name="address_line_2" rows="4" placeholder="Landmark/Directions/More details"><?=(!empty($formdata['address_line_2'])? $formdata['address_line_2'] : $this->native_session->get('__address_line_2'))?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <h4>Payment method</h4>
                                        <div class="row">
                                            <div class="col">
                                                <div class="d-block my-1">
                                                    <!--
                                                	<div class="custom-control custom-radio my-4 border-bottom border-1">
                                                      	<input id="mobile-money" name="payment_method" type="radio" value="mobile_money" class="custom-control-input">
                                                      	<label class="custom-control-label" for="mobile-money">
                                                            Mobile Money
                                                        </label>
                                                        <div class="momo_pay float-end">
                                                            <img src="<?=IMAGE_URL?>mtnmomo.svg" />
                                                        </div>
                                                        <p class="mx-lg-3">Pay with your MTN mobile money account. Please use number format as follows: 07xxxxxxxxx</p>
                                                        <div class="form-outline mb-3 w-75" style="display: none" id="momo-phone-number">
                                                            <input type="text" name="momo_phone_number" class="form-control" value="<?=(!empty($formdata['telephone_number']))? $formdata['telephone_number'] : $this->native_session->get('__telephone')?>" placeholder="Enter phone number here" />
                                                        </div>
                                                    </div>
                                                    -->
                                                    <div class="custom-control custom-radio">
                                                      	<input id="cash-on-delivery" name="payment_method" type="radio" value="cash_on_delivery" class="custom-control-input">
                                                      	<label class="custom-control-label" for="cash-on-delivery">Cash On Delivery</label>
                                                        <p class="mx-lg-3">Pay with cash on Delivery</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-2 mt-4">
                                            <div class="col-md-7 col-4 ">
                                                Subtotal
                                            </div>
                                            <div class="col-md-5 d-none d-md-block text-xl-end">
                                                <span>Ush <?=format_number($total_amount)?></span>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-md-7 col-4 ">
                                                Delivery fee
                                            </div>
                                            <div class="col-md-5 d-none d-md-block text-xl-end delivery_fee">
                                                <span><i>Calculated after selecting region and town</i></span>
                                            </div>
                                        </div>
                                        <div class="row mb-2">
                                            <div class="col-md-7 col-4 ">
                                                <b class="display-6">Total</b>
                                            </div>
                                            <div class="col-md-5 d-none d-md-block text-xl-end order_total">
                                                <span class="bold display-6">Ush <?=format_number($total_amount)?></span>
                                            </div>
                                        </div>
                                      
                                        <!-- Submit button -->
                                        <div class="pt-1 form-btn mb-4">
                                            <button type="submit" class="submit-btn submitbtn" id="place-order" name="place_order" value="place_order">
                                            	PLACE ORDER
                                            </button>
                                        </div>
                                      
                                      </form>
                                </div>
                            </div>                      
							
							<?php else:
									
									print format_notice($this,'WARNING: There are no items in your cart');
									
								endif;
							?>
                        </article>
                        
                        <aside class="col-lg-3 col-md-12">
                            <div class="sidebar order-summary">
                                <h3 class="sidebar-title">Order Summary</h3>
                                <?php if(!empty($cart_items)): ?>
                                <ul>
                                    <li class="summary_cost">Cost of Goods <span>ush <?=format_number($total_amount)?></span></li>
                                    <li class="summary_delivery_fee">Delivery fee<span><i style="font-size: small">Calculated from selected town</i></span></li>
                                    <li class="summary_total_cost">Total Cost <span>ush <?=format_number($total_amount)?></span></li>
                                </ul>
                                <?php endif; ?>
                            </div>
                        </aside>
                        <div class="shop-cart-btn d-flex d-lg-none">
                            <button class="continue-shop-btn"><a href="#">Continue Shopping</a> </button>
                            <button class="confirm-btn"><a href="confirmed.html">Confirm & Checkout</a> </button>
                        </div>
                        <div class="d-block d-lg-none cancel"> <a href="#"><i class="icofont-close-line"></i>Cancel Order</a></div>
                    </div>
                </div>
            </section>                 
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 	
 	<input type='hidden' id='layerid' name='layerid' value='' />
	<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
<?php if(!empty($towns)): ?>
    <script type="application/javascript">
        <?php foreach($towns as $town):?>
        towns.push({'id':<?=$town['id']?>, 'region_id': <?=$town['_region_id']?>, 'title': '<?=$town['title']?>'});
        <?php endforeach; ?>
    </script>
<?php endif;?>
</html>