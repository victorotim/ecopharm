<div class="modal-header">
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
</div>
<h2>Shopping Cart</h2>
<?php
	if(!empty($cart_items)):
		
		$total_amount = 0;
		
		foreach($cart_items as $cart_item):
			
			$img_url = (!empty($cart_item['photo_url'])? BASE_URL .'assets/uploads/' . $cart_item['photo_url']: IMAGE_URL .'no-image-found.jpg');
			
			$total_amount += ($cart_item['price'] * $cart_item['qty']);
?>
<div class="row " id="product-row-<?=encrypt_value($cart_item['product_id'])?>">
	<div class="col-md-4 col-6 ">
		<div class="product-box">
			<img src="<?=$img_url?>">
		</div>
	</div>
	<div class="col-md-8 col-6 align-self-center">
		<h4 class="product-price"> ush <?=format_number($cart_item['price'] * $cart_item['qty'])?></h4>
		<h3 class="product-title"><?=$cart_item['product_title']?></h3>
        <div class="product-quantity">
            <span class="dec qtybutton" role="button">-</span>
            <input id="qty-<?=encrypt_value($cart_item['product_id'])?>" name="qty" type="text" value="<?=$cart_item['qty']?>">
            <span class="inc qtybutton" role="button">+</span>
        </div>
		<p class="remove" role="button" id="item-<?=encrypt_value($cart_item['product_id'])?>"><i class="icofont-bin"></i> Remove</p>
	</div>
</div>
<?php
		endforeach;
	else:
?>
<div class="row ">
	<div class="col-md-8 col-6 align-self-center">
		<?=format_notice($this, 'WARNING: There no items in your shopping cart')?>
	</div>
</div>
<?php
	endif;
	
	if(!empty($cart_items)):
?>
<div class="modal-footer">
    <p class="total">Total:&nbsp;<span>ush <?=format_number($total_amount)?></span></p>
    <a href="<?=base_url()?>cart/checkout"><button type="button" class="checkout-btn">Checkout</button></a>
</div>
<?php endif?>