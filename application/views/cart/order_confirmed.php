<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': '.(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title confirmed">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li>Order Status</li>
                    </ul>
                    <h1>
                    	<?=$msg?>
                    </h1>
                </div>
            </section>

            <section class="order-details section-pad">
                <div class="container">
                    <div class="row bg-white rounded p-3">
                        <div class="col-md col-6">
                            <h3>Order Date</h3>
                            <p><?=date(FULL_DATE_FORMAT, strtotime($order_summary['dateadded']))?></p>
                        </div>
                        <div class="col-md col-6">
                            <h3>Order #</h3>
                            <p><?=$this->_util->encode($order_summary['order_id'])?></p>
                        </div>
                        <div class="col-md col-6">
                            <h3>Payment Method</h3>
                            <p><?=$order_summary['payment_method']?></p>
                        </div>
                        <div class="col-md col-6">
                            <h3>Delivery Address </h3>
                            <p><?=$order_summary['address']?></p>
                            <p><?=$order_summary['additional_address_info']?></p>
                        </div>
                        <div class="col-md d-none d-md-block">
                            <h3>Delivery Date</h3>
                            <?php
                                $dateadded_strtime = strtotime($order_summary['dateadded']);
                                $dateadded_hour = date('H', $dateadded_strtime);
                            ?>
                            <p><?=date('d M, ', $dateadded_strtime). ($dateadded_hour++) .' - '.($dateadded_hour+1)?></p>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section-pad shop-cart">
                <div class="container-fluid px-5">
                        <div class="row">
                        	<!--
                            <div class="row bg-white px-2 py-3 rounded">
                                <div class="col-md-2 col-4">
                                    <div class="product-box">
                                        <img src="img\products\Bernadryl.png">
                                    </div>
                                </div>
                                <div class="col-md-6 col-5 align-self-center">
                                    <h2 class="product-title">Benadryl Antihistamine</h2>
                                    <h2 class="product-price fw-normal d-block d-sm-none"> Ush 66 000 </h2>
                                </div>
                                <div class="col-md-2 col-3 align-self-center">
                                    <p>Qty: <span>2</span></p>    
                                </div>
                                <div class="col-md-2 d-none d-md-block align-self-center">
                                    <h2 class="product-price fw-normal"> Ush 66 000 </h2>
                                </div>
                            </div>
                            -->
                            <div class="shop-cart-btn justify-content-center">
                                <button class="continue-shop-btn"><a href="<?=BASE_URL?>">Continue Shopping</a> </button>
                            </div>
                        </div>
                </div>
            </section> 
            
            <!-- PRODUCT CATEGORY SECTION START -->
            <section class="section-pad bg-gray more-items">
                <div class="container">
                    <div class="section-title">
                        <h2>More items to Consider</h2>
                        <a href="#">See all</a>
                    </div>
                    <div class="row">
                    	<?php 
							if(!empty($catalog)): 
								
								foreach($catalog as $item):
									
									$img_url = (!empty($item['photo_url'])? BASE_URL .'assets/uploads/' . $item['photo_url']: IMAGE_URL .'no-image-found.jpg');
						?>
                        <div class="col-md-2 col-sm-4 col-6">
                            <a href="<?=base_url() .'pages/product_details/p/'. encrypt_value($item['product_id'])?>">
                                <div class="product-box">
                                    <img src="<?=$img_url?>">
                                    <h3 class="product-price">Ush <?=format_number($item['current_price'])?></h3>
                                    <p class="product-title"><?=$item['product_title']?>
                                        <span class="product-size">500g</span>
                                    </p>
                                </div>
                            </a>
                        </div>
                        <?php
								endforeach;
							endif;
						?>
                    </div>
                </div>
            </section>
            <!-- PRODUCT CATEGORY SECTION END -->
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 	
 	<input type='hidden' id='layerid' name='layerid' value='' />
	<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
</html>