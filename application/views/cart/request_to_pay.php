<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': Waiting for your payment'?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper" id="request-to-pay">
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title alert-info">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li>Order Status</li>
                    </ul>
                    <h1>
                    	<?=$msg?>
                    </h1>
                </div>
            </section>

            <section class="order-details section-pad">
                <div class="container">
                    <div class="row bg-white rounded p-3 d-flex justify-content-center waiting_for_you_info">
                        <?php if (!empty($result) && $result):?>
                            <div class="loader-demo-box">
                                <div class="jumping-dots-loader"> <span></span> <span></span> <span></span> </div>
                            </div>

                            <h5>We are waiting for you</h5>
                            <p class="text-muted">Please follow the instructions and do not refresh or leave this page.<br/>This may take up to 2 minutes</p>
                            <p>
                                If you do not receive a USSD prompt, follow these instructions to complete you payment:
                                <ol style="margin-left: 2%">
                                    <li>Dial <b>*165*8*2</b> to see the pending payment on MTN USSD menu.</li>
                                    <li>Enter your MTN Mobile Money PIN to confirm</li>
                                    <li>If your payment is successful, you will be redirected to the confirmation page</li>
                                </ol>
                            </p>

                        <?php endif;?>
                    </div>
                </div>
            </section>

            <section class="section-pad shop-cart">
                <div class="container-fluid px-5">
                        <div class="row">
                        	<!--
                            <div class="row bg-white px-2 py-3 rounded">
                                <div class="col-md-2 col-4">
                                    <div class="product-box">
                                        <img src="img\products\Bernadryl.png">
                                    </div>
                                </div>
                                <div class="col-md-6 col-5 align-self-center">
                                    <h2 class="product-title">Benadryl Antihistamine</h2>
                                    <h2 class="product-price fw-normal d-block d-sm-none"> Ush 66 000 </h2>
                                </div>
                                <div class="col-md-2 col-3 align-self-center">
                                    <p>Qty: <span>2</span></p>    
                                </div>
                                <div class="col-md-2 d-none d-md-block align-self-center">
                                    <h2 class="product-price fw-normal"> Ush 66 000 </h2>
                                </div>
                            </div>
                            -->
                        </div>
                </div>
            </section> 

        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 	
 	<input type='hidden' id='layerid' name='layerid' value='' />
	<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
</html>