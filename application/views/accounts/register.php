<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': Login'?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg" class="vh-100">
	<div class="page-wraper h-100"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?>                        		
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <!-- LOGIN SECTION START -->
            <section class="section-pad">
                <div class="container">
                	<div class="section-title">
                        <h2>Create an account</h2>
                    </div>
                    <? if(!empty($msg)):?>
                    <div class="row d-flex mb-4">
                    	<div class="col-md-12">
                        	<?=format_notice($this, $msg)?>
                        </div>
                    </div>
                    <? endif; ?>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-12 align-items-center">
                        	<form method="post" id="create-account" class="simplevalidator" action="<?=base_url().'accounts/register'. ((!empty($src) && $src=='cart')?'/src/cart':'')?>" enctype="multipart/form-data">
                                <!-- 2 column grid layout with text inputs for the first and last names -->
                            	<div class="row mb-4">
                                	<div class="col">
                                    	<div class="form-outline">
                                      		<input type="text" id="first-name" name="firstname" class="form-control" value="<?=(!empty($formdata['firstname'])? $formdata['firstname'] :'')?>" placeholder="First Name" />
                                    	</div>
                                  	</div>
                                  	<div class="col">
                                    	<div class="form-outline">
                                      		<input type="text" id="last-name" name="lastname" class="form-control" value="<?=(!empty($formdata['lastname'])? $formdata['lastname'] :'')?>" placeholder="Last Name" />
                                    	</div>
                                  	</div>
                                </div>
                                
                                <!-- email and mobile number input -->
                                <div class="row mb-4">
                                	<div class="col">
                                    	<div class="form-outline">
                                      		<input type="email" id="email" name="emailaddress" class="form-control" value="<?=(!empty($formdata['emailaddress'])? $formdata['emailaddress'] :'')?>" placeholder="Email" />
                                    	</div>
                                  	</div>
                                  	<div class="col">
                                    	<div class="form-outline input-group">
                                      		<input type="text" id="zip-code" name="zip_code" class="form-control input-group-prepend" placeholder="256" value="<?=(!empty($formdata['zip_code'])? $formdata['zip_code'] :'+256')?>" />
                                            <input type="number" id="telephone-number" name="telephone" class="form-control" value="<?=(!empty($formdata['telephone'])? $formdata['telephone'] :'')?>"  placeholder="Mobile number e.g 0782345985"/>
                                    	</div>
                                  	</div>
                                </div>
                              
                                <!-- Password input -->
                                <div class="row mb-4">
                                	<div class="col">
                                    	<div class="form-outline">
                                      		<input type="password" id="password" name="password" class="form-control" value="<?=(!empty($formdata['password'])? $formdata['password'] :'')?>" placeholder="Password" />
                                    	</div>
                                  	</div>
                                  	<div class="col">
                                    	<div class="form-outline">
                                      		<input type="password" id="repeat-password" name="newpassword" class="form-control" value="<?=(!empty($formdata['newpassword'])? $formdata['newpassword'] :'')?>" placeholder="Repeat password" />
                                    	</div>
                                  	</div>
                                </div>
                              
                                <!-- Submit button -->
                                <div class="pt-1 form-btn mb-4">
                                    <div class="col">
                                        <input type="checkbox" id="check-terms-conditions" name="check_terms_conditions" class="-check required_check" />
                                        <label for="check-terms-conditions" id="label-terms-conditions" style="font-weight: normal">
                                            I have read the Ecopharm <a href="javascript:void(0);"><b>privacy policy</b></a>
                                        </label>
                                    </div>
                                    <div class="col">
                                        <button type="submit" class="submit-btn submitbtn" name="create_account" value="create_account">Create account</button>
                                    </div>
                                </div>

                                <div id="privacy-policy-text" style="display: none">
                                    <?php $this->load->view('pages/privacy_policy'); ?>
                                </div>
                              
                              </form>
                        </div>                        
                    </div>
                </div>
            </section>
            <!-- LOGIN SECTION END -->              
        </div>
        <!-- CONTENT END -->      
    </div>
    <?=$this->load->view('addons/checkout_modal')?>
 	<input type='hidden' id='layerid' name='layerid' value='' />
	<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
</html>