<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': Login'?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg" class="vh-100">
	<div class="page-wraper h-100"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?>                        		
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <!-- LOGIN SECTION START -->
            <section class="section-pad">
                <div class="container">
                	<div class="section-title">
                        <h2>Welcome, please sign into your account</h2>
                    </div>
                    
					<? if(!empty($msg)):?>
                    <div class="row d-flex mb-4">
                    	<div class="col-md-12">
                        	<?=format_notice($this, $msg)?>
                        </div>
                    </div>
                    <? endif; ?>
                    
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6 align-items-center">
                        	<form method="post" class="w-75">
                                <h4 class="mb-4">Returning Customer</h4>
                                <div class="form-outline mb-4">
                                  <input type="email" id="loginusername" name="loginusername" autocapitalize='off' class="form-control form-control-lg" placeholder="Email address" />
                                </div>
              
                                <div class="form-outline mb-4">
                                  <input type="password" id="loginpassword" name="loginpassword" autocapitalize='off'  class="form-control form-control-lg" placeholder="Password" />
                                	<?=(!empty($src) && $src == 'cart'? '<input type="hidden" id="loginsrc" name="loginsrc" value="cart" />':'')?>
                                </div>
              
                                <div class="pt-1 form-btn mb-4">
                                  <button id="submitlogin" class="submit-btn" type="button">Login</button>
                                </div>
              
                                <a class="small text-muted" href="<?=base_url()?>accounts/forgot">Forgot password?</a>
                          	</form>
                        </div>
                        <div class="col-md-6 align-items-center text-center">
                            <h4 style="text-align: center">New Customer</h4>
                            <div class="icofont-pen display-6">&nbsp;</div>
                            <p class="mb-5 pb-lg-2">
                                By creating an account on our website, you will be able to shop faster, be up to date on an order status and keep track of the orders you have previously made.
                            </p>
                            <div class="form-btn d-block">
                                <button class="submit-btn position-relative" type="button">
                                    Create Account
                                    <a class="stretched-link" href="<?=base_url().'accounts/register'. ((!empty($src) && $src=='cart')?'/src/cart':'')?>">&nbsp;</a>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- LOGIN SECTION END -->              
        </div>
        <!-- CONTENT END -->     
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 
 	<input type='hidden' id='layerid' name='layerid' value='' />
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.js', 'ecopharm.fileform.js'));?>
</body>
</html>