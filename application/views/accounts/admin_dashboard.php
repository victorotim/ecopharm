<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL;?>favicon.ico">
	<title><?php echo SITE_TITLE.': Dashboard';?></title>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.list.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.pagination.css" type="text/css" media="screen" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,300;0,400;0,600;1,100;1,600&display=swap" rel="stylesheet">
</head>

<body>
<table class='body-table water-mark-bg'>
<?php 
$this->load->view('addons/secure_header', array('__page'=>'Dashboard'));
$this->load->view('addons/'.$this->native_session->get('__user_type').'_top_menu', array('__page'=>'my_dashboard'));
?>

<tr>
  	<td>&nbsp;</td>
  	<td class='one-column fill-page admin_dashboard' style="vertical-align: top">
    	<div class="left_panel">
        	<div class="form_section">
                <div class="form_section_paddings">
                    <h3>To do list</h3>
                    <ul class="todo">
                        <?php
    
                            # Orders requiring processing
                            if(!empty($store_statistics['num_of_awaiting_processing'])):
                                
                                print '<li class="btn" data-rel="orders/manage/q/awaiting_processing">';
                                
                                print $store_statistics['num_of_awaiting_processing'] .' order'.($store_statistics['num_of_awaiting_processing']>1? 's need ' : ' needs ') .'processing';
                                
                                print '</li>';
                            
                            endif;
    
                            # Orders awaiting delivery
                            if(!empty($store_statistics['num_of_awaiting_delivery'])):
    
                                print '<li class="btn" data-rel="orders/manage/q/awaiting_delivery">';
    
                                print $store_statistics['num_of_awaiting_delivery'] .' order'.($store_statistics['num_of_awaiting_delivery']>1? 's need ' : ' needs ') .' to be delivered';
    
                                print '</li>';
    
                            endif;
    
                            # Orders in transit
                            if(!empty($store_statistics['num_of_dispatched'])):
    
                                print '<li class="btn" data-rel="orders/manage/q/dispatched">';
    
                                print $store_statistics['num_of_dispatched'] .' order'.($store_statistics['num_of_dispatched']>1? 's are ' : ' is ') .' still in transit';
    
                                print '</li>';
    
                            endif;
                        ?>
                    </ul>
                </div>
            </div>
            
            
        	<div class="form_section">
                <div class="form_section_paddings">
                    <h3>Top products</h3>
                    <div class="top_products">
                    <?php if(!empty($top_products)): ?>
                        <table cellpadding="8" cellspacing="0">							
                            <tbody>
                    <?php for($row = 0; $row<5 && $row<count($top_products); $row++):
                                
                                $top_product_row = $top_products[$row];
                                
                                $img_url = (!empty($top_product_row['photo_url'])? BASE_URL .'assets/uploads/thumb_'. $top_product_row['photo_url']: IMAGE_URL .'no-image-found.jpg');
                    ?>
                        <tr>
                            <td><div class="top_product_photo_wrap"><img alt="<?=$top_product_row['product_title']?>" src="<?=$img_url?>" /></div></td>
                            <td><?=$top_product_row['product_title']?></td>
                            <td><?=format_number($top_product_row['total_qty_ordered'])?></td>
                        </tr>
                    <?php endfor; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                    </div>
            	</div>
        	</div>
        </div>
        <div class="right_panel">
        	<div class="form_section">
                <div class="form_section_paddings" style="padding: 1% 2%">
                    <h2>Store Overview</h2>
                    <div>
                        <span>Last order</span>
                        <span>
                            <?php
                                if(!empty($latest_order)):
                                    $order_no = $this->_util->encode($latest_order['order_id']);
                                    $last_order_date = date_create($latest_order['dateadded']);
                                    date_add($last_order_date, date_interval_create_from_date_string('10 hours'));
                                    date_format($last_order_date, 'd M, Y');
                                    $date_interval_array =  format_date_interval($latest_order['dateadded'], '');
                                    $interval_text = 'on '. date_format($last_order_date, 'd M, Y') .' at '. date_format($last_order_date, 'H:i');

                                    print $interval_text . ', order#: <a href="'. base_url().'orders/details/d/'. encrypt_value($latest_order['order_id']) .'" class="shadowbox"><b>'. $order_no .'</b></a>';
    
                                else:
                                    print 'NONE';
                                endif;
                            ?>
                        </span>
    
                    </div>
                    <ul class="store_overview_stats">
                        <li class="stats_orders">
                            <div class="stat_header">Orders</div>
                            <div class="stat_value"><?=format_number($store_statistics['num_of_orders'], 0)?> <span>(<?=format_number($store_statistics['order_total'], 0)?>)</span></div>
                        </li>
                        <li class="stats_fulfilled_orders">
                            <div class="stat_header">Fulfilled Orders</div>
                            <div class="stat_value"><?=format_number($store_statistics['num_of_delivered'], 0)?> <span>(<?=format_number($store_statistics['delivered_order_total'], 0)?>)</span></div>
                        </li>
    
                        <li class="stats_products">
                            <div class="stat_header">Products</div>
                            <div class="stat_value"><?=format_number(count($active_products), 0)?></div>
                        </li>
                        <li class="stats_customers">
                            <div class="stat_header">Customers</div>
                            <div class="stat_value"><?=format_number(count($customers), 0)?></div>
                        </li>
                    </ul>
                    <div>
                        <h3>Monthly stats</h3>
                        <div class="montly_stats">
                            <?php
                                $months_str_arr = array();
                                $months_str = '';
                                $num_of_orders_arr = array();
                                $num_of_deliveries_arr = array();
                                $num_of_orders_str = '';
                                $num_of_delivered_orders_str = '';
                                if(!empty($monthly_statistics)):

                                    foreach ($monthly_statistics as $monthly_statistic):

                                        $num_of_orders_arr[] = $monthly_statistic['num_of_orders'];

                                        $months_str_arr[$monthly_statistic['order_month'].'_'.$monthly_statistic['order_year']] = date('M', mktime(0, 0, 0, $monthly_statistic['order_month'], 10)).', '.$monthly_statistic['order_year'].'\'';

                                    endforeach;

                                endif;

                                if(!empty($monthly_delivery_statistics)):

                                    foreach ($monthly_delivery_statistics as $monthly_delivery_statistic):

                                        $num_of_deliveries_arr[] = $monthly_delivery_statistic['num_of_delivered'];

                                        $months_str_arr[$monthly_delivery_statistic['fulfilment_status_month'].'_'.$monthly_delivery_statistic['fulfilment_status_year']] = date('M', mktime(0, 0, 0, $monthly_delivery_statistic['fulfilment_status_month'], 10)).', '.$monthly_delivery_statistic['fulfilment_status_year'].'\'';

                                    endforeach;

                                endif;
                                $num_of_delivered_orders_str = implode(',', $num_of_deliveries_arr) ;
                                $num_of_orders_str = implode(',', $num_of_orders_arr);
                                $months_str = '\''. implode(',\'', $months_str_arr);
                            ?>
                            <div id="bar-chart" style="width: 100%; height: auto; margin: 0 auto"></div>
                        </div>
                    </div>
                    <div>
                        <h3>Recent orders</h3>
                        <div class="recent_orders">
                        <?php if(!empty($recent_orders)): ?>
                            <table cellpadding="8" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Order No.</th>
                                        <th>Date</th>
                                        <th>Customer</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th style="text-align:center">View order</th>
                                    </tr>
                                </thead>
                                <tbody>
                        <?php for($row = 0; $row<5 && $row<count($recent_orders); $row++):
                                    
                                    $recent_order_row = $recent_orders[$row];
                        ?>
                            <tr>
                                <td><?=$this->_util->encode($recent_order_row['order_id'])?></td>
                                <td><?=date('M d, Y', strtotime($recent_order_row['dateadded']))?></td>
                                <td><?=$recent_order_row['customer_first_name'] .' '.$recent_order_row['customer_last_name']?></td>
                                <td><?=format_number($recent_order_row['order_total'])?></td>
                                <td><div class="order_status <?=str_replace(' ', '_', $recent_order_row['fulfillment_status'])?>"><?=ucwords($recent_order_row['fulfillment_status'])?></div></td>
                                <td align="center"><a href="<?=base_url() .'orders/details/d/'. encrypt_value($recent_order_row['order_id'])?>" class="shadowbox view_details"><img style="width: 30px; height:auto" src="<?=IMAGE_URL?>view.png" /></a></td>
                            </tr>
                        <?php endfor; ?>
                                </tbody>
                            </table>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
	</td>
	<td>&nbsp;</td>
</tr>

<?php $this->load->view('addons/secure_footer');?>

</table>
<?php echo minify_js('accounts__admin_dashboard', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.pagination.js', 'ecopharm.fileform.js', 'highcharts/js/highcharts.js', 'highcharts/js/modules/exporting.js'));?>

<script type="application/javascript">
    $(function() {
        $(document).find('.filter-list').last().click();
    });

    $(function () {
        var chart;
        $(document).ready(function() {

            var colors = Highcharts.getOptions().colors,
                categories = [<?=$months_str?>],
                name = 'Months',
                ordersData = [<?=$num_of_orders_str?>],
                deliveredData = [<?=$num_of_delivered_orders_str?>];


            chart = new Highcharts.Chart({
                chart: {
                    renderTo: 'bar-chart',
                    type: 'column'
                },
                plotOptions: {
                    series: {
                        shadow: false,
                        pointWidth: 8,
                        pointMargin:0
                    },
                    column: {
                        pointPadding: 0,
                        borderWidth: 0
                    }
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: categories,
                    labels: {enabled: true},
                    minPadding: 2,
                    maxPadding: 2,
                    title: {
                        enabled: true,
                        text: '<b>Months</b>',
                        style: {
                            fontWeight: 'normal'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: '<?='Number of Orders'?>'
                    }
                },
                tooltip: {
                    formatter: function() {
                        var point = this.point,
                            s = this.x +'<br/>'+this.series.name+':<b>'+ (this.y).toLocaleString() +'</b><br/>';
                        return s;
                    }
                },
                legend: {
                    enabled: false
                },
                series: [{
                    data: ordersData,
                    name: 'Number of Orders',
                    color: '#FABB57'
                },  {
                        data: deliveredData,
                        name: 'Delivered Orders',
                        color: '#2A875C'
                    }],
                exporting: {
                    enabled: true
                }
            });
        });

    });
</script>
</body>
</html>