<?php if(!empty($catalog_item)): ?>

<?php $product_img_url = (!empty($catalog_item['photo_url'])? BASE_URL .'assets/uploads/thumb_' . $catalog_item['photo_url']: IMAGE_URL .'no-image-found.jpg'); ?>

<div class="col-md-<?=(!empty($col_md)? $col_md : 2)?> col-sm-4 col-6 position-relative">
    <?php if($catalog_item['stock_status'] == 'out_of_stock'): ?>
        <div class="position-absolute p-1 w-auto out_of_stock">out of stock</div>
    <?php endif; ?>
    <a href="<?=base_url() .'product/'. $catalog_item['slug']?>">
        <div class="product-box">
            <img alt="<?=$catalog_item['product_title']?>" src="<?=$product_img_url?>">
            <h3 class="product-price">Ush <?=format_number($catalog_item['current_price'], 9, 0)?></h3>
            <p class="product-title"><?=$catalog_item['product_title']?>
                <span class="product-size"><?=$catalog_item['quantity_per_unit']?></span>
            </p>
        </div>
    </a>
</div>

<?php endif; ?>