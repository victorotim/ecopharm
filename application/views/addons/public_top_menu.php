<div class="col-md-2 col-5  align-self-center ">
    <a class="navbar-brand" href="<?=base_url()?>">
        <img src="<?=IMAGE_URL?>ecopharm_logo.jpg" class="img-fluid" alt="Ecopharm Uganda logo" />
    </a>
</div>
<div class="col-md-10 col-7 p-0 p-md-2">
    <ul class="navbar-nav">
        <li class="nav-item search-form" style="position: relative">
            <form class="card card-sm" action="<?=base_url().'pages/search_catalog'?>" method="post">
                <div class="card-body row no-gutters align-items-center" style="padding: 0 0.5rem 0 0.5rem">
                    <div class="col-auto">
                        <i class="fas icofont-search-1 h4 text-body"></i>
                    </div>
                    <!--end of col-->
                    <div class="col"  style="padding: 0">
                        <input name="search_term" class="form-control form-control-lg form-control-plaintext shadow-none" id="search-catalog" type="search" placeholder="Search..." />
                    </div>
                    <!--end of col-->
                    <div class="col-auto">
                        <button class="btn btn-primary" id="btn-search-catalog" type="submit" name="search_btn" value="search_catalog">Search</button>
                    </div>
                    <!--end of col-->
                </div>
            </form>
            <ul id="search-results" class="shadow p-3 mb-5 bg-white rounded w-100 list-unstyled" style="display: none">

            </ul>
        </li>
        <li class="nav-item d-none d-md-block align-self-center ">
            <a class="nav-link  align-middle" href="<?=BASE_URL?>">Shop</a>
        </li>
        <li class="nav-item d-none d-md-block align-self-center ">
            <a class="nav-link" href="<?=base_url().'pages/about'?>">About</a>
        </li>                            
        <li class="nav-item d-none d-md-block align-self-center ">
            <a class="nav-link" href="<?=base_url().'pages/contact_us'?>">Contact Us</a>
        </li>
        <li class="nav-item align-self-center ">
            <a class="nav-link " href="<?=base_url().'pages/prescription'?>">
                <i class="icofont-capsule me-2 d-inline d-md-inline"></i> 
                <span class="d-none d-md-inline">Prescription</span>
            </a>
        </li>
        <li class="nav-item align-self-center ">
            <a class="nav-link view-cart" href="#" data-bs-toggle="modal" data-bs-target="#checkout-modal">
            	<i class="icofont-shopping-cart"></i>
                <?=($this->native_session->get('__cart')? '<span class="cart-count">'. count($this->native_session->get('__cart')) .'</span>' : '')?>
          	</a>
        </li>
    </ul>
</div>