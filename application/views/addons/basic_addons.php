<?php
$jquery = "<script src='".base_url()."assets/js/jquery.min.js' type='text/javascript'></script>";
$javascript = "<script type='text/javascript' src='".base_url()."assets/js/ecopharm.js'></script>".get_ajax_constructor(TRUE);
$tableHTML = "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.css' type='text/css' media='screen' />";
if(!empty($area) && $area == "dropdown_list")
{
	$tableHTML .= !empty($list)? $list: "";
}

else if(!empty($area) && $area == "basic_msg")
{
	$tableHTML .= format_notice($this, $msg);
}

else if(!empty($area) && $area == "json_msg")
{
	$tableHTML = json_encode($json_data);
}

else if(!empty($area) && $area == "product_photo")
{
	$tableHTML .= '<div style="text-align:center"><img src="'. BASE_URL .'assets/uploads/'. $photo_url .'" /></div>';
}

else if(!empty($area) && $area == "catalog_search_results")
{
    $tableHTML = '';

    if(!empty($list)):

        foreach($list as $list_item):

            $item_lnk = ($list_item['item_type'] == 'product_category'? 'product_category/': 'product/') . $list_item['slug'];

            $tableHTML .= "<li class=\"p-2 position-relative\"><a href='". base_url(). $item_lnk ."' class='stretched-link'>&nbsp;</a> {$list_item['item_title']}</li>";

        endforeach;

    else:

        $tableHTML .='<li class="p-2">No items match your search criteria</li>';

    endif;

}


else if(!empty($area) && $area == "product_category_items")
{
    $tableHTML = '';

    if (!empty($category_products)):

        foreach ($category_products as $category_product):

            $this->load->view('addons/public_product_widget', array('catalog_item' => $category_product, 'col_md' => 3));

        endforeach;

    else:

        $tableHTML .='<li class="p-2">No items to show</li>';

    endif;

}

else if(!empty($area) && $area == "refresh_list_msg")
{
	$tableHTML .= format_notice($this, $msg)."<br><br>
	<button type='button' id='refreshlistfromiframe' name='refreshlistfromiframe' class='btn blue' style='width:100%;' onclick='parent.location.reload();'>Refresh List</button>";
}


echo $tableHTML;
?>