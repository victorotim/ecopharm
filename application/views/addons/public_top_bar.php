<div class="container">
    <div class="row justify-content-between">
            <ul class="col-lg-6 col-md-7 col-sm-7 col-10 topbar-left">
                <li>Need Help?<span>Call support 256 706 900800</span></li>
                <li class="d-none d-md-block">info@ecopharmug.com</li>
            </ul>
            <ul class="col-lg-3 col-md-4 col-sm-5 col-12 topbar-right">
                <li class=""><a href="#">Store Locator</a></li>
                <?php if($this->native_session->get('__user_id')): ?>
                <li class=""><a href="<?=base_url().$this->native_session->get('__default_view')?>">Hi, <?=$this->native_session->get('__first_name')?></a></li>
                <?php else: ?>
                <li class=""><a href="<?=base_url().'accounts/login'?>">Login</a></li>
                <li class=""><a href="<?=base_url().'accounts/register'?>">Register</a></li>                
                <?php endif; ?>
            </ul>
    </div>
</div>