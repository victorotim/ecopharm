<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL;?>favicon.ico">
	<title><?php echo SITE_TITLE.': New category';?></title>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.list.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />
</head>

<body>
<table class='body-table water-mark-bg'>
<?php 
$this->load->view('addons/secure_header', array('__page'=>'New category' ));
$this->load->view('addons/'.$this->native_session->get('__user_type').'_top_menu', array('__page'=>('catalog') ));

if($this->native_session->get('__user_type') != 'provider'){
?>
<tr>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
</tr>
<?php $this->load->view('addons/catalog_ribbon', array('page'=>'categories')); 
}
?>


<tr>
	<td>&nbsp;</td>
  	<td class='one-column body-form-area microform ignoreclear'>
		<table>
			<tr>
				<td width="50%">
                	<div class="form_section">                    
                    	<div class="form_section_paddings">
                    		<div class="label">Category title</div>
							<input type='text' id='category-title' name='category_title' placeholder='Category title' class='' value='<?=(!empty($formdata['category_title'])? $formdata['category_title']: '')?>'/>
							<div class="label">Description</div>
							<textarea id='category_description' name='category_description' placeholder='What is this category about? Give customers a simple and clear idea about what they will find' class='limit-chars' data-max='500' style='height: 150px;'><?php echo (!empty($formdata['description'])? $formdata['description']: '');?></textarea>
                            <div class="label">Parent category</div>
                            <select id='parent-category' name='parent_category' class='drop-down optional' style="width:calc(100% + 17px);">
                                <?php echo get_option_list($this, 'product_categories', 'select', '', array('selected'=>(!empty($formdata['parent_category_id'])? $formdata['parent_category_id']: '') ));?>
                            </select>
                            <div class="label">Show on home banner?</div>
                            <label class="container yes">Yes
                                <input name="banner_item" id="is-banner-item" value="Y" type="radio" <?=(!empty($formdata['show_on_banner']) && $formdata['show_on_banner'] == 'YES')? 'checked' : ''?> />
                                <span class="checkmark"></span>
                            </label>
                            <label class="container no">No
                                <input name="banner_item" id="not-banner-item" value="N" type="radio" <?=(!empty($formdata['show_on_banner']) && $formdata['show_on_banner'] == 'NO')? 'checked' : ''?> />
                                <span class="checkmark"></span>
                            </label>
                    	</div>
                    </div>
                    <div class="form_section product_photos">           
                    	
      				</div>
              	</td>
				<td rowspan="2">
                	<div class="form_section">                    	    
                    	<div class="form_section_paddings">
                        	<div class="manage_product_photos" id="manage-product-photos">       	
								<?php
                                    if(!empty($formdata['photo_url'])):                                                                           
                                        
										print '<div id="item_'. $formdata['photo_url'] .'">'.
											  '<div class="delete_photo"></div>'.
											  '<div class="view_photo"><a href="'. base_url() .'catalog/view_product_photo/p/'. $formdata['photo_url'] .'" class="shadowbox">View</a></div>'.
											  '<img src="'. BASE_URL .'assets/uploads/'. $formdata['photo_url'] .'" /></div>';                                        
                                        
                                    endif;
                                ?>                            
                            </div>
                            <table class='default-table' style="width:calc(100% + 18px);">
                                <tr>
                                    <td style='width:99%;'>
                                        <input type='text' id='product-photo' name='category_photo' class='filefield optional' data-resultdiv='manage-product-photos' data-actionurl='categories/upload_photo' data-val='jpg,png,jpeg' data-size='5120000' placeholder='OPTIONAL: Select category image' value=''/>                    	               
                                    </td>
                                </tr>
                            </table>
                    	</div>
                    </div>
                </td>
			</tr>
			<tr>
    			
         	</tr>      
      		<tr>
                <td colspan="2" style="text-align:right; padding-right:0px;padding-top:30px; padding-bottom:20px;">
                	<button type="button" id="save" name="save" class="btn green submitmicrobtn" style='width: calc(100% + 10px);'>Save</button>
          			<input type='hidden' id='action' name='action' value='<?=base_url().'categories/add'?>' />
          			<input type='hidden' id='redirectaction' name='redirectaction' value='<?=base_url().'categories/manage'?>' />
          			<input type='hidden' id='resultsdiv' name='resultsdiv' value='' />
          			<?=(!empty($p)? "<input type='hidden' id='category_id' name='categoryid' value='".$p."' />" : "")?>
          		</td>
       		</tr>
      </table>
</td>
<td>&nbsp;</td>
</tr>

<?php $this->load->view('addons/secure_footer');?>

</table>
<?php echo minify_js('categories__new_category', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.pagination.js', 'ecopharm.fileform.js'));?>
<script type="application/javascript">
	
</script>
</body>
</html>