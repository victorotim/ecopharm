<?php

echo "<table id='product-category-list'>" .
    "<tr>" .
    "<th colspan='2'>Category details</th>" .
    "<th>Number of products</th>".
    "<th>Parent category</th>" .
    "<th>Banner item</th>" .
    "<th>Last updated</th>" .
    "</tr><tbody>";

if(!empty($list)){
		
	foreach($list AS $row) {

        $last_updated = date_create($row['last_updated']);
        date_add($last_updated, date_interval_create_from_date_string('10 hours'));
		
		$img_url = (!empty($row['photo_url'])? BASE_URL .'assets/uploads/'. $row['photo_url']: IMAGE_URL .'no-image-found.jpg');
		
		$published_status = '<img src="'. IMAGE_URL .($row['isactive'] == 'Y'? 'correct.png" /><span class=""><i>activated</i></span>' : 'wrong.png" /><span class=""><i>deactivated</i></span>');
		
		echo "<tr>".
			 "<td width='1%' style='vertical-align: middle'><input id='select_".$row['category_id']."' name='selectall[]' type='checkbox' value='".$row['category_id']."' class='bigcheckbox'><label for='select_".$row['category_id']."'></label></td>".
			 "<td  style='vertical-align: top' nowrap>".
			 "<div class='product_photo'><a href='".base_url()."categories/add/p/". encrypt_value($row['category_id']) ."'><img class='thumb' src='". $img_url ."' /></a></div>".
			 "<div class='product_basic_details'>".
			 "<div class='product_title'>". $row['category_title'] ."</div>".			 
			 "<div class='product_status'><div class='on_status'>". $published_status ."</div></div>".
			 "</div>".
			 "</td>".
             "<td>{$row['num_of_products']}</td>".
             "<td>{$row['parent_category_title']}</td>".
             "<td>{$row['show_on_banner']}</td>".
             "<td>".date_format($last_updated, 'd M, Y H:i')  ." by {$row['updater']}</td>".
             "</tr>";
	}
}

else {
	echo "<tr><td>".format_notice($this, 'WARNING: There are no products in this list.')."</td></tr>";
}
echo "</tbody></table>";
?>