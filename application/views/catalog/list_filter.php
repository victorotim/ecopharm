<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />


<table class='normal-table filter-container'>
    <tr>
        <td colspan="2"><input type='text' id='phrase' name='phrase' placeholder='Product title' data-final='phrase'
                   value='<?php echo $this->native_session->get('catalog__phrase'); ?>' style='width:100%;'/>
        </td>
    </tr>
    <tr>
        <td colspan="2"><input type='text' id='product-description' name='product-description' placeholder='Product description' data-final='product_description'
                   value='<?php echo $this->native_session->get('catalog__product_description'); ?>' style='width:100%;'/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='product-category' name='product_category' class='drop-down' data-final='product_category' style="width:calc(100% - 17px);">
                <?php echo get_option_list($this, 'product_categories', '', '', array('selected'=>$this->native_session->get('catalog__product_category')));?>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2"><input type='text' id='quantity-per-unit' name='phrase' placeholder='Quantity per unit' data-final='quantity_per_unit'
                   value='<?php echo $this->native_session->get('catalog__quantity_per_unit'); ?>' style='width:100%;'/>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='stock-status' name='stock_status' class='drop-down' data-final='stock_status' style="width:calc(100% - 17px);">
                <option value="">-Stock status-</option>
                <option value="available" <?=$this->native_session->get('catalog__stock_status') == 'available'? 'selected' : ''?>>Available</option>
                <option value="out_of_stock" <?=$this->native_session->get('catalog__out_of_stock') == 'out_of_stock'? 'selected' : ''?>>Out of stock</option>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='product-status' name='product_status' class='drop-down' data-final='product_status' style="width:calc(100% - 17px);">
                <option value="">-Product status-</option>
                <option value="Y" <?=$this->native_session->get('catalog__product_status') == 'Y'? 'selected' : ''?>>Activated</option>
                <option value="N" <?=$this->native_session->get('catalog__product_status') == 'N'? 'selected' : ''?>>Deactivated</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <input class="calendar" type='text' id='start-last-updated' name='start_last_updated' placeholder='Last updated start date' data-final='start_last_updated'
                   value='<?php echo $this->native_session->get('catalog__start_last_updated'); ?>' style='width:100%;'/>
        </td>
        <td>
            <input class="calendar" type='text' id='end-last-updated' name='end_last_updated' placeholder='Last updated end date' data-final='end_last_updated'
                   value='<?php echo $this->native_session->get('catalog__end_last_updated'); ?>' style='width:100%;'/>
        </td>
    </tr>
    <tr>
        <td>
            <input type='text' id='lower-price-bound' name='lower_price_bound' placeholder='Low price bound' data-final='lower_price_bound'
                   value='<?php echo $this->native_session->get('catalog__lower_price_bound'); ?>' style='width:100%;'/>
        </td>
        <td>
            <input type='text' id='upper-price-bound' name='end_last_updated' placeholder='Upper price bound' data-final='upper_price_bound'
                   value='<?php echo $this->native_session->get('catalog__upper_price_bound'); ?>' style='width:100%;'/>
        </td>
    </tr>


<tr><td colspan="2"><button type="button" id="applyfilterbtn" name="applyfilterbtn" class="btn blue" onClick="applyFilter('catalog')" style="width:100%;">Apply Filter</button>
<input name="layerid" id="layerid" type="hidden" value="" />
<?php 
if(!empty($listtype)){
	echo "<input name='listtype' id='listtype' data-final='listtype' type='hidden' value='".$listtype."' />";
}
if(!empty($t)){ 
	echo "<input name='area' id='area' data-final='area' type='hidden' value='".$t."' />";
}?>
</td></tr>
</table>
<?php echo minify_js('catalog__list_filter', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.datepick.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.fileform.js', 'ecopharm.pagination.js', 'ecopharm.datepicker.js'));?>
