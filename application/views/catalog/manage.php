<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL;?>favicon.ico">
	<title><?php echo SITE_TITLE.': Catalog';?></title>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.list.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.pagination.css" type="text/css" media="screen" />
</head>

<body>
<table class='body-table water-mark-bg'>
<?php 
$this->load->view('addons/secure_header', array('__page'=>'Catalog' ));
$this->load->view('addons/'.$this->native_session->get('__user_type').'_top_menu', array('__page'=>'catalog' ));
?>

<tr>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
</tr>
<?php $this->load->view('addons/catalog_ribbon', array('page'=>(!empty($a)?$a: 'catalog') )); ?>

<tr>
  <td>&nbsp;</td>
  <td class='one-column fill-page'>

<table class='home-list-table'> 
<tr><th class='h3 dark-grey' style='padding-left:10px;border-bottom:1px solid #999;'>Product List</th>
<th style='border-bottom:1px solid #999; width:1%;padding:0px; padding-right:15px;'><button type='button' id='newitem' name='newitem' class='btn smallbtn green' data-rel='<?= 'catalog/add'?>'>New</button></th>

<th style='border-bottom:1px solid #999; width:1%;padding:0px;'><div id='catalog_actions' class='actions-list-btn list-actions' data-url='catalog/list_actions' data-width='300' data-targetdiv='paginationdiv__catalog_list'><div class='settings'>&nbsp;</div><div>&nbsp;</div></div></th>
</tr>

<tr><td colspan='<?php echo !(!empty($a) && $a == 'awards')? '3': '2';?>'><div id='paginationdiv__catalog_list' class='page-list-div'>
<div id="catalog__1"><?php $this->load->view('catalog/product_list',array('list'=>$list, 'type'=>(!empty($a)?$a: '') ));?></div>
</div>
<button type='button' id='refreshlist' name='refreshlist' style='display:none;'></button></td></tr>
<tr><td colspan='<?php echo !(!empty($a) && $a == 'awards')? '3': '2';?>'>
<table><tr><td>
         
<div id='catalog_pagination_div' class='pagination' style="margin:0px;padding:0px; display:inline-block;">
	<div id="catalog" class="paginationdiv no-scroll">
    	<div class="previousbtn" style='display:none;'>&#x25c4;</div>
    	<div class="selected">1</div>
    	<div class="nextbtn">&#x25ba;</div>
    </div>
    <input name="paginationdiv__catalog_action" id="paginationdiv__catalog_action" type="hidden" value="<?=base_url()."lists/load/t/catalog"?>" />
	<input name="paginationdiv__catalog_maxpages" id="paginationdiv__catalog_maxpages" type="hidden" value="<?php echo NUM_OF_LISTS_PER_VIEW;?>" />
	<input name="paginationdiv__catalog_noperlist" id="paginationdiv__catalog_noperlist" type="hidden" value="<?php echo NUM_OF_ROWS_PER_PAGE;?>" />
	<input name="paginationdiv__catalog_showdiv" id="paginationdiv__catalog_showdiv" type="hidden" value="paginationdiv__catalog_list" />
	<input name="paginationdiv__catalog_extrafields" id="paginationdiv__catalog_extrafields" type="hidden" value="" />
</div>
          

</td><td width='1%' class='filter-list shadowbox closable' data-url='<?=base_url().'catalog/list_filter/listtype/all'?>'>FILTER</td></tr></table>
</td></tr>
</table>







</td>
<td>&nbsp;</td>
</tr>

<?php $this->load->view('addons/secure_footer');?>

</table>
<?php echo minify_js('catalog__manage', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.pagination.js', 'ecopharm.fileform.js', 'ecopharm.list.js'));?>

<?php if(!empty($action)){ ?>
<script>
$(function() { 
	$(document).find('.filter-list').last().click(); 
});
</script>
<?php }?>
</body>
</html>