<?php if(empty($msg)) $msg = get_session_msg($this);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL;?>favicon.ico">
	<title><?php echo SITE_TITLE.': New product';?></title>
    
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.form.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.list.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/trumbowyg.css" type="text/css" media="screen" />
</head>

<body>
<table class='body-table water-mark-bg'>
<?php 
$this->load->view('addons/secure_header', array('__page'=>'New product' ));
$this->load->view('addons/'.$this->native_session->get('__user_type').'_top_menu', array('__page'=>($this->native_session->get('__user_type') == 'provider'? 'contracts': 'procurement') ));

if($this->native_session->get('__user_type') != 'provider'){
?>
<tr>
  <td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
</tr>
<?php $this->load->view('addons/catalog_ribbon', array('page'=>'bids')); 
}
?>


<tr>
	<td>&nbsp;</td>
  	<td class='one-column body-form-area microform ignoreclear'>
		<table>
            <?php if(!empty($msg)): ?>
            <tr>
                <td colspan="2">
                    <?=format_notice($this, $msg)?>
                </td>
            </tr>
            <?php endif ?>
			<tr>
				<td width="50%">
                	<div class="form_section">                    
                    	<div class="form_section_paddings">
                    		<div class="label">Product title*</div>
							<input type='text' id='product_title' name='product_title' placeholder='Product title' class='' value='<?php echo (!empty($formdata['product_title'])? $formdata['product_title']: '');?>'/>
							<div class="label">Description*</div>
							<textarea id='product_description' name='product_description' placeholder='Use this area to type in a description of the product and help customers learn more about this product' class='limit-chars htmlfield' data-max='500' style='height: 150px;'><?php echo (!empty($formdata['description'])? $formdata['description']: '');?></textarea>
                    	</div>
                    </div>
                    <div class="form_section product_photos">           
                    	<div class="form_section_paddings">
                        	<div class="manage_product_photos" id="manage-product-photos">       	
								<?php
                                    if(!empty($formdata['photos'])):
                                                                           
                                        foreach($formdata['photos'] AS $photo):
                                        
                                          print '<div id="item_'. $photo['id'] .'__'. $photo['url'] .'">'.
										  		'<div class="delete_photo delete_icon"></div>'.
												'<div class="sort_instr">Drag to sort</div>'.
												'<div class="view_photo"><a href="'. base_url() .'catalog/view_product_photo/p/'. $photo['url'] .'" class="shadowbox">View</a></div>'.
												'<img src="'. BASE_URL .'assets/uploads/thumb_'. $photo['url'] .'" /></div>';
                                        
                                        endforeach;
                                        
                                    endif;
                                ?>                            
                            </div>
                            <table class='default-table' style="width:calc(100% + 18px);">
                                <tr>
                                    <td style='width:99%;'>
                                        <input type='text' id='product-photo' name='product_photo' class='filefield optional upload_on_select' data-resultdiv='manage-product-photos' data-actionurl='catalog/upload_photo' data-val='jpg,png,jpeg' data-size='5120000' placeholder='OPTIONAL: Select image' value=''/>                    	               
                                    </td>
                                </tr>
                            </table>
                    	</div>
      				</div>
              	</td>
				<td rowspan="2">
                	<div class="form_section">                    	    
                    	<div class="form_section_paddings">
                            <div>
                                <div class="label">Pricing*</div>
                                <input type='text' id='current_price' name='current_price' placeholder='Pricing' class='' value='<?php echo (!empty($formdata['current_price'])? $formdata['current_price']: '');?>'/>
                            </div>
                            <div>
                                <div class="label">Quantity per unit*</div>
                                <input type='text' id='quantity-per-unit' name='quantity_per_unit' placeholder='e.g 190ml' class='' value='<?php echo (!empty($formdata['quantity_per_unit'])? $formdata['quantity_per_unit']: '');?>'/>
                            </div>
                    	</div>
                    </div>
                    <div class="form_section">    
                    	<div class="form_section_paddings">
                    		<div class="label">Publish product?</div>
                            <label class="container yes">Yes
                                <input name="isactive" id="active-product" value="Y" type="radio" <?=(!empty($formdata['isactive']) && $formdata['isactive'] == 'Y')? 'checked' : ''?> />
                                <span class="checkmark"></span>
                            </label>
                            <label class="container no">No
                                <input name="isactive" id="deactivated-product" value="N" type="radio" <?=(!empty($formdata['isactive']) && $formdata['isactive'] == 'N')? 'checked' : ''?> />
                                <span class="checkmark"></span>
                            </label>
                    	</div>
                    </div>
                    <div class="form_section">    
                    	<div class="form_section_paddings">
                    		<div class="label">Stock control</div>
							<label class="container yes">Available
                                <input name="stock_status" id="available-product" value="available" type="radio" <?=(!empty($formdata['stock_status']) && $formdata['stock_status'] == 'available')? 'checked' : ''?>>
                                <span class="checkmark"></span>
                            </label>
                            <label class="container no">Out of stock
                                <input name="stock_status" id="unavailable-product" value="out_of_stock" type="radio" <?=(!empty($formdata['stock_status']) && $formdata['stock_status'] == 'out_of_stock')? 'checked' : ''?>>
                                <span class="checkmark"></span>
                            </label>
                    	</div>
                    </div>
                    <div class="form_section">    
                    	<div class="form_section_paddings">
                    		<div class="label">Product category</div>
                            <?php
								if(!empty($categories)):
									
									# show categories in three columns
									$items_per_column = count($categories)/3;
									
									$ctr = 1;
									
									foreach($categories as $category):
									
							?>
                            	<label class="container yes">
                                	<?=$category['category_title']?>
                                    <input name="category" id="category-<?=$category['category_id']?>" value="<?=$category['category_id']?>" type="checkbox" <?=(!empty($formdata['categories']) && in_array($category['category_id'], $formdata['categories']))? 'checked' : ''?>>
                                    <span class="checkmark"></span>
                                </label>                            
                            <?php								
									endforeach;	
								endif;						
							?>
                    	</div>
                    </div>
                </td>
			</tr>
			<tr>
    			
         	</tr>      
      		<tr>
                <td style="text-align:right; padding-right:0px;padding-top:30px; padding-bottom:20px;">
                	<button type="button" id="save" name="save" class="btn green submitmicrobtn" style='width: calc(100% + 10px);'>Save</button>
          		</td>
                <td style="text-align:right; padding-right:0px;padding-top:30px; padding-bottom:20px;">
                    <button type="button" id="save-and-new" name="save_and_new" class="btn blue submitmicrobtn save_and_new" style='width: calc(100% + 10px);'>Save & New</button>
                    <input type='hidden' id='action' name='action' value='<?=base_url().'catalog/add'?>' />
                    <input type='hidden' id='redirectaction' name='redirectaction' value='<?=base_url().'catalog/manage'?>' />
                    <input type='hidden' id='form-url' name='formurl' value='<?=base_url().'catalog/add'?>' />
                    <input type='hidden' id='resultsdiv' name='resultsdiv' value='' />
                    <?=(!empty($p)? "<input type='hidden' id='product_id' name='productid' value='".$p."' />" : "")?>
                </td>
       		</tr>
      </table>
</td>
<td>&nbsp;</td>
</tr>

<?php $this->load->view('addons/secure_footer');?>

</table>
<?php echo minify_js('bids__new_bid', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.pagination.js', 'ecopharm.fileform.js', 'trumbowyg.js'));?>
<script type="application/javascript">
    $('.htmlfield').trumbowyg({btns: [
            '|', ['bold', 'italic', 'underline'],
            '|', ['justifyLeft', 'justifyCenter', 'justifyRight'],
            '|', ['unorderedList', 'orderedList'],
            'insertHorizontalRule'],
        removeformatPasted: true
    });
</script>
</body>
</html>