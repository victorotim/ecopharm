<?php

$stopHtml = "<input name='paginationdiv__catalog_stop' id='paginationdiv__catalog_stop' type='hidden' value='1' />";
$listCount = count($list);
$i = 0;

echo "<table id='product-list'>" .
    "<tr>" .
    "<th colspan='2'>Product details</th>" .
    "<th>Price</th>".
    "<th>Product category</th>" .
    "<th>Last updated</th>" .
    "</tr><tbody>";

if(!empty($list)){
		
	foreach($list AS $row) {
        $last_updated = date_create($row['last_updated']);
        date_add($last_updated, date_interval_create_from_date_string('10 hours'));

        $i++;
		$img_url = (!empty($row['photo_url'])? BASE_URL .'assets/uploads/thumb_'. $row['photo_url']: IMAGE_URL .'no-image-found.jpg');
		
		$stock_status = '<span class="'. ($row['stock_status'] == 'available'? 'green_text">Stock available' : 'red_text">Out of stock').'</span>';

		$published_status = '<img src="'. IMAGE_URL .($row['isactive'] == 'Y'? 'correct.png" /><span class=""><i>activated</i></span>' : 'wrong.png" /><span class=""><i>deactivated</i></span>');
		
		echo "<tr>".
			 "<td width='1%' style='vertical-align: middle'><input id='select_".$row['product_id']."' name='selectall[]' type='checkbox' value='".$row['product_id']."' class='bigcheckbox'><label for='select_".$row['product_id']."'></label></td>".
			 "<td  style='vertical-align: top' nowrap>".
			 "<div class='product_photo'><a href='".base_url()."catalog/add/p/". encrypt_value($row['product_id']) ."'><img class='thumb' src='". $img_url ."' /></a></div>".
			 "<div class='product_basic_details'>".
			 "<div class='product_title'>". $row['product_title'] .(!empty($row['quantity_per_unit'])? '<i style="font-weight: normal">&nbsp;,'.$row['quantity_per_unit'] .'</i>' : '' ) ."</div>".
			 "<div class='product_status'><div class='on_status'>". $published_status ."</div><div class='stock_status'>". $stock_status ."</div></div>".
			 "</div></td>".
			 "<td><div class='product_price' style='width: auto'><span>". format_number($row['current_price'], 100, 0)." UgX</span></div></td>".
             "<td>{$row['category_names']}</td>".
			 "<td>".date_format($last_updated, 'd M, Y H:i') ." by {$row['updater']}";

        # Check whether you need to stop the loading of the next pages
        if($i == $listCount && ((!empty($n) && $listCount < $n) || (empty($n) && $listCount < NUM_OF_ROWS_PER_PAGE))){
            echo $stopHtml;
        }

		echo "</td></tr>";
	}
}

else {
	echo "<tr><td>".format_notice($this, 'WARNING: There are no products in this list.')."</td></tr>";
}
echo "</tbody></table>";
?>