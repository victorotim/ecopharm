<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="speciality pharmacy, medication & support, e-shop" />
    <meta name="author" content="" />
    <meta name="robots" content="" />
    <meta name="description" content="Ecopharm Ltd is a leading private chain of pharmacies in Uganda" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->

    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css" />
    <?php echo minify_css('home', array('shop.css'));?>
    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">        
        	<!-- CAROUSEL START -->
        	<section class="banner-area">
            	<div class="container">
                	<div class="carousel-slider-active owl-carousel">
                    	<?php
                        $banner_content_color = 0;

                        foreach ($categories as $category):

                            if ($category['show_on_banner'] == 'NO'):

                                continue;

                            else:

                                $banner_content_color++;

                                if($banner_content_color>3) $banner_content_color = 0;

                            endif;

                            $category_img_url = (!empty($category['photo_url']) ? BASE_URL . 'assets/uploads/' . $category['photo_url'] : IMAGE_URL . 'no-image-found.jpg');
                    	?>
                        
						<div class="banner-wrap">
                        	<img src="<?=$category_img_url?>" alt="<?=$category['category_title']?>">
                            <div class="banner-content bg-color-<?=$banner_content_color?>">
                            	<!--<small class="banner-desc <? #$banner_content_color == 2? 'text-muted' : ''?>">new arrivals</small> -->
                            	<h2 class="banner-title"><?=ucwords(strtolower($category['category_title']))?></h2>
                            	<button class="banner-btn"><a href="<?=base_url(). 'product_category/'. $category['slug']?>">shop now</a></button>
                            </div>
                        </div>
						<?php endforeach; ?>
                    </div>
                </div>
            </section>
            <!-- CAROUSEL END --> 
            
            <!-- PRODUCT CATEGORY SECTION START -->
            <?php
                # show two categories before the banner
                $category_ctr = 0;

                for($i=$category_ctr; $i<count($categories); $i++):

                    if($category_ctr>1):

                        # set index for next category iterator after the banner
                        $category_ctr = $i;

                        break;

                    endif;

				    $category = $categories[$i];

				    if(!empty($category['num_of_products'])):

                        $category_ctr++;
			?>
            <section class="section-pad">
                <div class="container">
                	<div class="section-title">
                        <h2><span>Explore</span><?=ucwords(strtolower($category['category_title']))?></h2>
                        <a href="<?=base_url().'product_category/'. $category['slug']?>">See all</a>
                    </div>
                    <div class="row">
						<?php
                            # Get 5 products under the category
                            $category_products = 0;
                            for($j=0; $j<count($catalog); $j++):

                                $catalog_item = $catalog[$j];

                                if($category_products>5) break;

                                if(in_array($category['category_id'], explode(',', $catalog_item['categories']))):

                                    $category_products++;

									$this->load->view('addons/public_product_widget', array('catalog_item'=>$catalog_item, 'col-md'=>2));

                                endif;

                            endfor;
                        ?>
                    </div>
                </div>
            </section>
            <?php 
					endif;

				endfor;
			?>            
            <!-- PRODUCT CATEGORY SECTION END -->

            <!-- banner SECTION START
            <section class="section-pad banner-section">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-6 col-12">
                            <div class="banner-wrap">
                                <img src="<?=IMAGE_URL?>banners/mask.jpg" alt="">
                                <div class="banner-content">
                                <small class="banner-desc">new arrivals</small>
                                <h2 class="banner-title">Face Masks</h2>
                                <button class="banner-btn"><a href="#">shop now</a></button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 d-none d-sm-block">
                            <div class="banner-wrap">
                                <img src="<?=IMAGE_URL?>banners/saniter.jpg" alt="">
                                <div class="banner-content">
                                <small class="banner-desc">80% off</small>
                                <h2 class="banner-title">Hand Sanitizers</h2>
                                <button class="banner-btn"><a href="#">shop now</a></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            banner SECTION END -->

            <!-- PRODUCT CATEGORY SECTION START -->
            <?php
            # show 4 categories after the banner
            for ($i = $category_ctr; $i < count($categories); $i++):

                if ($category_ctr > 5) break;

                if (!empty($categories[$i]) && !empty($categories[$i]['num_of_products'])):

                    $category = $categories[$i];

                    $category_ctr++;
                    ?>
                    <section class="section-pad">
                        <div class="container">
                            <div class="section-title">
                                <h2><span>Explore</span><?=ucwords(strtolower($category['category_title']))?></h2>
                                <a href="<?=base_url().'product_category/'. $category['slug']?>">See all</a>
                            </div>
                            <div class="row">
                                <?php
                                # Get 5 products under the category
                                $category_products = 0;
                                for ($k = 0; $k < count($catalog); $k++):

                                    $catalog_item = $catalog[$k];

                                    if ($category_products > 5) break;

                                    if (in_array($category['category_id'], explode(',', $catalog_item['categories']))):

                                        $category_products++;

                                        $this->load->view('addons/public_product_widget', array('catalog_item' => $catalog_item, 'col_md'=>2));

                                    endif;
                                endfor;
                                ?>
                            </div>
                        </div>
                    </section>
                <?php
                endif;
            endfor;
            ?>
            <!-- PRODUCT CATEGORY SECTION END -->
              
        </div>
        <!-- CONTENT END -->       
    </div>
    <?=$this->load->view('addons/checkout_modal')?>  
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js'));?>
</body>
</html>