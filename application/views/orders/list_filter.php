<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/external-fonts.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ecopharm.shadowbox.css" type="text/css" media="screen" />


<table class='normal-table filter-container'>
    <tr>
        <td>
            <input class="calendar" type='text' id='order-start-date' name='order_start_date' placeholder='Order start date' data-final='order_start_date'
                   value='<?php echo $this->native_session->get('order__order_start_date'); ?>' style='width:100%;'/>
        </td>
        <td>
            <input class="calendar" type='text' id='order-end-date' name='order_end_date' placeholder='Order end date' data-final='order_end_date'
                   value='<?php echo $this->native_session->get('order__order_end_date'); ?>' style='width:100%;'/>
        </td>
    </tr>
    <tr>
        <td colspan="2"><input type='text' id='order-no' name='order_no' placeholder='Order No.' data-final='order_no'
                   value='<?php echo $this->native_session->get('order__order_no'); ?>' style='width:100%;'/></td>
    </tr>
    <tr>
        <td colspan="2"><input type='text' id='customer-name' name='customer_name' placeholder='Customer name' data-final='customer_name'
                   value='<?php echo $this->native_session->get('order__customer_name'); ?>' style='width:100%;'/></td>
    </tr>
    <tr>
        <td colspan="2"><input type='text' id='telephone-number' name='telephone_number' placeholder='Contacts' data-final='telephone_number'
                   value='<?php echo $this->native_session->get('order__telephone_number'); ?>' style='width:100%;'/></td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='payment-method' name='payment_method' class='drop-down' data-final='payment_method' style="width:calc(100% - 17px);">
                <option value="">-Payment method-</option>
                <option value="cash_on_delivery" <?=$this->native_session->get('order__payment_method') == 'cash_on_delivery'? 'selected' : ''?>>Cash on delivery</option>
                <option value="mobile_money" <?=$this->native_session->get('orders__payment_method') == 'mobile_money'? 'selected' : ''?>>Mobile Money</option>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='payment-status' name='payment_status' class='drop-down' data-final='payment_status' style="width:calc(100% - 17px);">
                <option value="">-Payment status-</option>
                <?php echo get_option_list($this, 'paymentstatus', '', '', array('selected'=>$this->native_session->get('order__payment_status')));?>
            </select>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <select id='fulfillment-status' name='fulfillment_status' class='drop-down' data-final='fulfillment_status' style="width:calc(100% - 17px);">
                <option value="">-Fulfillment status method-</option>
                <?php echo get_option_list($this, 'fulfillmentstatus', '', '', array('selected'=>$this->native_session->get('order__fulfillment_status')));?>
            </select>
    </tr>


<tr><td colspan="2"><button type="button" id="applyfilterbtn" name="applyfilterbtn" class="btn blue" onClick="applyFilter('order')" style="width:100%;">Apply Filter</button>
<input name="layerid" id="layerid" type="hidden" value="" />
<?php 
if(!empty($listtype)){
	echo "<input name='listtype' id='listtype' data-final='listtype' type='hidden' value='".$listtype."' />";
}
if(!empty($t)){ 
	echo "<input name='area' id='area' data-final='area' type='hidden' value='".$t."' />";
}?>
</td></tr>
</table>
<?php echo minify_js('catalog__list_filter', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.datepick.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.fileform.js', 'ecopharm.pagination.js', 'ecopharm.datepicker.js'));?>
