<?php if(empty($msg)) $msg = get_session_msg($this);?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo IMAGE_URL; ?>favicon.ico">
    <title><?php echo SITE_TITLE . ': Orders'; ?></title>

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/external-fonts.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ecopharm.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ecopharm.list.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ecopharm.shadowbox.css" type="text/css"
          media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ecopharm.pagination.css" type="text/css"
          media="screen"/>
</head>

<body>
<table class='body-table water-mark-bg'>
    <?php
    $this->load->view('addons/secure_header', array('__page' => 'Orders'));
    $this->load->view('addons/' . $this->native_session->get('__user_type') . '_top_menu', array('__page' => 'orders'));
    ?>

    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <?php ($this->native_session->get('__user_type') == 'admin' ? $this->load->view('addons/orders_ribbon', array('page' => 'orders')) : ''); ?>

    <tr>
        <td>&nbsp;</td>
        <td class='one-column fill-page'>
            <?php if (!empty($msg)) print format_notice($this, $msg); ?>
            <table class='home-list-table'>
                <tr>
                    <th class='h3 dark-grey' style='padding-left:10px;border-bottom:1px solid #999;'>Order list</th>
                    <th style='border-bottom:1px solid #999; width:1%;padding:0px; padding-right:15px;'>
                    </th>

                    <th style='border-bottom:1px solid #999; width:1%;padding:0px;'>
                        <div id='order_actions' class='actions-list-btn list-actions' data-url='orders/list_actions'
                             data-width='300' data-targetdiv='paginationdiv__order_list'>
                            <div class='settings'>&nbsp;</div>
                            <div>&nbsp;</div>
                        </div>
                    </th>
                </tr>

                <tr>
                    <td colspan='<?php echo !(!empty($a) && $a == 'awards') ? '3' : '2'; ?>'>
                        <div id='paginationdiv__order_list' class='page-list-div'>
                            <div id="order__1"><?php $this->load->view('orders/order_list', array('list' => $list, 'type' => (!empty($a) ? $a : ''))); ?></div>
                        </div>
                        <button type='button' id='refreshlist' name='refreshlist' style='display:none;'></button>
                    </td>
                </tr>
                <tr>
                    <td colspan='<?php echo !(!empty($a) && $a == 'awards') ? '3' : '2'; ?>'>
                        <table>
                            <tr>
                                <td>

                                    <div id='order_pagination_div' class='pagination'
                                         style="margin:0px;padding:0px; display:inline-block;">
                                        <div id="order" class="paginationdiv no-scroll">
                                            <div class="previousbtn" style='display:none;'>&#x25c4;</div>
                                            <div class="selected">1</div>
                                            <div class="nextbtn">&#x25ba;</div>
                                        </div>
                                        <input name="paginationdiv__order_action" id="paginationdiv__order_action"
                                               type="hidden" value="<?= base_url() . "lists/load/t/order" ?>"/>
                                        <input name="paginationdiv__order_maxpages" id="paginationdiv__order_maxpages"
                                               type="hidden" value="<?php echo NUM_OF_LISTS_PER_VIEW; ?>"/>
                                        <input name="paginationdiv__order_noperlist" id="paginationdiv__order_noperlist"
                                               type="hidden" value="<?php echo NUM_OF_ROWS_PER_PAGE; ?>"/>
                                        <input name="paginationdiv__order_showdiv" id="paginationdiv__order_showdiv"
                                               type="hidden" value="paginationdiv__order_list"/>
                                        <input name="paginationdiv__order_extrafields"
                                               id="paginationdiv__order_extrafields" type="hidden" value=""/>
                                    </div>


                                </td>
                                <td width='1%' class='filter-list shadowbox closable'
                                    data-url='<?= base_url() . 'orders/list_filter' ?>'>FILTER
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </td>
        <td>&nbsp;</td>
    </tr>

    <?php $this->load->view('addons/secure_footer'); ?>

</table>
<?php echo minify_js('orders__manage', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.shadowbox.js', 'ecopharm.pagination.js', 'ecopharm.fileform.js', 'ecopharm.list.js')); ?>

<?php if (!empty($action)) { ?>
    <script>
        $(function () {
            $(document).find('.filter-list').last().click();
        });
    </script>
<?php } ?>
</body>
</html>