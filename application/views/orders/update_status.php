<?php 
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.css' type='text/css' media='screen' />";
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.list.css' type='text/css' media='screen' />";
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.form.css' type='text/css' media='screen' />"; 
echo minify_js('orders__update_status', array('jquery-2.1.1.min.js', 'jquery-ui.js', 'jquery.form.js', 'ecopharm.js', 'ecopharm.fileform.js'));


if(!empty($msg)){
	echo format_notice($this, $msg);
}
else
{
	$date_str = date('M d, Y', strtotime($summary['dateadded'])) ." at ". date('H:i', strtotime($summary['dateadded'])) .'hrs';
	
	$order_details_str = '<table cellpadding="8" cellspacing="0">';
	
	$order_total = 0;
	
	foreach($details as $detail):
		
		$img_url = (!empty($detail['photo_url'])? BASE_URL .'assets/uploads/'. $detail['photo_url']: IMAGE_URL .'no-image-found.jpg');
		
		$order_details_str .= '<tr>'.
							  '<td>'.
							  '<div class="product_photo"><img class="thumb" src="'. $img_url .'" /></div>'.
							  '</td><td nowrap>'.
							  '<div class="product_basic_details">'.
							  '<div>'. $detail['product_title'] .'</div>'.
							  '<div>*'. format_number($detail['quantity']) .' @'. format_number($detail['price']) .'UgX</div>'.
							  '</div>
							  </td>'.
							  '<td align="right">'. format_number($detail['quantity'] * $detail['price']) .'</td>';
		
		$order_details_str .= '</tr>';
		
		$order_total += ($detail['quantity'] * $detail['price']);
	
	endforeach;
	
	$order_details_str .= '<tr><td class="bold" align="right" colspan=2>Subtotal:</td><td align="right">'. format_number($order_total) .'</td></tr>'.
						  '<tr><td class="bold" align="right" colspan=2>Delivery fee:</td><td align="right">2000</td></tr>'.
						  '<tr><td class="bold" align="right" colspan=2>Order total:</td><td align="right">'. format_number($order_total + 2000) .'</td></tr>';
	
	$order_details_str .= '</table>';
	
	 "<table>".
		 "<tr>".
			"<td class='bold' style='padding:1em' colspan=2>#".$this->_util->encode($summary['order_id'])." <span style='font-weight: normal'>". $date_str ."</span></td></tr>
			<tr><td>Customer Name:</td><td>". $summary['customer_first_name'] .' '. $summary['customer_last_name'] ."</td></tr>
			<tr><td>Telephone No:</td><td>". $summary['phone_number'] ."</td></tr>
			<tr><td>Other No:</td><td>". $summary['other_phone_number'] ."</td></tr>
			<tr><td>Email Address:</td><td>". $summary['customer_email_address'] ."</td></tr>
			<tr><td>Physical Address:</td><td>". $summary['address'].'<br/>'. $summary['additional_address_info'] ."</td></tr>			
			<tr><td><div style='padding: 1em 0.5em'><h3>Order details</h3>". $order_details_str ."</div></td></tr>
		 </table>";
}
?>
<div>
	<h2>Update order <?=$this->_util->encode($summary['order_id'])?></h2>
    <h3><?=$summary['customer_first_name'].' '.$summary['customer_last_name'] .' on '. $date_str?></h3>
    <div class="status_type_container">
    	<ul class="payment_status">
        	<li>Payment Status</li>
            <?php 
				if(!empty($payment_statuses)):
					
					foreach($payment_statuses as $payment_status): 
			?>
            <li>
            	<label class="<?=!empty($summary['payment_status']) && $summary['payment_status'] == $payment_status? 'active_status': ''?>">
             		<span class="radio_label"><?=ucwords($payment_status)?></span>
               		<input type="radio" name="payment_status" value="<?=$payment_status?>" <?=!empty($summary['payment_status']) && $summary['payment_status'] == $payment_status? 'checked': ''?>/>
            	</label>
            </li>            
            <?php
					endforeach;
				endif;
			?>
        </ul>
        <ul class="fulfillment_status">
        	<li>Fulfillment Status</li>
            <?php 
				if(!empty($fulfillment_statuses)):
					
					foreach($fulfillment_statuses as $fulfillment_status): 
			?>
            <li>
            	<label class="<?=!empty($summary['fulfillment_status']) && $summary['fulfillment_status'] == $fulfillment_status? 'active_status': ''?>">
             		<span class="radio_label"><?=ucwords($fulfillment_status)?></span>
               		<input type="radio" name="fulfillment_status" value="<?=$fulfillment_status?>" <?=!empty($summary['fulfillment_status']) && $summary['fulfillment_status'] == $fulfillment_status? 'checked': ''?>/>
            	</label>
            </li>            
            <?php
					endforeach;
				endif;
			?>
        </ul>
        <div style="width:100%" class="update_status_actions">
        	<div>
            	<input type="hidden" name="order_id" id="order-id" value="<?=encrypt_value($summary['order_id'])?>" />
            	<button type="button" id="btn-update-status" name="update_status" class="btn green submitmicrobtn">Update status</button>
            </div>
        	<div><a href="javascript:void(0)" class="bold">Cancel</a></div>
        </div>
    </div>
</div>

<input type='hidden' id='layerid' name='layerid' value='' />