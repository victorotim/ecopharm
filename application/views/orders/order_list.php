<?php 
$stopHtml = "<input name='paginationdiv__order_stop' id='paginationdiv__order_stop' type='hidden' value='1' />";
$listCount = count($list);
$i = 0;

echo "<table>
	 <tr><th>Date</th><th>Order No.</th>";

if($this->native_session->get('__user_type') == 'admin') echo "<th>Customer Name</th><th>Contacts</th>";
	 
echo "<th>Order Total</th>
	 <th>Payment Method</th>
	 <th>Payment Status</th>
	 <th>Fulfillment Status</th><th>&nbsp;</th></tr>";
	 
	foreach($list AS $row) {
		
		$update_status_lnk = ($this->native_session->get('__user_type') == 'admin')? '<a href="'. base_url(). 'orders/update_status/d/'. encrypt_value($row['order_id']) .'" class="shadowbox">Update status</a>' : '';
		
		$i++;
		echo "<tr><td>".date(FULL_DATE_FORMAT, strtotime($row['dateadded']))."</td>".
			 "<td><a href='". base_url() ."orders/details/d/". encrypt_value($row['order_id']) ."' class='bold shadowbox'>". $this->_util->encode($row['order_id']) ."</a> </td>";
		
		if($this->native_session->get('__user_type') == 'admin')
			print '<td>'.ucwords(strtolower($row['customer_first_name'].' '.$row['customer_last_name'])).'</td>'.
				  '<td>'. $row['phone_number'] .'</td>';
		
		echo "<td>".format_number($row['order_total'])."</td>
		<td>".strtoupper($row['payment_method'])."</td>
		<td><div class='order_status ". str_replace(' ', '_', $row['payment_status'])."'>".ucwords($row['payment_status'])."</div></td>
		<td><div class='order_status ". str_replace(' ', '_', $row['fulfillment_status'])."'>".ucwords($row['fulfillment_status'])."</div></td>
		<td>". $update_status_lnk;

		# Check whether you need to stop the loading of the next pages
		if($i == $listCount && ((!empty($n) && $listCount < $n) || (empty($n) && $listCount < NUM_OF_ROWS_PER_PAGE))){
			echo $stopHtml;
		}
		  echo "</td>
		</tr>";
	}
echo "</table>";
?>
