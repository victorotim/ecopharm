<?php 
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.css' type='text/css' media='screen' />";
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.list.css' type='text/css' media='screen' />";
echo "<link rel='stylesheet' href='".base_url()."assets/css/ecopharm.form.css' type='text/css' media='screen' />"; 


if(!empty($msg)){
	echo format_notice($this, $msg);
}
else
{
	$date_str = date('M d, Y', strtotime($summary['dateadded'])) ." at ". date('H:i', strtotime($summary['dateadded'])) .'hrs';

	$order_details_str = '<table cellpadding="8" cellspacing="0">';
	
	$order_total = 0;
	
	foreach($details as $detail):
		
		$img_url = (!empty($detail['photo_url'])? BASE_URL .'assets/uploads/'. $detail['photo_url']: IMAGE_URL .'no-image-found.jpg');
		
		$order_details_str .= '<tr>'.
							  '<td>'.
							  '<div class="product_photo"><img class="thumb" src="'. $img_url .'" /></div>'.
							  '</td><td nowrap>'.
							  '<div class="product_basic_details">'.
							  '<div>'. $detail['product_title'] .'</div>'.
							  '<div>*'. format_number($detail['quantity']) .' @'. format_number($detail['price']) .'UgX</div>'.
							  '</div>
							  </td>'.
							  '<td align="right">'. format_number($detail['quantity'] * $detail['price']) .'</td>';
		
		$order_details_str .= '</tr>';
		
		$order_total += ($detail['quantity'] * $detail['price']);
	
	endforeach;
	
	$order_details_str .= '<tr><td class="bold" align="right" colspan=2>Subtotal:</td><td align="right">'. format_number($order_total) .'</td></tr>'.
						  '<tr><td class="bold" align="right" colspan=2>Delivery fee:</td><td align="right">'. format_number($summary['delivery_fee']) .'</td></tr>'.
						  '<tr><td class="bold" align="right" colspan=2>Order total:</td><td align="right">'. format_number($order_total + $summary['delivery_fee']) .'</td></tr>';
	
	$order_details_str .= '</table>';
	
	echo "<table>".
		 "<tr>".
			"<td class='bold' style='padding:1em' colspan=2>#".$this->_util->encode($summary['order_id'])." <span style='font-weight: normal'>". $date_str ."</span></td></tr>
			<tr><td class='label' width='1%' nowrap>Customer Name:</td><td>". $summary['customer_first_name'] .' '. $summary['customer_last_name'] ."</td></tr>
			<tr><td class='label'>Telephone No:</td><td>". $summary['phone_number'] ."</td></tr>
			<tr><td class='label'>Other No:</td><td>". $summary['other_phone_number'] ."</td></tr>
			<tr><td class='label'>Email Address:</td><td>". $summary['customer_email_address'] ."</td></tr>
			<tr><td class='label'>Physical Address:</td><td>". $summary['address'].'<br/>'. $summary['additional_address_info'] ."</td></tr>			
			<tr><td colspan='2'><div style='padding: 1em 0.5em'><h3>Order details</h3>". $order_details_str ."</div></td></tr>
		 </table>";
}
?>