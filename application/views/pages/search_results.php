<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE?>:&nbsp;Search results</title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="<?=base_url()?>js/html5shiv.min.js"></script>
        <script src="<?=base_url()?>js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li>Search results</li>
                    </ul>
                    <h1>Search results</h1>
                </div>
            </section>

            <section class="section-pad">
                <div class="container">
                    <div class="row">
                        <aside class="col-lg-3 col-md-4 d-none d-sm-block">
                            <ul class="category-list">
                            	<?php
									 if(!empty($categories)):
									 	
										foreach($categories as $category_row): 
								?>
                                <li class="<?=!empty($selected_category) && $selected_category['category_id'] == $category_row['category_id']? 'fw-bold' : ''?>" style="position: relative;">
									<?=$category_row['category_title']?> 
                                    <span class="count">(<?=$category_row['num_of_products']?>)</span>
                                    <a href="<?=base_url().'pages/product_category/c/'.encrypt_value($category_row['category_id'])?>" class="stretched-link">&nbsp;</a>
                              	</li>
                                <?php
										endforeach;
										
									endif;
								?>
                            </ul>
                        </aside>
                        <article class="col-lg-9 col-md-8">                        
                            <div class="row">
                            	<?php
									 if(!empty($list)):
									 	
										foreach($list as $list_item):
											
											$product_img_url = (!empty($list_item['photo_url'])? BASE_URL .'assets/uploads/' . $list_item['photo_url']: IMAGE_URL .'no-image-found.jpg');
								?>
                                <div class="col-md-3 col-sm-4 col-6">
                                    <a href="<?=base_url() .'pages/product_details/p/'. encrypt_value($list_item['product_id'])?>">
                                        <div class="product-box">
                                            <img src="<?=$product_img_url?>">
                                            <h3 class="product-price">Ush <?=format_number($list_item['current_price'], 9, 0)?></h3>
                                            <p class="product-title"><?=$list_item['product_title']?>
                                                <span class="product-size">100g</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <?php
										endforeach;
										
									endif;
								?>
                            </div>
                        </article>
                    </div>
                </div>
            </section>                 
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 
 
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js'));?>
</body>
</html>