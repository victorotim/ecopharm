
<div>
    <!-- 2 column grid layout with text inputs for the first and last names -->
    <div class="row mb-4">
        <div class="col">
            <p>Ecopharm respects your privacy. As such we have a policy that explains how we collect, use, disclose, and safeguard your information when you visit our site including any other related media channel. Please read this privacy policy carefully. If you do not agree with the terms of this privacy policy, please do not access the site.</p>
            <p>
We reserve the right to make changes to this Privacy Policy. In case of any changes, we shall inform you by updating the “Last Updated” date of this Privacy Policy. Any changes or modifications will be effective immediately upon posting the updated Privacy Policy on the site. We therefore encourage you to periodically review this policy to stay informed of any updates. You will be deemed to have been made aware of, will be subject to, and will be deemed to have accepted the changes in any revised Privacy Policy by your continued use of the site after the date such revised privacy policy is posted. 
</p>
            <h2>COLLECTION OF YOUR INFORMATION</h2>
            <p>We may collect information about you in a variety of ways. The information we may collect on the site includes:</p>
            <h3>Personal Data</h3>
            <p>Personally identifiable information, such as your name, shipping address, email address, and telephone number, and demographic information, such as your age, gender, hometown, that you voluntarily give to us when you register with forms at our branches, register with this site or when you choose to participate in activities related to this site including purchasing a product. You are under no obligation to provide us with personal information of any kind, however your refusal to do so may prevent you from using certain features of the site.</p>
            <h3>Derivative Data</h3>
            <p>Information our servers automatically collect when you access the Site, such as your IP address, your browser type, your operating system, your access times, and the pages you have viewed directly before and after accessing the Site.</p>
            <h3>Facebook (Meta) Permissions</h3>
            <p>
The Site may by default access your basic Facebook (Meta) account information, including your name, email, gender, birthday, current city, and profile picture URL, as well as other information that you choose to make public. We may also request access to other permissions related to your account, such as friends, check ins, and likes, and you may choose to grant or deny us access to each individual permission. For more information regarding Facebook permissions, refer to their permissions reference page.</p>
			<h3>Third-Party Data</h3>
Information <p>from third parties, such as personal information or network friends, if you connect your account to the third party and grant the site permission to access this information.</p>

			<h2>USE OF YOUR INFORMATION</h2>
            <p>Having accurate information about you permits us to provide you with a smooth, efficient and customized experience. Specifically, we may use information collected about you via the site to:
            	<ul>
                <li>Email/Call you regarding your account or order.</li>
                <li>Fulfill and manage purchases, orders, payments, and other transactions related to the site.</li>
                <li>Generate a personal profile about you to make future visits to the site more personalized.</li>
                <li>Increase the efficiency and operation of the site.</li>
                <li>Monitor and analyze usage and trends to improve your experience with the Site</li>
                <li>Notify you of updates to the site and our services.</li>
                <li>Offer new products, services and/or recommendations to you.</li>
                <li>Prevent fraudulent transactions, monitor against theft, and protect against criminal activity.</li>
                <li>Process payments and refunds.</li>
                <li>Request feedback and contact you about your use of the Site</li>
                <li>Resolve disputes and troubleshoot problems.</li>
                <li>Respond to product and customer service requests.</li>
                <li>Deliver targeted advertising, coupons, newsletters, and other information regarding promotions and Ecopharm to you</li>
                <li>Compile anonymous statistical data and analysis for use internally.</li>
                <li>Administer promotions and contests.</li>
                <li>Create and manage your account.</li>
                <li>Assist law enforcement and respond to subpoena.</li>
                </ul>
            </p>
			<h2>DISCLOSURE OF YOUR INFORMATION</h2>
            <p>We do not disclose your information except in certain situations as follows:</p>
            <h3>By Law or to Protect Rights</h3>
            <p>
If we believe the release of information about you is necessary to respond to legal process, to investigate or remedy potential violations of our policies, or to protect the rights, property, and safety of others, we may share your information as permitted or required by any applicable law, rule, or regulation. This includes exchanging information with other entities for fraud protection and credit risk reduction.</p>
			<h3>Third-Party Service Providers</h3>
            <p>We may share your information with third parties that perform services for us or on our behalf, including payment processing, data analysis, email delivery, hosting services, customer service, and marketing assistance.
			<h3>Marketing Communications</h3>
			<p>With your consent, or with an opportunity for you to withdraw consent, we may share your information with third parties for marketing purposes, as permitted by law.</p>
			<h3>Online Postings</h3>
			<p>When you post comments, contributions or other content to the site, your posts may be viewed by all users and may be publicly distributed outside the site in perpetuity.</p>
			<h3>Third-Party Advertisers</h3>
			<p>We may use third-party advertising companies to serve ads when you visit the Site . These companies may use information about your visits to the site and other websites that are contained in web cookies in order to provide advertisements about goods and services of interest to you.</p>
			<h3>Business Partners</h3>
			<p>We may share your information with our business partners to offer you certain products, services or promotions.</p>
			<h3>Other Third Parties</h3>
			<p>We may share your information with advertisers and investors for the purpose of conducting general business analysis. We may also share your information for research purposes, as permitted by law.</p>

			<h3>Internet-Based Advertising</h3>
			<p>Additionally, we may use third-party software to serve ads on the site, implement email marketing campaigns, and manage other interactive marketing initiatives. This third-party software may use cookies or similar tracking technology to help manage and optimize your online experience with us.</p> 
			<h3>Website Analytics</h3>
			<p>We may also partner with selected third-party vendorsto allow tracking technologies and remarketing services on the site through the use of first party cookies and third-party cookies, to, among other things, analyze and track users’ use of the Site, determine the popularity of certain content and better understand online activity. By accessing the Site. you consent to the collection and use of your information by these third-party vendors. You are encouraged to review their privacy policy and contact them directly for responses to your questions. We do not transfer personal information to these third-party vendors.</p>
			
            <h2>THIRD-PARTY WEBSITES</h2>
            <p>The Site may contain links to third-party websites and applications of interest, including advertisements and external services, that are not affiliated with us. Once you have used these links to leave the site, any information you provide to these third parties is not covered by this Privacy Policy, and we cannot guarantee the safety and privacy of your information. Before visiting and providing any information to any third-party websites, you should inform yourself of the privacy policies and practices (if any) of the third party responsible for that website, and should take those steps necessary to, in your discretion, protect the privacy of your information. We are not responsible for the content or privacy and security practices and policies of any third parties, including other sites, services or applications that may be linked to or from the Site.</p>
			<h2>SECURITY OF YOUR INFORMATION</h2>
            <p>We use administrative, technical, and physical security measures to help protect your personal information. While we have taken reasonable steps to secure the personal information you provide to us, please be aware that despite our efforts, no security measures are perfect or impenetrable, and no method of data transmission can be guaranteed against any interception or other type of misuse. Any information disclosed online is vulnerable to interception and misuse by unauthorized parties. Therefore, we cannot guarantee complete security if you provide personal information.</p>
			<h2>POLICY FOR CHILDREN</h2>
            <p>We do not knowingly solicit information from or market to children under the age of 13. If you become aware of any data we have collected from children under age 13, please contact us using the contact information provided below.</p>
            <h2>POLICY FOR CHILDREN</h2>
            <p>We do not knowingly solicit information from or market to children under the age of 13. If you become aware of any data we have collected from children under age 13, please contact us using the contact information provided below.</p>
			<h2>OPTIONS REGARDING YOUR INFORMATION</h2>
			<p>You may at any time review or change the information in your account or terminate your account by:</p>
            <ul>
            <li>Logging into your account settings and updating your account</li>
            <li>Contacting us using the contact information provided below</li>
            </ul>
            <p>Upon your request to terminate your account, we will deactivate or delete your account and information from our active databases. However, some information may be retained in our files to prevent fraud, troubleshoot problems, assist with any investigations, enforce our Terms of Use and/or comply with legal requirements.</p>
            <h2>Emails and Communications</h2>
            <p>If you no longer wish to receive correspondence, emails, or other communications from us, you may opt-out by:</p>
            <ul>
            <li>Logging into your account settings and updating your preferences.</li>
            <li>Contacting us using the contact information provided below</li>
            </ul>
            <p>If you no longer wish to receive correspondence, emails, or other communications from third parties, you are responsible for contacting the third party directly.</p>

			<h2>CONTACT US</h2>
            <p>If you have questions or comments about this Privacy Policy, please contact us at:</p>
            Ecopharm (U) Ltd<br/>
            P.O Box 3074,<br/>
            Plot 576, Kira Road<br/>
            Ntinda, Kampala<br/>
            +256706900800<br/>
            info@ecopharmug.com<br/>


        </div>
    </div>
</div>