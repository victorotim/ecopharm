<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:type" content="product">
    <meta property="og:title" content="<?=(!empty($product_details['product_title'])? $product_details['product_title'] : '').' | '.SITE_TITLE?>">
    <meta name="keywords" content="pharmacy, product details, patients, online pharmacy, uganda, drug store" />
    <meta name="description" content="<?=(!empty($product_details['description'])? substr(strip_tags(html_entity_decode($product_details['description'])), 0, 150) : ''). ' | '. SITE_TITLE?>" />
    <meta property="og:url" content="<?=BASE_URL.'product/'.(!empty($product_details['slug'])? $product_details['slug'] : '')?>">
    <meta property="og:site_name" content="<?=SITE_TITLE?>">
    <meta property="product:price:amount" content="<?=(!empty($product_details['current_price'])? $product_details['current_price'] : '')?>">
    <meta property="product:price:currency" content="UGX">
    <meta property="product:availability" content="<?=(!empty($product_details['stock_status'])? $product_details['stock_status'] : '')?>">
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': '.(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li><?=(!empty($category['title'])? $category['title'] : '')?></li>
                    </ul>
                    <h1><?=(!empty($category['title'])? $category['title'] : '')?></h1>
                </div>
            </section>

            <section class="section-pad">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-9 col-md-8">
							<? if(!empty($product_details)): ?>
                            <div class="row product-summary">
                                <div class="col-md-6">
                                    <div class="d-block d-md-none">
                                        <h1 class="product-title">
											<?=(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?>
                                        </h1>
                                    <h2 class="product-price"> ush <?=(!empty($product_details['current_price'])? $product_details['current_price'] : '<i>undefined product</i>')?><span class="available">Available</span></h2>
                                    </div>
                                        <div class="product-img position-relative">
                                            <?php if($product_details['stock_status'] == 'out_of_stock'): ?>
                                                <div class="position-absolute p-1 w-auto out_of_stock">out of stock</div>
                                            <?php endif; ?>
                                            <img class="img-fluid" alt="<?=(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?>" src="<?=(!empty($product_details['photo_url'])? BASE_URL .'assets/uploads/' . $product_details['photo_url']: IMAGE_URL .'no-image-found.jpg')?>">
                                        </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="product-content">
                                    	<form method="post" id="frm-buy-now" class="simplevalidator" action="<?=base_url().'cart/checkout'?>" enctype="multipart/form-data">
                                            <h1 class="product-title d-none d-md-block"><?=(!empty($product_details['product_title'])? $product_details['product_title'] : '<i>undefined product</i>')?></h1>
                                            <h2 class="product-price d-none d-md-block"> ush <?=format_number(!empty($product_details['current_price'])? $product_details['current_price'] : '00')?><?=$product_details['stock_status'] != 'out_of_stock'? '<span class="available">Available</span>' : ''?></h2>
                                            <input id="product-id" name="product_id" value="<?=encrypt_value($product_details['product_id'])?>" style="display:none" />
                                            <p class="product-desc"><?=(!empty($product_details['description'])? html_entity_decode($product_details['description']) : '')?></p>
                                            <?php if($product_details['stock_status'] != 'out_of_stock'): ?>
                                            <div class="product-quantity">
                                                    <label>Quantity:</label>
                                                    <span class="dec qtybutton" role="button">-</span>
                                                    <input id="qty" name="qty" type="text" value="1">
                                                    <span class="inc qtybutton" role="button">+</span>
                                            </div>
                                            <div class="checkout-btn">
                                                <button class="add-to-cart" type="button" data-bs-toggle="modal" data-bs-target="#checkout-modal">
                                                	<a href="javascript:void(0);">Add to Cart</a>
                                               	</button>
                                                <button class="buy-now" type="submit" name="buy_now" value="buy_now">
                                                	Buy Now
                                              	</button>
                                            </div>
                                            <?php else: ?>
                                                <div>
                                                    <b>Sorry, this product is out of stock</b>
                                                </div>
                                            <?php endif; ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
							<? endif;?>
                        </article>
                        <aside class="col-lg-3 col-md-4 d-none d-md-block">
                            <div class="sidebar">
                                <h3 class="sidebar-title">Similar Products</h3>
                                <?php 
									if(!empty($related_products)):
										
										foreach($related_products as $related_product):

                                            $this->load->view('addons/public_product_widget', array('catalog_item' => $related_product, 'col_md'=>12));

										endforeach;

									endif; 
								?>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>                 
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 
 
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js'));?>
</body>
</html>