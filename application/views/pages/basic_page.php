<?php $msg = empty($msg)? get_session_msg($this): $msg; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': '.(!empty($page_title)? $page_title : '<i>undefined page</i>')?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">  

    
    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li><?=(!empty($page_title)? $page_title : '')?></li>
                    </ul>
                    <h1><?=(!empty($page_title)? $page_title : '')?></h1>
                </div>
            </section>

            <section class="section-pad">
                <div class="container">
                    <div class="row">
                        <article class="col-lg-9 col-md-8">
                            <div class="row product-summary">
                                <div class="col-md-12">
                                	<?php 
										if(!empty($msg)) echo "<div>".format_notice($this,$msg)."</div>";
										
										$this->load->view($page_to_load);
									?>
                                </div>                                
                            </div>
                        </article>
                        <aside class="col-lg-3 col-md-4 d-none d-md-block">
                            <div class="sidebar">
                                <h3 class="sidebar-title">Recent Products</h3>
                                <?php 
									if(!empty($recent_products)):
										
										foreach($recent_products as $recent_product):
											
											$img_url = (!empty($recent_product['photo_url'])? BASE_URL .'assets/uploads/' . $recent_product['photo_url']: IMAGE_URL .'no-image-found.jpg');
								?>
                                <a href="<?=base_url().'pages/product_details/p/'.encrypt_value($recent_product['product_id'])?>">
                                    <div class="product-box">
                                        <img src="<?=$img_url?>">
                                        <h3 class="product-price">Ush <?=format_number($recent_product['current_price'])?></h3>
                                        <p class="product-title"><?=$recent_product['product_title']?></p>
                                    </div>
                                </a>
                                <?php
										endforeach; 
									endif; 
								?>
                            </div>
                        </aside>
                    </div>
                </div>
            </section>                 
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?>
    <input type='hidden' id='layerid' name='layerid' value='' />
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js', 'ecopharm.fileform.js', 'ecopharm.js'));?>
    <script>
        $(".custom-file-input").on("change", function () {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
    </script>
</body>
</html>