
<div>
    <!-- 2 column grid layout with text inputs for the first and last names -->
    <div class="row mb-4">
        <div class="col">
            <p>We are Ecopharm Limited and have been in the pharmaceutical space for 11 years.</p>
            <p>Ecopharm Limited is Uganda’s fastest growing Pharmacy Retail chain currently located in strategic locations in and around Uganda’s Central business district.</p>
            <p>We are a professionally managed state-of-the-art pharmaceutical company. We have an established name, trust and track record in retailing of high quality branded and generic medicines.</p>
        </div>
    </div>
</div>