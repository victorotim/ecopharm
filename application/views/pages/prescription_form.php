<?php if((!empty($result) && $result !== true) || empty($result)): ?>
<div class="microform">
    <form class="prescription-form" method="post" class="simplevalidator" enctype="multipart/form-data" action="<?=base_url()?>pages/prescription">
        <div class="mb-3">
            <label for="name" class="form-label">Your Name <span class="required">*</span></label>
            <input type="text" name="customer_name" value="<?=!empty($formdata['customer_name'])? $formdata['customer_name'] : ''?>" class="form-control border">
        </div>
        <div class="mb-3">
            <label for="medication" class="form-label">Email Address<span class="required">*</span></label>
            <input type="email" name="email_address" value="<?=!empty($formdata['email_address'])? $formdata['email_address'] : ''?>" class="form-control border">
        </div>
        <div class="mb-3">
            <label for="email" class="form-label">Phone Number<span class="required">*</span></label>
            <input type="text" name="telephone_number" value="<?=!empty($formdata['telephone_number'])? $formdata['telephone_number'] : ''?>" class="form-control border">
        </div>
        <div class="mb-3">
            <label for="prescription" class="form-label">Prescribing Clinic<span class="required">*</span></label>
            <input type="text" name="prescribing_clinic" value="<?=!empty($formdata['prescribing_clinic'])? $formdata['prescribing_clinic'] : ''?>" class="form-control border">
        </div>
        <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile" name="prescription_file">
            <label class="custom-file-label" for="customFile">Upload prescription file <i class="icofont-clip"></i></label>
        </div>

        <div class="form-btn">
            <button type="submit" class="submit-btn">Submit Prescription</button>
        </div>
    </form>
</div>
<?php endif; ?>