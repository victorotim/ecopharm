<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="pharmacy, product category, medicine, doctor, patients" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="<?=SITE_TITLE.': '.(!empty($category['category_title'])? $product['category_title'] : '<i>undefined category</i>')?>" />
    
    <!-- FAVICONS ICON -->
    <link rel="shortcut icon" type="image/x-icon" href="<?=IMAGE_URL?>favicon.ico" />
    
    <!-- PAGE TITLE HERE -->
    <title><?=SITE_TITLE.': '.(!empty($category['category_title'])? $product['category_title'] : '<i>undefined category</i>')?></title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/fontawesome/css/all.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/fonts/icofont/icofont.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/shop.css">


    <!-- GOOGLE FONTS -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">

    
</head>

<body id="bg">
	<div class="page-wraper"> 
		<!-- HEADER START -->
       	<header class="site-header">
        	<!-- TOP BAR START -->
        	<div class="top-bar">
				<?php $this->load->view('addons/public_top_bar',array());?>
        	</div>
        	<!-- MAIN BAR START -->
        	<div class="menu-wrap">
            	<div class="container">                        
                	<!-- MAIN NAV -->
                	<nav class="navbar navbar-expand-lg">
                    	<div class="row w-100">
                        	<?php $this->load->view('addons/public_top_menu',array());?> 
                      	</div>
                   	</nav>
             	</div>
        	</div>
    	</header>
    	<!-- HEADER END -->
        
        <!-- CONTENT START -->
        <div class="page-content">
            <section class="page-title">
                <div class="container">
                    <ul class="breadcrumb">
                        <li>Shop</li>
                        <li><?=!empty($selected_category['category_title'])? $selected_category['category_title'] : ''?></li>
                    </ul>
                    <h1><?=!empty($selected_category['category_title'])? $selected_category['category_title'] : ''?></h1>
                </div>
            </section>

            <section class="section-pad">
                <div class="container">
                    <div class="row">
                        <aside class="col-lg-3 col-md-4 d-none d-sm-block">
                            <ul class="category-list">
                            	<?php
									 if(!empty($categories)):
									 	
										foreach($categories as $category_row): 
								?>
                                <li class="<?=!empty($selected_category) && $selected_category['category_id'] == $category_row['category_id']? 'fw-bold' : ''?>" style="position: relative;">
									<?=$category_row['category_title']?> 
                                    <span class="count">(<?=$category_row['num_of_products']?>)</span>
                                    <a href="<?=base_url().'product_category/'. $category_row['slug']?>" class="stretched-link">&nbsp;</a>
                                    <?=print_sub_categories($this, $category_row['category_id'], $selected_category['category_id'])?>
                              	</li>
                                <?php
										endforeach;
										
									endif;
								?>
                            </ul>
                        </aside>
                        <article class="col-lg-9 col-md-8">                        
                            <div class="row category_products">
                            	<?php
									 if(!empty($category_products)):
									 	
										foreach($category_products as $category_product):

                                            $this->load->view('addons/public_product_widget', array('catalog_item' => $category_product, 'col_md'=>3));

										endforeach;
										
									endif;
								?>
                            </div>
                            <?php if(!empty($number_of_products)): ?>
                            <nav aria-label="Product category navigation" class="mt-4">
                                <ul class="pagination product_category_pagination">
                                    <?php for ($i = 0; $i<($number_of_products / $products_per_page); $i++):?>
                                        <li class="page-item <?=($i==0? 'active' : '')?>"><a class="page-link" href="javascript:void(0);"><?=($i+1)?></a></li>
                                    <?php endfor; ?>
                                </ul>
                            </nav>
                            <?php endif; ?>
                        </article>
                    </div>
                </div>
            </section>                 
        </div>
        <!-- CONTENT END -->               
    </div>
    <?=$this->load->view('addons/checkout_modal')?> 
 
<?php echo minify_js('home', array('jquery-2.1.1.min.js', 'bootstrap.min.js', 'owl.carousel.min.js', 'main.js'));?>
</body>
</html>