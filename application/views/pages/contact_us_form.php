
<div class="microform">
    <div class="row mb-4" id="resultdiv" style="display: none">
        <div class="col">
            &nbsp;
        </div>
    </div>
    <!-- 2 column grid layout with text inputs for the first and last names -->
    <div class="row mb-4">
        <div class="col">
            <div class="form-outline">
                <label for="yourname" class="form-label">Your Name</label>
                <input type="text" id="yourname" name="yourname" class="form-control" placeholder="Your Name" <?php echo ($this->native_session->get('__first_name')? " value='".$this->native_session->get('__first_name').' '.$this->native_session->get('__last_name')."' style='font-weight:bold;' readonly": " value=''");?>/>
            </div>
        </div>
    </div>

    <!-- email and mobile number input -->
    <div class="row mb-4">
        <div class="col">
            <div class="form-outline">
                <label for="email" class="form-label">Email Address</label>
                <input type="email" id="email" name="emailaddress" class="form-control" value="" placeholder="Email address" />
            </div>
        </div>
        <div class="col">
            <div class="form-outline">
                <label for="telephone" class="form-label">Mobile Phone Number</label>
                <input type="number" id="telephone" name="telephone" class="form-control" value=""  placeholder="Mobile number e.g 0782345985"/>
            </div>
        </div>
    </div>

    <!-- Reason for contact -->
    <div class="row mb-4">
        <div class="col">
            <div class="form-outline">
                <label for="reason__contactreason" class="form-label">Message Subject</label>
                <input type="text" id="reason__contactreason" name="reason__contactreason" class="form-control" value="" placeholder="Enter or Select reason" />
            </div>
        </div>
    </div>

    <!-- Message -->
    <div class="row mb-4">
        <div class="col">
            <div class="form-outline">
                <label for="address-line-2" class="form-label">Message</label>
                <textarea class="form-control" id="details" name="details" rows="4" placeholder="Enter your message here"></textarea>
            </div>
        </div>
    </div>

    <!-- Submit button -->
    <div class="pt-1 form-btn mb-4">
        <button type="submit" class="submit-btn submitmicrobtn" name="send_message" value="send_message">Send message</button>
        <input type='hidden' id='action' name='action' value='<?php echo base_url().'pages/contact_us';?>' />
        <input type='hidden' id='redirectaction' name='redirectaction' value='<?php echo base_url(). 'pages/contact_us';?>' />
        <input type='hidden' id='resultsdiv' name='resultsdiv' value='' />
    </div>

</div>