<?php
/**
 * This class generates and formats account details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 07/30/2015
 */
class _account extends CI_Model
{
	
	# Verify a new user's account
	public function verify($code)
	{
		$user = $this->_query_reader->get_row_as_array('get_user_by_id', array('user_id'=>extract_id($code) ));
		# Mark the email and account as verified
		if(!empty($user['id'])) {
			$result = $this->_query_reader->run('update_user_value', array('field_name'=>'email_verified', 'field_value'=>'Y', 'user_id'=>$user['id']));
			if($result) $result = $this->_query_reader->run('update_user_field', array('user_id'=>$user['id'], 'field_name'=>'user_status', 'field_value'=>'active'));
		}
		
		return array('verified'=>(!empty($result) && $result? 'Y': 'N'));
	}
	
	
	
	
	# Check if the user name is available
	public function valid_user_name($desiredUserName)
	{
		return array('is_valid'=>($this->_query_reader->get_count('check_user_name', array('user_name'=>htmlentities($desiredUserName, ENT_QUOTES) )) > 0? 'N': 'Y'));
	}
	
	
	
	
	# Resend an account verification link
	public function resend_link($emailAddress, $userId, $baseLink)
	{
		$result = $this->_messenger->send($userId, array(
				'code'=>'account_verification_link',
				'emailaddress'=>$emailAddress,
				'verificationlink'=>$baseLink.'u/'.format_id($userId)
			),
			array('system', 'email'), 
			TRUE);
		return array('result'=>($result? 'SUCCESS': 'FAIL'));
	}
	
	
	
	
	# Login into the system
	public function login($userName, $password, $details=array())
	{
		$response = array('result'=>'FAIL', 'default_view'=>'', 'user_details'=>array(), 'permissions'=>array());
		
		# Check whether the userName and password match for the user - and get their user id
		$iam = $this->_query_reader->get_row_as_array('get_user_by_name_and_pass', array('login_name'=>$userName, 'login_password'=>sha1($password) ));

		# Get the user details if they exist
		if(!empty($iam['user_id'])) $user = $this->_query_reader->get_row_as_array('get_user_by_id', array('user_id'=>$iam['user_id']));

		# Collect the rest of the user details if login is successful
		if(!empty($user))
		{
			$response['result'] = 'SUCCESS';
			# Default view on login
			$response['default_view'] = $this->get_default_view($user['group_type']);
			
			# User details
			$response['user_details'] = array(
					'user_id'=>$user['id'], 
					'first_name'=>$user['first_name'], 
					'last_name'=>$user['last_name'], 
					'email_address'=>$user['email_address'], 
					'email_verified'=>$user['email_verified'],
					'telephone'=>$user['telephone'],
                    'otherphone'=>$user['otherphone'],
                    'address_line_1'=>$user['address'],
                    'address_line_2'=>$user['address_line_2'],
                    'telephone_carrier'=>(!empty($user['telephone_carrier'])? $user['telephone_carrier']: ''),
					'photo_url'=>(!empty($user['photo_url'])? BASE_URL.'assets/uploads/'.$user['photo_url']: ''),
					'user_type'=>$user['group_type'],
					'permission_group_name'=>$user['group_type'],
					'permission_group'=>$user['permission_group_id']
					
			);
			
			# The allowed permissions for the user
			$response['permissions'] = $this->_query_reader->get_single_column_as_array('get_user_permissions', 'permission_code', array('user_id'=>$user['id']));
		}
		
		# Log the login attempt event
		$this->_logger->add_event(array(
			'user_id'=>(!empty($user['id'])? $user['id']: 'username='.$userName), 
			'activity_code'=>'login', 
			'result'=>(!empty($user['id'])? 'SUCCESS':'FAIL'), 
			'log_details'=>"username=".$userName."|device=".(!empty($details['device'])? $details['device']: 'unknown')."|browser=".(!empty($details['browser'])? $details['browser']: 'unknown'),
			'uri'=>(!empty($details['uri'])? $details['uri']: ''),
			'ip_address'=>(!empty($details['ip_address'])? $details['ip_address']: '')
		));
		
		
		return $response;
	}
	
	
	
	
	
	
	# Logout of the system
	public function logout($userId, $details=array())
	{
		# Log the logout attempt event
		$this->_logger->add_event(array(
			'user_id'=>(!empty($userId)? $userId: ''), 
			'activity_code'=>'logout', 
			'result'=>'SUCCESS', 
			'log_details'=>"device=".(!empty($details['device'])? $details['device']: 'unknown')."|browser=".(!empty($details['browser'])? $details['browser']: 'unknown'),
			'uri'=>(!empty($details['uri'])? $details['uri']: ''),
			'ip_address'=>(!empty($details['ip_address'])? $details['ip_address']: '')
		));
	}
	
	
	
	# Get the default view to redirect the user on login
	function get_default_view($groupType)
	{
		$view = '';
		if(!empty($groupType)){
			switch($groupType){
				case 'admin':
					$view = 'accounts/admin_dashboard';
				break;
								
				case 'client':
				default:
					$view = 'orders/manage';
			}
		}
		
		return $view;
	}
	
	
	
	
	# Function to send a password recovery link
	function send_password_link($email_address)
	{
		$msg = '';
		$user = $this->_query_reader->get_row_as_array('get_user_by_email',array('email_address'=>$email_address));
		
		if(!empty($user['user_id'])){
			#Generate a reset token, use _payment model to general rabdom characters
            $this->load->model('_payment');
            $token = $this->_payment->get_reference_id();

            # save the token to the db
            $token_result = $this->_query_reader->add_data('add_password_reset_token', array('token'=>$token, 'user_id'=>$user['user_id'], 'dateadded'=>date('Y-m-d H:i:s')));

            if($token_result){
                $sendResult = $this->_messenger->send($user['user_id'], array(
                    'first_name'=>$user['first_name'],
                    'code'=>'password_recovery_link',
                    'security_email'=>SECURITY_EMAIL,
                    'recovery_link'=>base_url().'accounts/password_recovery/token/'.encrypt_value($token)
                ), array('system', 'email'), true);
            } else $msg = 'The password reset token could not be generated. Please try again';
		}
		else $msg = 'The user with the entered email address does not exist.';
		
		return array('result'=>(!empty($sendResult) && $sendResult? true: false), 'msg'=>$msg);
	}



    # Verify password recovery token
    function verify_password_recovery_token($token)
    {
        $result = false;
        $msg = '';

        $token_data = $this->db->get_where('account_reset', array('token'=>$token))->row_array();
        $token_age = format_date_interval($token_data['dateadded'], date('Y-m-d H:i:s'), true);

        if (empty($token_data)):

            $msg = 'Invalid recovery token';

        elseif ((!empty($token_age['minutes']) && $token_age['minutes'] > 15) || !empty($token_age['years']) || !empty($token_age['months']) || !empty($token_age['months']) || !empty($token_age['days']) || !empty($token_age['hours']) || $token_data['isactive'] == 'N'):

            $msg = 'Password recovery link has expired';

        else:
            $result = true;

        endif;

        return array('result'=>$result, 'msg'=>$msg);
    }



    # Verify password recovery token
    function password_recovery($new_password, $recovery_token)
    {
        $result = false;
        $msg = '';

        $token_verification_result = $this->verify_password_recovery_token($recovery_token);

        if($token_verification_result['result']):

            $token_data = $this->db->get_where('account_reset', array('token'=>$recovery_token))->row_array();

            $this->db->update('users', array('password'=>sha1($new_password)), array('id'=>$token_data['_user_id']));

            $result = $this->db->affected_rows();

            if($result):
                $this->db->update('account_reset', array('isactive'=>'N'), array('token'=>$recovery_token));
            else:
                $msg = 'The password could not be updated';
            endif;

        else:
            $msg = $token_verification_result['msg'];
        endif;

        return array('result'=>$result, 'msg'=>$msg);
    }
	
	
	
	
	
	
	
	
	# Get a list of accounts of a given type
	function types($type, $restrictions=array())
	{
		return $this->_query_reader->get_single_column_as_array('get_accounts_of_type', '_user_id', array('account_type'=>$type));
	}
	
	
	
	# Get user audit trail
	function audit_trail($scope = array('date'=>'', 'user_id'=>'', 'activity_code'=>'', 'phrase'=>'', 'offset'=>'0', 'limit'=>NUM_OF_ROWS_PER_PAGE))
	{
		return $this->_query_reader->get_list('get_audit_trail', array(
			'date_condition'=>(!empty($scope['date'])? " AND DATE(event_time) = DATE('".date('Y-m-d', strtotime(make_us_date($scope['date'])))."')": ''),
			'user_condition'=>(!empty($scope['user_id'])? " AND user_id='".$scope['user_id']."' ": ''),
			'activity_condition'=>(!empty($scope['activity_code'])? " AND activity_code='".$scope['activity_code']."' ": ''),
			'phrase_condition'=>(!empty($scope['phrase'])? " AND MATCH(log_details) AGAINST ('+\"".htmlentities($scope['phrase'], ENT_QUOTES)."\"') ": ''),
			'limit_text'=>" LIMIT ".$scope['offset'].",".$scope['limit']." "
		));
	}
	
	
}


?>