<?php
/**
 * This class generates and formats bid details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/15/2015
 */
class _catalog extends CI_Model
{
	# advanced search list of products
	function lists($instructions)
	{
		$limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;
		
		$start = !empty($instructions['page'])? $instructions['page']: 0;

		$search_string = (!empty($instructions['search_string'])? ' AND '. $instructions['search_string'] : '');

		$order_by = (!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY last_updated DESC');

		$limit_text = 'LIMIT ' . $start.','.($limit);

		# User just needs count of items
		if(!empty($instructions['get_count'])):

		    return  $this->_query_reader->get_count('get_product_list', array('search_string'=>$search_string, 'order_by'=>'', 'limit_text'=>''));

        else:

            return  $this->_query_reader->get_list('get_product_list', array('search_string'=>$search_string, 'order_by'=>$order_by, 'limit_text'=>$limit_text));

        endif;

	}


    # get count of products
    function get_count($instructions)
    {
        # add get count instruction to instructions
        $instructions['get_count'] = true;

        return  $this->lists($instructions);

    }


	# get union list of products and categories
    function lists_products_union_categories ($instructions)
    {
        $limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;

        $start = !empty($instructions['page'])? ($instructions['page']-1)*$limit: 0;

        return $this->_query_reader->get_list('get_products_union_categories', array(

            'search_string'=>(!empty($instructions['search_string'])? ' AND '. $instructions['search_string'] : ''),

            'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY dateadded DESC'),

            'limit_text'=>'LIMIT ' . $start.','.($limit+1)
        ));
    }
		
	
	# add a product
	function add($data)
	{
		$result = FALSE;
		
		$author = $this->native_session->get('__user_id');

		$msg = '';

		# check if similar product exists
        $existing_product = $this->db->get_where('products', array('title'=>$data['product_title']))->row_array();

        if(!empty($existing_product) && (empty($data['productid']) || (!empty($data['productid']) && $existing_product['id'] != $data['productid']))):

            $msg = 'A similar product exists in the system catalog';

        else:

            # if existing product title has changed, create a new slug
            if(!empty($data['productid']) && $existing_product['title'] == $data['product_title']):

                $slug = $existing_product['slug'];

            else:

                $slug = product_slug($this, $data['product_title']);

            endif;

            # a) save the main record
            $product_id = $this->_query_reader->add_data((!empty($data['productid'])? 'update': 'add').'_product', array(
                    'title'=>$data['product_title'],
                    'slug'=>$slug,
                    'description'=>htmlentities($data['product_description'], ENT_QUOTES),
                    'unit_quantity'=>!empty($data['quantity_per_unit'])? $data['quantity_per_unit'] : 0,
                    'status'=>empty($data['stock_status'])? 'out_of_stock' : $data['stock_status'],
                    'isactive'=>empty($data['isactive'])? 'N' : $data['isactive'],
                    'author'=>$author,
                    'product_id'=>(!empty($data['productid'])? $data['productid']: 0)
                ));

            # mark the result as true if the product already exists and update is successful, or new record is added
            if((!empty($data['productid']) && $this->db->affected_rows()) || $product_id) $result = TRUE;


            # save the photo and price records
            if($result || !empty($data['productid'])) {

                if(!empty($data['productid'])) $product_id = $data['productid'];

                # Update product price
                if(isset($data['current_price'])):

                    $this->_query_reader->run('update_product_price', array(
                        'author'=>$author,
                        'current_price'=>$data['current_price'],
                        'product_id'=>$product_id
                    ));

                    if($this->db->affected_rows()):
                        $this->db->where('price !='. $data['current_price']);
                        $this->db->where(array('_product_id'=>$product_id));
                        $this->db->update('product_prices', array('is_live'=>'N'), array('_product_id'=>$product_id));
                    endif;

                endif;

                # adding the new photos
                if(!empty($data['photos'])):
                    $display_order = 1;

                    foreach($data['photos'] AS $photo):

                        $result = $this->_query_reader->run('add_product_photo', array('product_id'=>$product_id, 'url'=>$photo, 'description'=>'', 'author'=>$author, 'display_order'=>$display_order));

                        $display_order++;

                    endforeach;

                endif;

                # Add product category
                if(!empty($data['category'])):
                    $data['category'] = explode(',', $data['category']);
                    # Delete saved categories not in selected array
                    $this->db->where_not_in('_product_category_id', $data['category']);
                    $this->db->where(array('_product_id'=>$product_id));
                    $this->db->delete('product_category_mapping');

                    foreach($data['category'] AS $category):

                        $result = $this->_query_reader->run('attach_product_category', array('product_id'=>$product_id, 'category_id'=>$category, 'author'=>$author));

                    endforeach;

                else:

                    # user deleted all product category mappings
                    $this->db->where(array('_product_id'=>$product_id));
                    $this->db->delete('product_category_mapping');

                endif;
            }

		endif;
		
		# log action
		$this->_logger->add_event(array(
			'user_id'=>$author, 
			'activity_code'=>(!empty($data['productid'])? 'update': 'add').'_product', 
			'result'=>($result? 'SUCCESS': 'FAIL'), 
			'log_details'=>"username=".$this->native_session->get('__email_address')."|device=".get_user_device()."|browser=".$this->agent->browser(),
			'uri'=>uri_string(),
			'ip_address'=>get_ip_address()
		));
		
		return array('boolean'=>$result, 'reason'=>$msg);
	}
		
	
	function remove_photo($photo_id)
	{
		$result = false;
		
		# get photo details			
		$photo_details = $this->db->get_where('photos', array('id'=>$photo_id))->row_array();
		
		if(!empty($photo_details)):
		
			# remove the file from disk
			@unlink(UPLOAD_DIRECTORY.$photo_details['url']);
			
			$this->db->delete('photos', array('id'=>$photo_id));
			
			$result = $this->db->affected_rows();
					
		endif;
		
		return $result;
	}	
	
	
	# view product details
	function product_details($product_id)
	{
		$instruction['search_string'] = 'P.id = "'. $product_id .'"';
		
		$product_details = $this->lists($instruction);
		
		$product_details = end($product_details);
		
		return $product_details;
	}
		
	
	
	# update product status
	function update_status($newStatus, $productIds)
	{
		$msg = '';
		$productIds = implode("','",$productIds);
		
		# Remove the product record completely
		if($newStatus == 'delete'):
			 $result = $this->_query_reader->run('delete_product_record', array('product_ids'=>$productIds));
		
		# Simply change the stock status
		elseif(in_array($newStatus, array('out_of_stock', 'available'))): 
			
			$result = $this->_query_reader->run('update_stock_status', array('new_status'=>$newStatus, 'product_ids'=>$productIds, 'user_id'=>$this->native_session->get('__user_id') ));
		
		# Change whether product is activated or not
		elseif(in_array($newStatus, array('activate', 'deactivate'))):
			$status = array('activate'=>'Y', 'deactivate'=>'N');
			$result = $this->_query_reader->run('update_product_status', array('new_status'=>$status[$newStatus], 'product_ids'=>$productIds, 'user_id'=>$this->native_session->get('__user_id') ));
		endif;
		
		# log action
		$this->_logger->add_event(array(
			'user_id'=>$this->native_session->get('__user_id'), 
			'activity_code'=>'product_status_change', 
			'result'=>($result? 'SUCCESS': 'FAIL'), 
			'log_details'=>"newstatus=".$newStatus."|device=".get_user_device()."|browser=".$this->agent->browser(),
			'uri'=>uri_string(),
			'ip_address'=>get_ip_address()
		));
		
		return array('boolean'=>$result, 'reason'=>$msg);
	}
	
}
?>