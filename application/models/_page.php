<?php
/**
 * This class generates and formats public page details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/10/2015
 */
class _page extends CI_Model
{
	
	# send contact message
	public function send_contact_message($formDetails)
	{
		$message['code'] = 'website_contact_message';
		$message['yourname'] = htmlentities($formDetails['yourname'], ENT_QUOTES);
		$message['senderemailaddress'] = $formDetails['emailaddress'];
		$message['sendertelephone'] = (!empty($formDetails['telephone'])? $formDetails['telephone']: '');
		$message['sendtime'] = date(FULL_DATE_FORMAT, strtotime('now'));
		$message['messagesubject'] = htmlentities($formDetails['reason__contactreason'], ENT_QUOTES);
		$message['messagedetails'] = htmlentities($formDetails['details'], ENT_QUOTES);
			
		return $this->_messenger->send_direct_email($formDetails['emailaddress'], '', $message);
	}

	# upload prescription details
	function add_prescription($data)
    {
        $result = FALSE;

        $author = $this->native_session->get('__user_id');

        # a) save the main record
        $prescription_id = $this->_query_reader->add_data((!empty($data['prescription_id'])? 'update': 'add').'_prescription', array(
            'customer_id'=>$data['customer_id'],
            'email_address'=>$data['email_address'],
            'telephone_number'=>$data['telephone_number'],
            'prescribing_clinic'=>$data['prescribing_clinic'],
            'file_url'=>!empty($data['file_url'])? $data['file_url'] : '',
            'instructions'=>$data['instructions'],
            'prescription_id'=>(!empty($data['prescription_id'])? $data['prescription_id']: 0),
            'author'=>$author
        ));

        # mark the result as true if the prescription already exists and update is successful, or new record is added
        if (!empty($data['productid']) && $this->db->affected_rows()) $result = TRUE;
        elseif ($prescription_id) $result = $prescription_id;

        return $result;
    }


    # Notify customer about new prescription order has been received
    function notify_user_prescription_received ($prescription_id)
    {
        $result = false;

        $prescription_summary = $this->_query_reader->get_row_as_array('get_prescription_list', array('search_string' => 'p.id="'. $prescription_id .'"', 'order_by'=>'', 'limit_text'=>''));

        if (!empty($prescription_summary)):

            $message = array(
                'first_name' => $prescription_summary['first_name'],
                'customer_name' => $prescription_summary['first_name'] .', '. $prescription_summary['last_name'],
                'phone_number' => $prescription_summary['telephone_number'],
                'prescribing_clinic' => $prescription_summary['prescribing_clinic'],
                'email_address'=> $prescription_summary['customer_address'],
                'help_email' => HELP_EMAIL,
                'site_general_name' => SITE_GENERAL_NAME,
                'order_history_link' => base_url() . 'orders/manage',
                'fileurl'=> UPLOAD_DIRECTORY . $prescription_summary['file_url'],
                'code' => 'prescription_order_received',
                'system' => 'required'
            );

            $result = $this->_messenger->send($prescription_summary['customer_id'], $message, array('system', 'email'), true);

        endif;

        return $result;
    }




    # Notify admins about new prescription order
    function notify_admin_prescription_received ($prescription_id)
    {
        $result = false;

        $sys_admins = $this->_user->lists(array('search_string' => 'U._permission_group_id = 1'));

        $prescription_summary = $this->_query_reader->get_row_as_array('get_prescription_list', array('search_string' => 'p.id="' . $prescription_id . '"', 'order_by' => '', 'limit_text' => ''));

        if (!empty($prescription_summary) && !empty($sys_admins)):

            $sys_admin_emails = [];

            # CC all admins, skip first admin since they will be main recipient
            for ($i = 0; $i < count($sys_admins); $i++)
                if ($i > 0) $sys_admin_emails[] = $sys_admins[$i]['email_address'];

            $message = array(
                'first_name' => $prescription_summary['first_name'],
                'customer_name' => $prescription_summary['first_name'] . ', ' . $prescription_summary['last_name'],
                'phone_number' => $prescription_summary['telephone_number'],
                'prescribing_clinic' => $prescription_summary['prescribing_clinic'],
                'help_email' => HELP_EMAIL,
                'site_general_name' => SITE_GENERAL_NAME,
                'order_history_link' => base_url() . 'orders/manage',
                'code' => 'new_prescription_order_submitted',
                'system' => 'required',
                'fileurl'=> UPLOAD_DIRECTORY . $prescription_summary['file_url'],
                'cc' => implode(',', $sys_admin_emails)
            );

            $result = $this->_messenger->send($sys_admins[0]['user_id'], $message, array('system', 'email'), true);

        endif;

        return $result;
    }
}


?>