<?php
/**
 * This class generates and formats order details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/15/2015
 */
class _order extends CI_Model
{
    private $payment_statuses = array('awaiting payment', 'paid', 'canceled', 'refunded');

    private $fulfillment_statuses = array('awaiting processing', 'processing order', 'awaiting delivery', 'dispatched', 'delivered', 'delivery canceled', 'returned');

    private $status_email_templates = array('awaiting processing' => 'awaiting_processing',
        'processing order' => 'processing_order',
        'awaiting delivery' => 'awaiting_delivery',
        'dispatched' => 'dispatched',
        'delivered' => 'order_delivered',
        'delivery canceled' => 'delivery_canceled',
        'returned' => 'order_returned',
        'awaiting payment' => 'awaiting_payment',
        'canceled' => 'payment_canceled',
        'refunded' => 'payment_refunded',
        'paid' => 'payment_received');

    function get_payment_statuses ()
    {
        return $this->payment_statuses;
    }

    function get_fulfillment_statuses ()
    {
        return $this->fulfillment_statuses;
    }

	# advanced search list of orders
	function lists($instructions)
	{
		$limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;
		
		$start = !empty($instructions['page'])? ($instructions['page']-1): 0;
		
		$search_string = (!empty($instructions['search_string'])? ' AND ('. $instructions['search_string'] .')' : '');
		
		if($this->native_session->get('__user_id') && $this->native_session->get('__user_type') != 'admin'):
			
			$search_string .= ' AND C.id = "'. $this->native_session->get('__user_id') .'"';
			
		endif;
		
		return $this->_query_reader->get_list('get_order_list', array(
			
			'search_string'=>$search_string,
			
			'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY O.last_updated DESC'),
						
			'limit_text'=>'LIMIT ' . $start.','.($limit+1)
		));
	}
		
	
	# add an order
	function add($data)
	{
		$result = FALSE;

		$author = 0;
		if($this->native_session->get('__user_id')) $author = $this->native_session->get('__user_id');
		elseif (!empty($data['customer_id'])) $author = $data['customer_id'];
		$data['author'] = $author;
		
		# a) save the main record
		// Start transaction from here
		$this->db->trans_start();

		# Update the user's default address and other phone details if different from session, reset session variable if db update is successful
        if(!empty($data['address_line_1']) && $data['address_line_1'] != $this->native_session->get('__address_line'))
            if($this->db->update('users', array('address_line_1'=>$data['address_line_1']), array('id'=>$author)))
                $this->native_session->set('__address_line', $data['address_line_1']);


        if(!empty($data['address_line_2']) && $data['address_line_2'] != $this->native_session->get('__address_line'))
            if($this->db->update('users', array('address_line_2'=>$data['address_line_2']), array('id'=>$author)))
                $this->native_session->set('__address_line_2', $data['address_line_2']);


        if(!empty($data['other_telephone_number']) && $data['other_telephone_number'] != $this->native_session->get('__otherphone'))
            if($this->db->update('users', array('address_line_2'=>$data['address_line_2']), array('id'=>$author)))
                 $this->native_session->set('__otherphone', $data['other_telephone_number']);

        $data['dateadded'] = date('Y-m-d H:i:s');
		$order_id = $this->_query_reader->add_data((!empty($data['orderid'])? 'update': 'add').'_order', $data);
		
		# mark the result as true if the product already exists and update is successful, or new record is added
		if((!empty($data['orderid']) && $this->db->affected_rows()) || $order_id) $result = TRUE;

		# add the cart items
		if($result || !empty($data['orderid'])) {
		    
			if(!empty($data['orderid'])) $order_id = $data['orderid'];

			$this->add_order_items($order_id, $data['cart_items']);
			
			# Update the order fulfillment and payment status
			# default to pending on new orders
			if(empty($data['orderid'])):
				
				$this->db->insert('order_fulfillment_status', array('_order_id'=>$order_id, 'status'=>'awaiting processing', 'comments'=>'user placed order', '_author'=>$author, '_last_updated_by'=>$author));

			    $payment_status = (!empty($data['momo_status']) && $data['momo_status'] == 'SUCCESSFUL'? 'paid' : 'awaiting payment');
				$this->db->insert('order_payment_status', array('_order_id'=>$order_id, 'status'=>$payment_status, 'comments'=>'user placed order', '_author'=>$author, '_last_updated_by'=>$author));

                # send email to user if payment is successful
                if($payment_status == 'paid') $this->communicate_status_change('paid', array('order_id'=>$order_id));
				
			endif;
			
		}
		
		// if no errors were detected and there was an effect
		// finish the transaction and finally commit the changes to the database
				
		$this->db->trans_complete();
		$this->db->trans_commit();
		
		# log action
		$this->_logger->add_event(array(
			'user_id'=>$author, 
			'activity_code'=>(!empty($data['orderid'])? 'update': 'add').'_order',
			'result'=>($result? 'SUCCESS': 'FAIL'), 
			'log_details'=>"device=".get_user_device()."|browser=".$this->agent->browser(),
			'uri'=>uri_string(),
			'ip_address'=>get_ip_address()
		));
		
		return array('boolean'=>$result, 'order_id'=>$order_id, 'reason'=>'');
	}
		
	
	# Updates products associated with an order
	function add_order_items ($order_id, $order_items)
	{
		$result = false;
		
		$author = $this->native_session->get('__user_id');

		if(!empty($order_items)):
		
			$cart_items = array();
				
			foreach($order_items as $order_item) 
				array_push($cart_items, 
					array('_order_id'=>$order_id, 
						  '_product_id'=>$order_item['product_id'],
						  'price'=>$order_item['price'],
						  'quantity'=>$order_item['qty'],
						  '_author'=>$author,
						  '_last_updated_by'=>$author,
                          'dateadded'=>date('Y-m-d H:i:s'),
                          'dateadded'=>date('Y-m-d H:i:s')));
						  
			$this->db->insert_batch('order_details', $cart_items);
			
			$result = $this->db->affected_rows();
				
		endif;
		
		return $result;
	}	
	
	
	# view order summary
	function summary ($order_id)
	{			
		$instruction['search_string'] = 'O.id = "'. $order_id .'"';
		
		$order_summary = $this->lists($instruction);

        $order_summary = end($order_summary);
		
		return $order_summary;
	}


    # advanced search list of all order details
    function all_order_details ($instructions)
    {
        $limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;

        $start = !empty($instructions['page'])? ($instructions['page']-1)*$limit: 0;

        $search_string = (!empty($instructions['search_string'])? ' AND '. $instructions['search_string'] : '');

        if($this->native_session->get('__user_id') && $this->native_session->get('__user_type') != 'admin'):

            $search_string .= ' AND O.customer_id = "'. $this->native_session->get('__user_id') .'"';

        endif;

        return $this->_query_reader->get_list('get_order_details', array(

            'search_string'=>$search_string,

            'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY OD.last_updated DESC'),

            'limit_text'=>'LIMIT ' . $start.','.($limit+1)
        ));
    }


    # view products added to the order
    function details ($order_id)
    {
        $instruction['search_string'] = 'OD._order_id = "'. $order_id .'"';

        return $this->all_order_details($instruction);
    }


    # Return an order's payment and fulfillment status
    function get_order_status($order_id)
    {
        $status = array();

        $order_details = $this->summary($order_id);

        if(!empty($order_details)) $status = array('payment_status'=>$order_details['payment_status'], 'fulfillment_status'=>$order_details['fulfillment_status']);

        return $status;
    }

	
	# update order status
	function update_status($data)
	{
		$msg = '';
		$payment_status_result = 0;
		$fulfillment_status_result = 0;

		$author = $this->native_session->get('__user_id');

		$order_id = $data['order_id'];

        # get current order status
		$current_status = $this->get_order_status($order_id);

		$status_data['_order_id'] = $order_id;
		$status_data['_author'] = $author;
		$status_data['_last_updated_by'] = $author;

		# Update if the current status is different
		if($current_status['payment_status'] != $data['payment_status'] && !empty($data['payment_status'])):

            $payment_status_result = $this->db->insert('order_payment_status', array_merge($status_data, array('status'=>$data['payment_status'])));

		    # notify user
            if($payment_status_result ) $this->communicate_status_change($data['payment_status'], $data);

            # log action
            $this->_logger->add_event(array(
                'user_id'=>$author,
                'activity_code'=>'order_payment_status_change',
                'result'=>($payment_status_result? 'SUCCESS': 'FAIL'),
                'log_details'=>"newstatus=".$data['payment_status']."|device=".get_user_device()."|browser=".$this->agent->browser(),
                'uri'=>uri_string(),
                'ip_address'=>get_ip_address()
            ));

		endif;

		if($current_status['fulfillment_status'] != $data['fulfillment_status'] && !empty($data['fulfillment_status'])):

            $fulfillment_status_result = $this->db->insert('order_fulfillment_status', array_merge($status_data, array('status'=>$data['fulfillment_status'])));

            # notify user
            if($fulfillment_status_result) $this->communicate_status_change($data['fulfillment_status'], $data);

            # log action
            $this->_logger->add_event(array(
                'user_id'=>$author,
                'activity_code'=>'order_fulfillment_status_change',
                'result'=>($fulfillment_status_result? 'SUCCESS': 'FAIL'),
                'log_details'=>"newstatus=".$data['fulfillment_status']."|device=".get_user_device()."|browser=".$this->agent->browser(),
                'uri'=>uri_string(),
                'ip_address'=>get_ip_address()
            ));

		endif;

		$result = $payment_status_result + $fulfillment_status_result;

		$msg = $result? 'The order status has been updated' : 'ERROR: The order status could not be updated';

		return array('boolean'=>$result, 'reason'=>$msg);
	}


	# Notify users about status changes
    function communicate_status_change($new_status, $order_summary = array())
    {
        $result = false;

        # Get the template
        $email_template = !empty($this->status_email_templates[$new_status])? $this->status_email_templates[$new_status] : '';

        if(!empty($order_summary['order_id']) && !empty($email_template)):

            if(!empty($order_summary['order_details'])):
                $order_details = $order_summary['order_details'];
            else:
                $order_details = $this->details($order_summary['order_id']);
            endif;

            if(empty($order_summary['dateadded'])):
                $order_summary = $this->summary($order_summary['order_id']);
            endif;

            $order_details_html = $this->load->view('cart/order_details_confirmation_template',
                array('order_details'=>$order_details, 'order_summary'=>$order_summary), true);

            $message = array(
                'first_name'=>$order_summary['customer_first_name'],
                'order_no'=>$this->_util->encode($order_summary['order_id']) ,
                'new_status'=>$new_status,
                'dateadded'=>date('D, M d, Y ', strtotime($order_summary['dateadded'])) .' at '. date('H:iA ', strtotime($order_summary['dateadded'])),
                'help_email'=>HELP_EMAIL,
                'site_general_name'=>SITE_GENERAL_NAME,
                'order_history_link'=>base_url().'orders/manage',
                'order_details'=>$order_details_html,
                'code'=>$email_template,
                'system'=>'required'
            );

            $result = $this->_messenger->send($order_summary['customer_id'], $message, array('system', 'email'), true);

        endif;

        return $result;
    }


    # order received email
    function send_order_received_email($order_id)
    {
        $data['order_summary'] = $order_summary = $this->summary($order_id);
        $data['order_details'] = $order_details = $this->details($order_id);

        $order_details_html = $this->load->view('cart/order_details_confirmation_template', $data, true);

        $message = array(
            'first_name'=>$order_summary['customer_first_name'],
            'order_no'=>$this->_util->encode($order_summary['order_id']) ,
            'payment_status'=>'Awaiting Payment',
            'dateadded'=>date('D, M d, Y ', strtotime($order_summary['dateadded'])) .' at '. date('H:iA ', strtotime($order_summary['dateadded'])),
            'fulfillment_status'=>'Awaiting Processing',
            'help_email'=>HELP_EMAIL,
            'site_general_name'=>SITE_GENERAL_NAME,
            'order_history_link'=>base_url().'orders/manage',
            'order_details'=>$order_details_html,
            'customer_address'=>$order_summary['region_name'] . '<br/>' . $order_summary['town_name'] .'<br/>'. $order_summary['address'] . '<br/>' . $order_summary['additional_address_info'],
            'code'=>'order_confirmation',
            'system'=>'required'
        );

        return $this->_messenger->send($order_summary['customer_id'], $message, array('system', 'email'), true);
    }




    # Notify admins about new order
    function notify_admin_new_order ($order_id)
    {
        $result = false;

        $sys_admins = $this->_user->lists(array('search_string'=>'U._permission_group_id = 1'));

        if (!empty($sys_admins)):
            $sys_admin_emails = [];

            # CC all admins, skip first admin since they will be main recipient
            for ($i = 0; $i < count($sys_admins); $i++)
                if ($i > 0) $sys_admin_emails[] = $sys_admins[$i]['email_address'];

            $data['order_summary'] = $order_summary = $this->summary($order_id);
            $data['order_details'] = $order_details = $this->details($order_id);

            $order_details_html = $this->load->view('cart/order_details_confirmation_template', $data, true);

            $message = array(
                'first_name' => $order_summary['customer_first_name'],
                'last_name' => $order_summary['customer_last_name'],
                'customer_email_address' => $order_summary['customer_email_address'],
                'customer_phone_number' => $order_summary['phone_number'],
                'billing_info' => $order_summary['region_name'] . '<br/>' . $order_summary['town_name'] .'<br/>'. $order_summary['address'] . '<br/>' . $order_summary['additional_address_info'],
                'payment_method' => $order_summary['payment_method'],
                'order_no' => $this->_util->encode($order_summary['order_id']),
                'payment_status' => 'Awaiting Payment',
                'dateadded' => date('D, M d, Y ', strtotime($order_summary['dateadded'])) . ' at ' . date('H:iA ', strtotime($order_summary['dateadded'])),
                'fulfillment_status' => 'Awaiting Processing',
                'help_email' => HELP_EMAIL,
                'site_general_name' => SITE_GENERAL_NAME,
                'base_url' => base_url(),
                'order_details' => $order_details_html,
                'code' => 'new_order',
                'system' => 'required',
                'cc' => implode(',', $sys_admin_emails)
            );

            $result = $this->_messenger->send($sys_admins[0]['user_id'], $message, array('system', 'email'), true);


        endif;

        return $result;
    }


    # get statistics by products
    function get_top_products($instructions)
    {
        $limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;

        $start = !empty($instructions['page'])? ($instructions['page']-1)*$limit: 0;

        $search_string = (!empty($instructions['search_string'])? $instructions['search_string'] : 'OD.isactive="Y"');

        $having_string = (!empty($instructions['having_string'])? ' AND '. $instructions['having_string'] : '');;

        if($this->native_session->get('__user_id') && $this->native_session->get('__user_type') != 'admin'):

            $search_string .= ' AND C.id = "'. $this->native_session->get('__user_id') .'"';

        endif;

        return $this->_query_reader->get_list('get_top_products', array(

            'having_string'=>$having_string,

            'search_string'=>$search_string,

            'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY total_qty_ordered DESC'),

            'limit_text'=>'LIMIT ' . $start.','.($limit+1)
        ));
    }


    # get statistics by order
    function get_order_statistics($instructions)
    {
        $limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;

        $start = !empty($instructions['page'])? ($instructions['page']-1)*$limit: 0;

        $search_string = (!empty($instructions['search_string'])? $instructions['search_string'] : '1=1');

        $having_string = (!empty($instructions['having_string'])? ' AND '. $instructions['having_string'] : '');;

        $group_by = (!empty($instructions['group_by'])? ' GROUP BY '. $instructions['group_by'] : '');;

        if($this->native_session->get('__user_id') && $this->native_session->get('__user_type') != 'admin'):

            $search_string .= ' AND O.customer_id = "'. $this->native_session->get('__user_id') .'"';

        endif;

        return $this->_query_reader->get_list('get_order_statistics', array(

            'having_string'=>$having_string,

            'search_string'=>$search_string,

            'group_by'=>$group_by,

            'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY num_of_orders DESC'),

            'limit_text'=>'LIMIT ' . $start.','.($limit+1)
        ));
    }

}
?>