<?php
/**
 * This class generates and formats bid details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/15/2015
 */
class _category extends CI_Model
{
	# advanced search list of products
	function lists($instructions)
	{
		$limit = !empty($instructions['limit'])? $instructions['limit']: NUM_OF_ROWS_PER_PAGE;
		
		$start = !empty($instructions['page'])? ($instructions['page']-1)*$limit: 0;
		
		return $this->_query_reader->get_list('get_category_list', array(
			
			'search_string'=>(!empty($instructions['search_string'])? ' AND '. $instructions['search_string'] : ''),
			
			'order_by'=>(!empty($instructions['order_by'])? ' ORDER BY '. $instructions['order_by'] : ' ORDER BY last_updated DESC'),
						
			'limit_text'=>'LIMIT ' . $start.','.($limit+1)
		));
	}
		
	
	# add a product category
	function add($data)
	{
		$result = FALSE;
		
		$author = $this->native_session->get('__user_id');

        # Create the slug and verify that it's unique
        $this->load->helper('text');
        $slug = url_title(convert_accented_characters($data['category_title']), '-', true);
        $current_slug_row = $this->db->get_where('product_categories', array('slug'=>$slug))->row_array();
        if(!empty($current_slug_row) && (empty($data['categoryid']) || (!empty($data['categoryid']) && $current_slug_row['id'] == $data['categoryid']))) $slug .= '-1';
		
		# a) save the main record
		$category_id = $this->_query_reader->add_data((!empty($data['categoryid'])? 'update': 'add').'_product_category', array(
				'title'=>$this->db->escape_like_str($data['category_title']),
				'slug'=>$slug,
				'parent_category'=>(!empty($data['parent_category'])? $data['parent_category'] : 0),
                'banner_item'=>(!empty($data['banner_item'])? $data['banner_item'] : 'N'),
                'description'=>$this->db->escape_like_str(( htmlentities($data['category_description']))),
				'photo_url'=>(!empty($data['photo'])? $data['photo'] : ''), 
				'isactive'=>(empty($data['isactive'])? 'Y' : $data['isactive']), 
				'author'=>$author,
				'category_id'=>(!empty($data['categoryid'])? $data['categoryid']: 0)
			));
		
		# mark the result as true if the product already exists and update is successful, or new record is added
		if((!empty($data['categoryid']) && $this->db->affected_rows()) || $category_id) $result = TRUE;
				
		# log action
		$this->_logger->add_event(array(
			'user_id'=>$this->native_session->get('__user_id'), 
			'activity_code'=>(!empty($data['productid'])? 'update': 'add').'_product_category', 
			'result'=>($result? 'SUCCESS': 'FAIL'), 
			'log_details'=>"device=".get_user_device()."|browser=".$this->agent->browser(),
			'uri'=>uri_string(),
			'ip_address'=>get_ip_address()
		));
		
		return array('boolean'=>$result, 'reason'=>'');
	}
		
	
	function remove_photo($photo_id)
	{
		$result = false;
		
		# get photo details			
		$photo_details = $this->db->get_where('photos', array('id'=>$photo_id))->row_array();
		
		if(!empty($photo_details)):
		
			# remove the file from disk
			@unlink(UPLOAD_DIRECTORY.$photo_details['url']);
			
			$this->db->delete('photos', array('id'=>$photo_id));
			
			$result = $this->db->affected_rows();
					
		endif;
		
		return $result;
	}	
	
	
	# view product category details
	function category_details($category_id)
	{
		$instruction['search_string'] = 'PC.id = "'. $category_id .'"';
		
		$category_row = $this->lists($instruction);
		
		$category_details = end($category_row);
		
		return $category_details;
	}
		
	
	
	# update product category status
	function update_status($newStatus, $categoryIds)
	{
		$msg = '';
		$categoryIds = implode("','",$categoryIds);
		
		# Remove the product category record completely
		if($newStatus == 'delete'):
			 $result = $this->_query_reader->run('delete_product_category', array('category_ids'=>$categoryIds));
				
		# Change whether product is activated or not
		elseif(in_array($newStatus, array('activate', 'deactivate'))):
			$status = array('activate'=>'Y', 'deactivate'=>'N');
			$result = $this->_query_reader->run('update_product_category_status', array('new_status'=>$status[$newStatus], 'category_ids'=>$categoryIds, 'user_id'=>$this->native_session->get('__user_id') ));
		endif;
		
		# log action
		$this->_logger->add_event(array(
			'user_id'=>$this->native_session->get('__user_id'), 
			'activity_code'=>'product_category_status_change', 
			'result'=>($result? 'SUCCESS': 'FAIL'), 
			'log_details'=>"newstatus=".$newStatus."|device=".get_user_device()."|browser=".$this->agent->browser(),
			'uri'=>uri_string(),
			'ip_address'=>get_ip_address()
		));
		
		return array('boolean'=>$result, 'reason'=>$msg);
	}
	
}
?>