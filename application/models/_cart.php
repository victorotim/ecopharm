<?php
/**
 * This class generates and formats cart details. 
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/10/2015
 */
class _cart extends CI_Model
{	
	# add item to the user cart and return updated cart items and status
	function add_item ($item_details)
	{
		$cart['product_id'] = (!empty($item_details['product_id'])? $item_details['product_id'] : 0);
		$cart['price'] = (!empty($item_details['price'])? $item_details['price'] : 0);
		$cart['qty'] = (!empty($item_details['qty'])? $item_details['qty'] : 1);
		$cart['photo_url'] = (!empty($item_details['photo_url'])? $item_details['photo_url'] : '');
		$cart['product_title'] = (!empty($item_details['product_title'])? $item_details['product_title'] : '');
		
		$cart_items = array();
				
		# check if cart has already been created
		if(!empty($this->native_session->get('__cart'))) $cart_items = $this->native_session->get('__cart');
		
		# if the item is already in the cart, just +one the item qty in the cart
		$item_key = search_mult_array($cart['product_id'], 'product_id', $cart_items);
		
		if($item_key !== FALSE):
		
			$cart_items[$item_key]['qty'] ++;
			
		else:			
			
			array_push($cart_items, $cart);
			
		endif;
		
		$this->native_session->set('__cart', $cart_items);
		
		return $this->native_session->get('__cart');
	}

	
	# remove an item from the cart
	function remove_item ($product_id)
	{
		$result = false;
				
		$cart_items = array();
				
		# check if cart has already been created
		if(!empty($this->native_session->get('__cart'))): 
			
			$cart_items = $this->native_session->get('__cart');
		
			# get the item key
			$item_key = search_mult_array($product_id, 'product_id', $cart_items);
		
			if($item_key !== FALSE):
		
				unset($cart_items[$item_key]);
			
				$this->native_session->set('__cart', $cart_items);
			
				$result = true;		
			
			endif;
			
		endif;
		
		return $result;
	}
	
	
	# Update a product's quantity in the cart
	function update_qty ($product_id, $new_qty)
	{
		$result = false;
				
		$cart_items = array();
				
		# check if cart has already been created
		if(!empty($this->native_session->get('__cart'))): 
			
			$cart_items = $this->native_session->get('__cart');
		
			# get the item key
			$item_key = search_mult_array($product_id, 'product_id', $cart_items);
		
			if($item_key !== FALSE):
		
				$cart_items[$item_key]['qty'] = $new_qty;
			
				$this->native_session->set('__cart', $cart_items);
			
				$result = true;		
			
			endif;
			
		endif;
		
		return $result;
	}
	
	
	# get product row
	function get($product_id)
	{
		$product_row = array();
		
		# check if cart has already been created
		if(!empty($this->native_session->get('__cart'))): 
			
			$cart_items = $this->native_session->get('__cart');
		
			# get the item key
			$item_key = search_mult_array($product_id, 'product_id', $cart_items);
		
			if($item_key !== FALSE):
		
				return $cart_items[$item_key];
			
			endif;
			
		endif;
		
		return $product_row;
	}
	
	# Get total value of items in the cart
	function total_value ()
	{
		$total = 0;
				
		# check if cart has already been created
		if(!empty($this->native_session->get('__cart'))): 
			
			$cart_items = $this->native_session->get('__cart');
		
			foreach($cart_items as $cart_item) $total += ($cart_item['qty'] * $cart_item['price']);
			
		endif;
		
		return $total;
	}

    # Request momo pay
    function request_momo_pay ($order_data)
    {
        if (empty($order_data['amount'])):

            $result_array = array('result' => false, 'msg' => 'Order amount not specified');

        elseif (empty($order_data['customer_phone_number'])):

            $result_array = array('result' => false, 'msg' => 'Customer phone number not specified');

        elseif (!$this->native_session->get('__momo_order')):

            $result_array = array('result' => false, 'msg' => 'The order details could not be retrieved');

        else:

            $this->load->model('_payment');

            $request_pay_body['amount'] = $order_data['amount'];
            $request_pay_body['currency'] = 'EUR';
            $request_pay_body['externalId'] = '432568';
            $request_pay_body['payer']['partyIdType'] = 'MSISDN';
            $request_pay_body['payer']['partyId'] = $order_data['customer_phone_number'];
            $request_pay_body['payerMessage'] = !empty($order_data['payerMessage'])? $order_data['payerMessage'] : 'Payment for ecopharm order';
            $request_pay_body['payeeNote'] = !empty($order_data['payeeNote'])? $order_data['payeeNote'] : 'Request to authorize payment for your order at Ecopharm online shop';

            # Create MoMo pay API reference_id
            $reference_id = $this->_payment->get_reference_id();

            if(!$this->_payment->create_api_user()):

                $result_array = array('result'=>false, 'msg'=>'Could not connect to MoMo pay');

            elseif (empty($api_key = $this->_payment->get_api_key($reference_id))):

                $result_array = array('result'=>false, 'msg'=>'Could not create MoMo API key');

            elseif(empty($access_token = $this->_payment->get_access_token($reference_id, $api_key))) :

                $result_array = array('result'=>false, 'msg'=>'Could not generate MoMo API access token');

            elseif(($request_result = $this->_payment->request_to_pay($access_token->access_token, $request_pay_body)) !== false):

                # Save reference and access token in the session to check payment status
                $this->native_session->set('__momo_reference_id', $reference_id);
                $this->native_session->set('__momo_access_token', $access_token->access_token);

                $result_array = array('result' => true, 'msg' => 'The MoMo request pay has been successfully made');

            else:

                $result_array = array('result' => false, 'msg' => 'The MoMo request pay could not be made');

            endif;

        endif;

        # Log the request payment attempt event
        $log_author_id = ($this->native_session->get('__user_id')? $this->native_session->get('__user_id'): 'unknown');

        $log_details = array(
            'uri'=>uri_string(),
            'ip_address'=>get_ip_address(),
            'device'=>get_user_device(),
            'browser'=>$this->agent->browser()
        );

        $this->_logger->add_event(array(
            'user_id'=>$log_author_id,
            'activity_code'=>'cart_momo_request_to_pay',
            'result'=>($result_array['result']? 'SUCCESS':'FAIL'),
            'log_details'=>"username=".$log_author_id."|device=".(!empty($log_details['device'])? $log_details['device']: 'unknown')."|browser=".(!empty($log_details['browser'])? $log_details['browser']: 'unknown'),
            'uri'=>uri_string(),
            'ip_address'=>get_ip_address()
        ));

        return $result_array;

    }


    # Check momo pay status
    function check_momo_payment_status ($access_token, $reference_id)
    {
        $this->load->model('_payment');

        $payment_status = $this->_payment->check_payment_status($access_token, $reference_id);

        return $payment_status;
    }
}


?>