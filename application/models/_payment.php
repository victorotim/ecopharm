<?php
/**
 * This class handles online payments
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/15/2015
 */
class _payment extends CI_Model
{
    private $momo_url = 'https://sandbox.momodeveloper.mtn.com/v1_0/';
    private $momo_collection_url = 'https://sandbox.momodeveloper.mtn.com/collection/v1_0/';
    private $momo_collection_token_url = 'https://sandbox.momodeveloper.mtn.com/collection/token/';
    private $host = 'sandbox.momodeveloper.mtn.com';
    private $call_back_url = 'http://ecopharm.e-villasresidences.com/';
    private $target_environment = 'sandbox';
    private $reference_id;
    private $subscripion_key;

    public function __construct()
    {
        $this->reference_id = $this->generate_reference_id(openssl_random_pseudo_bytes(16));
        $this->subscripion_key = '980a87b5d9884261be7a5e1a4e1c33e7';
    }

    private function generate_reference_id ($data)
    {
        assert(strlen($data) == 16);

        $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
        $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

        return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
    }


    function get_reference_id()
    {
        return $this->reference_id;
    }


    function  create_api_user()
    {
        $url = $this->momo_url . 'apiuser';

        $body = array('providerCallbackHost'=>$this->call_back_url);

        $headers[] = 'Host: '. $this->host;
        $headers[] = 'X-Reference-Id: '. $this->reference_id;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, $body,'POST', false);

        return ($response['http_code'] == 201 ? $this->reference_id : false);
    }


    function get_api_key($reference_id)
    {
        $url = $this->momo_url. 'apiuser/'. $reference_id .'/apikey';

        $headers[] = 'Accept: */*';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Host: '. $this->host;
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, null);

        return ($response['http_code'] == 201) ? $response['response']->apiKey : null;

    }


    /**
     * retrieve the access token for the provided reference_id(api_user) and $api_key pair
     * @param $reference_id
     * @param $api_key
     * @return mixed|null an object with keys access_token,token_type, expires_in or null on failure
     */
    function get_access_token($reference_id, $api_key)
    {
        $url = $this->momo_collection_token_url;
        $authorization = base64_encode($reference_id .':'. $api_key);

        $headers[] = 'Accept: */*';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Authorization: Basic '. $authorization;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Host: '. $this->host;
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, null);

        return ($response['http_code'] == 200) ? $response['response'] : null;

    }


    /**
     * @param  $access_token
     * @param $request_pay_body array
     * @param $callback_url string not used for testing
     * @param $external_reference string external reference id
     * @return bool true when successful or false other wise
     */
    public function request_to_pay($access_token, $request_pay_body)
    {
        $url = $this->momo_collection_url . "requesttopay";

        $headers[] = 'Authorization: Bearer '. $access_token;
        $headers[] = 'X-Reference-Id: '. $this->reference_id;
        # $headers[] = 'X-Callback-Url: '. $external_reference; // causing the request to pay to fail
        $headers[] = 'X-Target-Environment: '. $this->target_environment;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, $request_pay_body);

        return ($response['http_code'] == 202) ? true : false;
    }


    /**
     * @param $access_token
     * @param $reference_id
     * @return mixed
     */
    public function check_payment_status ($access_token, $reference_id)
    {
        $url = $this->momo_collection_url . 'requesttopay/'. $reference_id;

        $headers[] = 'Authorization: Bearer '. $access_token;
        $headers[] = 'X-Target-Environment: '. $this->target_environment;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, null, 'GET');

        return $response['response'];
    }


    public function check_balance($access_token)
    {
        $url = $this->momo_collection_url . "account/balance";

        $headers[] = 'Authorization: Bearer '. $access_token;
        $headers[] = 'X-Target-Environment: '. $this->target_environment;
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Ocp-Apim-Subscription-Key: '. $this->subscripion_key;

        $response = $this->curl_post($url, $headers, null, 'GET');

        return $response['response'];
    }



    public function is_user_account_active($access_token, $partyIdType, $partyId)
    {
        $url = $this->momo_collection_url . "/v1_0/accountholder/$partyIdType/$partyId/active";

        $header = array(
            "Authorization: Bearer $access_token",
            "Content-Type: application/json",
            "X-Target-Environment: $this->target_environment",
            "Ocp-Apim-Subscription-Key: $this->subscriptionKey"
        );

        $res = $this->curl_post($url, $header, null, 'GET');

        return ($res['http_code'] == 200) ? true : false;
    }



    /**
     * Use to make http request
     * @param $url string the url to send the request
     * @param $headers array of string. the headers for the http request
     * @param $body array|object associative array of the http body
     * @param string $type request type POST or GET, default is POST
     * @param bool $returnResult , the return type is boolean when false or the result of the request when true
     * @return mixed|null return array of request result when $returnResult=true or boolean when $returnResult=false
     */
    private function curl_post($url, $headers, $body, $type = "POST", $returnResult = true)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => $returnResult,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $type,
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        $res_info = curl_getinfo($curl);
        curl_close($curl);
        $http_code = $res_info['http_code'];

        if ($err) {
            echo "Error #:" . $err . "<br/>";
            $resData = null;
        } else {

            $resData = json_decode($response);

        }
        return ['http_code' => $http_code, 'response' => $resData];
    }

}
?>