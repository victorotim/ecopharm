<?php
/* Dalton Firth Limited created under ODI-BSI contract ref: xxxx              */
/* (c) copyright 2014 ODI-BSI                                                 */

/* Overview
 * --------
 * Class and functions for generating a bespoke B30 encoded alpha-numeric
 * string for a given integer, and decoding the encoded strng back to an 
 * integer.
 *
 * Principle is similar to Crockford Base32 encoding, see:
 * http://en.wikipedia.org/wiki/Base32#Crockford.27s_Base32
 *
 * DF-base30 encoding brings several improbements that make the encoded 
 * number easier to read over the phone.
 * - No use of symbols in check digit. Instead use 2 check digits.
 * - Use of 1 (one) and 0 (zero) is ONLY used in check digit and first
 *   check digit is either one or zero, making it possible to detect
 *   algorithmically whether a check digit is present.
 *
 * This class provides  methods support optional check digit and string prefix.
 * Also support to pad-out string to fixed number of digits is provided.
 *
 */
 
/* How to use this class:
 * - Create an instance of the class and call one of the simple access methods
 * - OR - use one of the access functions
 */

class _util extends CI_Model {
	public $prefix             = FALSE;
	public $integer            = FALSE;
	public $separator          = "-"; 
	public $checkPass          = NULL;
	public $failReason         = NULL;
	public $requireCheckDigits = TRUE;
	public $checkDigits        = TRUE;
	public $groupSize          = 4;
	public $padToWholeGroups   = TRUE;
	public $minGroups          = 2;


	public function df_check($base10) 
	{
		// Use a check digit, instead of basic Crockford, 
		// which isn't too easy to read over the phone due to using some 
		// symbols (~* etc) use 2 check digits
		return substr("0A0B0C0D0E0F0G0H0J0K0M0N0P0Q0R0S0T0U0V0W0X0Y0Z1A1B1C1D1E1F1G1H1J1K1M1N1P1Q",
					  (($base10 % 37)*2), 2);
	}

	public function encode($base10, $prefix=NULL, $use_check_digit=NULL) 
	{
		if ($base10 < 0)
		{
			throw new Exception("Error: can only encode positive integers");
		}

		// Use instance variable setting if not passed as a paramater
		if ($use_check_digit === NULL)
		{
			$use_check_digit = $this->checkDigits;
		}

		if (($prefix !== NULL) && ($this->separator !== NULL))
		{
			if (strlen($prefix) !== 1)
			{
				throw new Exception("Error: prefix must be a single character");
			}
			
			$prefix = $prefix . $this->separator;
		}
		else
		{
			$prefix = "";
		}
		
		$encoded_string = $this->_df_encode($base10);

		if ($use_check_digit)
		{
			$encoded_string .= $this->df_check($base10);
		}

		// Add padding if necessary - pad to group length
		if ($this->padToWholeGroups)
		{
			while ((strlen($encoded_string) % $this->groupSize) !== 0)
			{
				// Add leading zero (encoded as 'Y');
				$encoded_string = 'Y' . $encoded_string;
			}
		}
		
		// Pad to minimum group size
		while (strlen($encoded_string) < ($this->minGroups * $this->groupSize))
		{
			$encoded_string = 'Y' . $encoded_string; 
		}
		
		$chunks = str_split($encoded_string, $this->groupSize);

		$encoded_string = implode("-", $chunks);

		return $prefix . $encoded_string;

	}

	public function decode($base30) 
	{
		// Zero instance variables
		$this->prefix     = FALSE;
		$this->integer    = FALSE;
		$this->checkPass  = NULL;
		$this->failReason = NULL;


		// Check to see if there's a prefix and remove
		// any dashes and padding (U)
		$temp_array = explode($this->separator, $base30);

		if ((count($temp_array) > 1) &&
			(strlen($temp_array[0]) === 1))
		{
			// A separator is present but the initial group is less than
			// group length therefore is a prefix
			$this->prefix =  $temp_array[0];
			array_shift($temp_array);
		}
		
		// recreate base30 string with no separators
		$base30 = implode("", $temp_array);

		// Turn all letter 'O' to zero (0) and letters 'I' and 'L' to one (1)
		$base30 = strtr(strtoupper($base30), 
						"ILO",
						"110");

		// Remove any padding characters (leading Y's)
		$base30 = ltrim($base30, 'Y');

		// Check to see if check digits are present. Check digits are the 
		// only place in the string 0 or 1 is used (and I, L and O are not 
		// used at all, so no confusion).
		if ((($chk_pos=strpos($base30, "0")) !== FALSE) ||
			(($chk_pos=strpos($base30, "1")) !== FALSE))
		{
			$check_digits = substr($base30, $chk_pos);
			$base30 = substr($base30, 0, $chk_pos);
		}
		else
		{
			$check_digits = FALSE;
		}

		// IF the result after removing leading 'Y's  and check digits is empty 
		// string then the string must be zero (Y);
		if (strlen($base30) === 0)
		{
			$base30 = 'Y';
		}               
		
		$base10 = $this->_df_decode($base30);

		if ($check_digits !== FALSE)
		{
			if ($check_digits !== $this->df_check($base10))
			{
				$this->failReason = "Input is invalid (check sum fails)";
				return FALSE;
			}            
			else
			{
				$this->checkPass = TRUE;
			}
		}
		else if ($this->requireCheckDigits && $this->checkDigits)
		{
			$this->failReason = "Check digits missing (check digits are required)";
			return FALSE;
		}

		$this->integer = $base10;
		return $base10;
	}

	// Encode with no check digit, internal method
	protected function _df_encode($base10) 
	{
		return strtr(base_convert($base10, 10, 30),
					 "abcdefghijklmnopqrst01",
					 "ABCDEFGHJKMNPQRSTVWXYZ");
	}

	// Decode with no check digit, internal method
	protected function _df_decode($base30)
	{
		$base30 = strtr(strtoupper($base30), 
						"ABCDEFGHJKMNPQRSTVWXYZILO",
						"abcdefghijklmnopqrst01110");

		return base_convert($base30, 30, 10);
	}

}
