<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing account pages.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 2021-07-02
 */
class Accounts extends CI_Controller 
{
	
	#Constructor to set some default values at class load
	public function __construct()
    {
        parent::__construct();
        $this->load->model('_account');
        $this->load->model('_user');
	}
	
	
	# Register a user account
	function register()
	{

		$data = filter_forwarded_data($this);
				
		# if user has posted the form
		if(!empty($_POST['emailaddress'])){
			
			$data['formdata'] = $_POST;
			
			# check if all required fields have been submitted
			$required_fields_check = process_fields($this, $_POST, array('emailaddress', 'firstname', 'lastname', 'telephone', 'zip_code', 'password', 'newpassword', 'check_terms_conditions'));
			
			$data['error_fields'] = $required_fields_check['error_fields'];
			
			if(!$required_fields_check['boolean']):
				
				$data['msg'] = 'ERROR: '.(!empty($required_fields_check['msg'])? $required_fields_check['msg'] : 'Please enter all the required fields');
			
			elseif($_POST['password'] != $_POST['newpassword']):
			
				$data['msg'] = 'ERROR: The passwords you entered don\'t match';
				
			else:
				
				# add the user with the customer permission group
				$_POST['permission_group'] = 2;
				
				# default activate user
				$_POST['userstatus'] = 'active';
				
				$response = $this->_user->add($_POST);
				
				# there was an error
				if(!(!empty($response) && $response['boolean'])):
				
					$data['msg'] = !empty($response['reason'])? $response['reason']: 'ERROR: There was an error saving the user details.';
				
				# The account has been successfully created
				else:

                    $checkout_view = '';
				    $other_msg = 'You can login to view your profile';

                    if(!empty($data['src']) && $data['src'] == 'cart'):
                        $checkout_view = '/src/cart';
                        $other_msg = 'You can now login to complete your order';
                    endif;

					$this->native_session->set('msg', 'Your account has been successfully created. <br/>'. $other_msg);
					
					# redirect the user to the login page
					redirect(base_url(). 'accounts/login'. $checkout_view);
				
				endif;
				
			endif;		
		}
		
		$this->load->view('accounts/register', $data);
	}

	
	
	# login
	function login()
	{
		$data = filter_forwarded_data($this);
		
		# The user wants to proceed to login
		if(!empty($_POST)){
			if(!empty($_POST['verified'])){
				$response = $this->_account->login($_POST['loginusername'], $_POST['loginpassword'], array(
					'uri'=>uri_string(),
					'ip_address'=>get_ip_address(),
					'device'=>get_user_device(),
					'browser'=>$this->agent->browser()
				));

				# Proceed based on the login response from the API
				if(!empty($response['result']) && $response['result'] == 'SUCCESS' && !empty($response['default_view'])) {
					
					add_to_user_session($this, $response['user_details']);
					$this->native_session->set('__default_view', $response['default_view']);
					
					if(!empty($response['permissions'])) $this->native_session->set('__permissions', $response['permissions']);

                    # Redirect the user to the check out page if they were trying to complete an order
                    if (!empty($_POST['loginsrc']) && $_POST['loginsrc'] == 'cart'):

                        redirect(base_url() . 'cart/checkout');

                    # Redirect the user to the pages/prescription method if __prescription session variable exists
                    elseif ($this->native_session->get('__prescription')):

                        redirect(base_url() . 'pages/prescription');

                    elseif (!empty($response['default_view']) && !empty($response['permissions'])):

                        if (!empty($_POST['redirect'])) redirect(base_url() . get_redirect_url($_POST['redirect']));
                        else redirect(base_url() . $response['default_view']);

                    else:
						
						$data['msg'] = "ERROR: No permissions could be resolved for your account.";
					
					endif;
				}
				else $data['msg'] = "ERROR: The user name and password do not match a registered user. Please check and try again.";
			}
			else $data['msg'] = "ERROR: Your login could not be verified.";
		}
		
		# User has been redirected while trying to checkout
		if(!empty($data['src']) && $data['src']=='cart' && empty($this->native_session->get('msg'))) $data['msg'] = 'WARNING: Please login, or create an account on the website to complete your order';
		
		$this->load->view('accounts/login', $data);
	}
	
	
	
	# The admin dashboard
	function admin_dashboard()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'view_admin_dashboard');

        $this->load->model('_catalog');
        $this->load->model('_customer');
		$this->load->model('_order');

        $store_statistics = $this->_order->get_order_statistics(array());

        $data['store_statistics'] = end($store_statistics);

        $data['monthly_statistics'] = $this->_order->get_order_statistics(array('group_by'=>'YEAR(O.dateadded), MONTH(O.dateadded)', 'order_by'=>'order_year, order_month'));

        $data['monthly_delivery_statistics'] = $this->_order->get_order_statistics(array('searchstring'=>'OFS.status="delivered', 'group_by'=>'YEAR(OFS.last_updated), MONTH(OFS.last_updated)', 'order_by'=>'order_year, order_month'));

        $data['recent_orders'] = $this->_order->lists(array('order_by'=>'O.dateadded DESC', 'limit'=>5));

		$data['latest_order'] = count($data['recent_orders'])? $data['recent_orders'][0] : array();

		$data['active_products'] = $this->_catalog->lists(array('search_string'=>'P.isactive="Y"', 'limit'=>1000000));

		$data['top_products'] = $this->_order->get_top_products(array('limit'=>5));

		$data['customers'] = $this->_customer->lists(array());

		$this->load->view('accounts/admin_dashboard', $data);
	}
	
	
	# The client dashboard
	function client_dashboard()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);
		
		$this->load->model('_order');
		$data['list'] = $this->_order->lists(array());
		$this->load->view('orders/manage', $data);
	}
	
	
	
	# logout
	function logout()
	{
		$data = filter_forwarded_data($this);
		#Log sign-out event
		$userId = $this->native_session->get('__user_id')? $this->native_session->get('__user_id'): "";
		$email = $this->native_session->get('__email_address')? $this->native_session->get('__email_address'): "";
		$this->_logger->add_event(array('user_id'=>$userId, 'activity_code'=>'user_logout', 'result'=>'success', 'log_details'=>"userid=".$userId."|email=".$email ));
		
		# Set appropriate message - reason for log out.
		$data['msg'] = $this->native_session->get('msg')? get_session_msg($this): "WARNING: You have been logged out.";
					
		#Remove any set session variables
		$this->native_session->delete_all();
		$this->load->view('accounts/login', $data);
	}


	# Forgot Password
	function forgot()
	{
		$data = filter_forwarded_data($this);  # if form is submitted
		if(!empty($_POST['registeredemail'])) {

			$this->load->model('_user');
			$result = $this->_account->send_password_link($_POST['registeredemail'], base_url());
			$msg = $result['result']? 'Email with instructions has been sent to you.': 'ERROR:'. $result['msg'];

			echo format_notice($this, $msg);

		}
		else $this->load->view('accounts/recover_password', $data);
	}


    # Recover Password
    function password_recovery()
    {
        $data = filter_forwarded_data($this);

        # if form is submitted
        if (!empty($_POST['confirm_password'])):

            # check if all required fields have been posted
            $required_fields_check = process_fields($this, $_POST, array('new_password', 'confirm_password'));

            $data['error_fields'] = $required_fields_check['error_fields'];

            if(!$required_fields_check['boolean']):

                if(empty($_POST['new_password'])):

                    $msg = 'Please enter your new password';

                elseif(empty($_POST['confirm_password'])):

                    $msg = 'Please confirm your new password';

                endif;

                $msg = (!empty($msg)? $msg : 'Please enter all the required fields <br/>'.implode(',', $data['error_fields']));

            elseif($_POST['new_password'] != $_POST['confirm_password']):

                $msg = 'Your new password does not match the confirmed new password';

            elseif (!validatePassword($_POST['confirm_password'])['boolean']):

                $msg = 'ERROR: Your password does not meet the minimum password strength requirements<br/>' .
                    validatePassword($_POST['confirm_new_password'])['msg'];

            else:

                $result = $this->_account->password_recovery($_POST['confirm_password'], $_POST['token']);
                $msg = $result['msg'];
            endif;

            $msg = !empty($result['result']) && $result['result']? 'Your password has been successfully updated.<br/> Click <a href="'. base_url() .'accounts/login">here to login</a>' : 'ERROR:' . $msg;

            echo format_notice($this, $msg);

        elseif (!empty($data['token'])):

            $data['token'] = decrypt_value($data['token']);

            # verify token
            $data['token_verification_result'] = $this->_account->verify_password_recovery_token($data['token']);

            $this->load->view('accounts/reset_password', $data);

        endif;
    }
	
	
	# Check provider user name
	function check_user_name()
	{
		$data = filter_forwarded_data($this);
		if(!empty($_POST['user_name'])){
			$check = $this->_account->valid_user_name($_POST['user_name']);
			echo !empty($check['is_valid']) && $check['is_valid'] == 'Y'? 'VALID': 'INVALID';
		}
	}




	
	
	# Filter audit form
	function audit_filter()
	{
		$data = filter_forwarded_data($this);
		$this->load->view('accounts/audit_filter', $data);
	}
	
	
}

/* End of controller file */