<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing order pages.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/4/2015
 */
class Orders extends CI_Controller
{
    # constructor to set some default values at class load
    public function __construct()
    {
        parent::__construct();
        $this->load->model('_order');
    }


    # to manage the orders
    function manage()
    {
        $data = filter_forwarded_data($this);
        logout_invalid_user($this, 'view_orders');

        $instructions = array();

        if (!empty($data['q'])):

            switch ($data['q']):

                case 'awaiting_processing':
                    $instructions['search_string'] = 'OFS.status LIKE "awaiting processing"';
                    break;

                case 'awaiting_delivery':
                    $instructions['search_string'] = 'OFS.status LIKE "awaiting delivery"';
                    break;

                case 'dispatched':
                    $instructions['search_string'] = 'OFS.status LIKE "dispatched"';
                    break;

            endswitch;

        endif;

        $data['list'] = $this->_order->lists($instructions);

        $this->load->view('orders/manage', $data);
    }


    # get order details
    function details()
    {
        $data = filter_forwarded_data($this);

        logout_invalid_user($this, 'view_order_details');

        $order_id = decrypt_value($data['d']);

        $data['summary'] = $this->_order->summary($order_id);

        $data['details'] = $this->_order->details($order_id);

        if (empty($data['summary']) || empty($data['details'])) $data['details'] = "ERROR: Order details can not be resolved.";

        $this->load->view('orders/order_details', $data);
    }


    # list actions
    function list_actions()
    {
        $data = filter_forwarded_data($this);
        echo get_option_list($this, 'order_list_actions', 'div');
    }


    # filter orders
    function list_filter()
    {
        $data = filter_forwarded_data($this);
        $this->load->view('orders/list_filter', $data);
    }


    # Add/Edit an order
    function add()
    {
        $data = filter_forwarded_data($this);

        logout_invalid_user($this, 'add_order');

        # user has posted the new product information
        if (!empty($_POST)) {
            # Upload the file before you proceed with the rest of the process
            $fileUrls = upload_many_files($_FILES, 'photo__fileurl', 'photo_', 'jpeg,jpg,png');
            $_POST['photos'] = !empty($fileUrls) ? $fileUrls : array();

            if (!empty($_POST['productid'])) $_POST['productid'] = decrypt_value($_POST['productid']);

            $result = $this->_catalog->add($_POST);

            if (!$result['boolean']) echo "ERROR: The product could not be added. " . $result['reason'];
        } # just coming to the form
        else {
            if (!empty($data['p'])):

                $product_id = decrypt_value($data['p']);

                $data['formdata'] = $this->_catalog->product_details($product_id);

                $this->db->order_by('display_order');
                $data['formdata']['photos'] = $this->db->get_where('photos', array('_product_id' => $product_id, 'isactive' => 'Y'))->result_array();

            endif;

            $this->load->view('orders/new_order', $data);
        }
    }


    # update an order's status
    function update_status()
    {
        $data = filter_forwarded_data($this);

        logout_invalid_user($this, 'update_order_status');

        $view_to_load = '';

        if (!empty($_POST['update_order_status']) && !empty($_POST['order_id'])):

            $_POST['order_id'] = decrypt_value($_POST['order_id']);

            $result = $this->_order->update_status($_POST);

            if ($result['boolean']):

                $order_summary = $this->_order->summary($_POST['order_id']);

                # Notify the user if the status change is successful
                /*
                $message['code'] = '';
                $this->_messenger->send($order_summary['customer_id'], $message);
                */
            endif;

            $data['msg'] = !empty($result['boolean']) && $result['boolean'] ? 'The order status has been changed.' : 'ERROR: The order status could not be changed.';

            $this->native_session->set('msg', $data['msg']);

            $data['result'] = $result['boolean'];

            $data['json_data'] = $data;

            $data['area'] = 'json_msg';

            $view_to_load = 'addons/basic_addons';

        else:

            $order_id = (!empty($data['d']) ? decrypt_value($data['d']) : 0);

            $data['summary'] = $this->_order->summary($order_id);

            $data['details'] = $this->_order->details($order_id);

            $data['payment_statuses'] = $this->_order->get_payment_statuses();

            $data['fulfillment_statuses'] = $this->_order->get_fulfillment_statuses();

            $view_to_load = 'orders/update_status';

        endif;

        $this->load->view($view_to_load, $data);
    }

}

/* End of controller file */