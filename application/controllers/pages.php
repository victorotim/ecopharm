<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing public pages.
 * @property CI_Model _logger
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 10/20/2015
 */
class Pages extends CI_Controller 
{

	#Constructor to set some default values at class load
	public function __construct()
	{
		parent::__construct();
		$this->load->model('_page');
		$this->load->model('_category');
		$this->load->model('_catalog');

		# log page hit
        $event_details['user_id'] = $this->native_session->get('__user_id');
        $event_details['activity_code'] = $this->uri->segment(2);
        $event_details['result'] = 'SUCCESS';

        $log_details[] = 'device='.get_user_device();
        $log_details[] = 'browser='.$this->agent->browser();

        if (!empty($_POST))
            foreach ($_POST as $key => $val)
                if (!is_array($val)) array_push($log_details, $key . '=' . $val);

        $event_details['log_details'] = implode("|", $log_details);

        $this->_logger->add_event($event_details);
	}

	# home page
	function index()
	{
		$data = filter_forwarded_data($this);
		
		# Collect all data needed for the UI
		$data['categories'] = $this->_category->lists(array('search_string'=>'PC.isactive="Y"'));
		$data['catalog'] = $this->_catalog->lists(array('search_string'=>'P.isactive="Y"', 'limit'=>1000));
		
		$this->load->view('home', $data);
	}	


	# view single product page
	function product_details ()
	{
		$data = filter_forwarded_data($this);
        $slug = $this->uri->segment(2, 0);

		if($slug):

            $product_row = $this->db->get_where('products', array('slug'=>$slug))->row_array();

		    $product_id = (!empty($product_row['id'])? $product_row['id'] : 0);
			
			$data['product_details'] = $this->_catalog->product_details($product_id);

            if (!empty($data['product_details'])):

                # break down the associated categories
                $categories = explode(',', $data['product_details']['categories']);

                if (!empty($categories)):

                    $data['category'] = $this->db->get_where('product_categories', array('id' => end($categories)))->row_array();

                    $this->db->where_in('_product_category_id', $categories);
                    $product_ids = array_column($this->db->get('product_category_mapping')->result_array(), '_product_id');

                    if(empty($product_ids)) $product_ids[] = 0;

                    $search_string = ' P.id IN (' . implode(',', $product_ids) . ') AND P.isactive="Y" AND P.id != ' . $product_id;

                    $data['related_products'] = $this->_catalog->lists(array('search_string' => $search_string));

                endif;

            endif;
			
		endif;
				
		$this->load->view('pages/product_details', $data);
	}
	

	# view products under a selected category
	function product_category ()
	{
		$data = filter_forwarded_data($this);

		# Get only the top/parent categories
		$data['categories'] = $this->_category->lists(array('search_string'=>'PC.isactive="Y" AND (PC.parent_category IS NULL OR PC.parent_category = 0) AND P.num_of_products>0', 'order_by'=>'PC.title'));

		$slug = $this->uri->segment(2, 0);

		$view_to_load = 'pages/product_category';

		if($slug):

            $category_row = $this->db->get_where('product_categories', array('slug'=>$slug))->row_array();

            $category_id = (!empty($category_row['id'])? $category_row['id'] : 0);

			$data['selected_category'] = $this->_category->category_details($category_id);
			
			$this->db->where(array('_product_category_id'=> $category_id));
			$product_ids = array_column($this->db->get('product_category_mapping')->result_array(), '_product_id');
			
			if(!empty($product_ids)):
			
				$search_string = ' P.id IN ('. implode(',', $product_ids) .')';

			    $data['products_per_page'] = $products_per_page = 20;

                $page = (empty($data['page'])? 0 : $data['page'] - 1) * $products_per_page;
					
				$data['category_products'] = $this->_catalog->lists(array('search_string'=>$search_string, 'limit'=>$products_per_page, 'page'=>$page));

                # if user is accessing a particular limit set then no need to load the whole page or do another inventory count
                if (!empty($data['page'])):

                    $data['area'] = 'product_category_items';

                    $view_to_load = 'addons/basic_addons';

                else:
                    $data['number_of_products'] = $this->_catalog->get_count(array('search_string' => $search_string));
                endif;
			
			endif;
			
		endif;
				
		$this->load->view($view_to_load, $data);
	}




    # view the prescription form
    function prescription ()
    {
        $data = filter_forwarded_data($this);

        if(!empty($_POST) || $this->native_session->get('__prescription'))
        {

            if($this->native_session->get('__prescription'))
            {
                $_POST = $this->native_session->get('__prescription');

                $this->native_session->delete('__prescription');
            }

            $data['formdata'] = $_POST;

            # check if all required fields have been submitted
            $required_fields_check = process_fields($this, $_POST, array('customer_name', 'email_address', 'prescribing_clinic', 'telephone_number'));

            $data['error_fields'] = $required_fields_check['error_fields'];

            if (!$required_fields_check['boolean']):

                $data['msg'] = 'ERROR:' . (!empty($msg) ? $msg : 'Please enter all the required fields <br/>' . implode(',', $data['error_fields']));

            # check if prescription file has been uploaded
            elseif (empty($_FILES['prescription_file']['size'])):

                $data['msg'] = 'ERROR: Please upload the prescription file';

            else:

                # Upload the file before you proceed with the rest of the process
                $fileUrls = upload_many_files($_FILES, 'prescription__fileurl', 'prescription_file_', 'jpeg,jpg,png,pdf');
                $_POST['file_url'] = !empty($fileUrls) ? end($fileUrls) : '';

                # if user is not yet logged in, store the details in session and redirect them to login/create account pages
                if (!$this->native_session->get('__user_id')):

                    $this->native_session->set('msg', 'Please login or create an account to complete your prescription order');

                    $this->native_session->set('__prescription', $_POST);

                    redirect(base_url(). 'accounts/login/src/prescription');

                else:

                    $_POST['customer_id'] = $this->native_session->get('__user_id');
                    $_POST['instructions'] = 'instructions';

                    $data['result'] = $result = $this->_page->add_prescription($_POST);

                    # Notify the customer
                    if($result) $this->_page->notify_user_prescription_received($result);

                    # Notify system admins
                    if($result) $this->_page->notify_admin_prescription_received($result);

                    $msg = $result? 'The prescription has been successfully submitted. We will contact you to confirm the prescription details<br/> Thank you!' : 'ERROR: The prescription could not be submitted';
                    $this->native_session->set('msg', $msg);

                    redirect(base_url(). 'pages/prescription');

                endif;

            endif;

        }

        $data['page_title'] = 'Enter Prescription';

        $search_string = 'P.isactive="Y"';

        $data['recent_products'] = $this->_catalog->lists(array('search_string' => $search_string, 'order_by' => 'P.dateadded DESC', 'limit' => 4));

        $data['page_to_load'] = 'pages/prescription_form';

        $this->load->view('pages/basic_page', $data);
    }
	
	
	# Update the user cart and show the cart modal window 
	function update_cart ()
	{
		$data = filter_forwarded_data($this);
		
		# check if cart session already exists
		if(!empty($data['p'])): 
			
			$this->load->model('_cart');
			
			$product_id = decrypt_value($data['p']);
			
			$product_details = $this->_catalog->product_details($product_id);
									
			if(!empty($product_details)):
				
				$item_details['product_id'] = $product_id;
				$item_details['product_title'] = $product_details['product_title'];
				$item_details['price'] = $product_details['current_price'];
				$item_details['qty'] = (!empty($data['qty'])? $data['qty'] : 1);
				$item_details['img_url'] = $product_details['img_url'];
				
				$data['cart_items'] = $this->_cart->add_item($item_details);
			
			endif;
			
		endif;
				
		$this->load->view('pages/check_out_modal', $data);
	}


	# Generate a custom drop down list
	function get_custom_drop_list()
	{
		$data = filter_forwarded_data($this);

		if (!empty($data['type'])) {
			$searchBy = !empty($data['search_by']) ? $data['search_by'] : '';
			$data['list'] = get_option_list($this, $data['type'], 'div', $searchBy, $data);
		}

		$data['area'] = "dropdown_list";
		$this->load->view('addons/basic_addons', $data);
	}
	
	
	# contact us page
	function contact_us()
	{
		$data = filter_forwarded_data($this);

		if(!empty($_POST)) {
			$msg = $this->_page->send_contact_message($_POST)? 'Your message has been sent': 'ERROR: Your message could not be sent';
			$this->native_session->set('msg',$msg);
		}
		else
		{			
			$data['page_title'] = 'Contact us';
		
			$search_string = 'P.isactive="Y"';
						
			$data['recent_products'] = $this->_catalog->lists(array('search_string'=>$search_string, 'order_by'=>'P.dateadded DESC', 'limit'=>4));

            $data['page_to_load'] = 'pages/contact_us_form';
			
			$this->load->view('pages/basic_page', $data);
		}
	}
	
	
	# about us page
	function about()
	{
		$data = filter_forwarded_data($this);
		
		$data['page_title'] = 'About the online shop';
		
		$search_string = 'P.isactive="Y"';
						
		$data['recent_products'] = $this->_catalog->lists(array('search_string'=>$search_string, 'order_by'=>'P.dateadded DESC', 'limit'=>4));

        $data['page_to_load'] = 'pages/about_us';

		$this->load->view('pages/basic_page', $data);
	}


    # Terms and conditions
    function privacy_policy()
    {
        $data = filter_forwarded_data($this);

        $data['page_title'] = 'Ecopharm Privacy Policy';

        $search_string = 'P.isactive="Y"';

        $data['recent_products'] = $this->_catalog->lists(array('search_string'=>$search_string, 'order_by'=>'P.dateadded DESC', 'limit'=>4));

        $data['page_to_load'] = 'pages/privacy_policy';

        $this->load->view('pages/basic_page', $data);
    }


    # search the product catalog
    function search_catalog()
    {
        $data = filter_forwarded_data($this);

        if(!empty($_POST['search_btn'])):
            $data['categories'] = $this->_category->lists(array('search_string'=>'PC.isactive="Y"', 'order_by'=>'PC.title'));

            $instructions['search_string'] = '(P.title LIKE "%'. $_POST['search_term'] .'%" OR PC.category_names LIKE "%'. $_POST['search_term'] .'%" OR P.description LIKE "%'. $_POST['search_term'] .'%") AND P.isactive ="Y"';

            $data['list'] = $this->_catalog->lists($instructions);

        elseif(!empty($_POST['search_term'])):

            $instructions['search_string'] = 'item_title LIKE "%'. $_POST['search_term'] .'%" AND isactive = "Y"';

            $data['list'] = $this->_catalog->lists_products_union_categories($instructions);

        endif;

        $data['area'] = 'catalog_search_results';

        $view_to_load = !empty($_POST['search_btn']) && $_POST['search_btn'] == 'search_catalog'? 'pages/search_results' : 'addons/basic_addons';

        $this->load->view($view_to_load, $data);
    }
	
}

/* End of controller file */