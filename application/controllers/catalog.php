<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing bid pages.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/4/2015
 */
class Catalog extends CI_Controller 
{
	# constructor to set some default values at class load
	public function __construct()
    {
        parent::__construct();
        $this->load->model('_catalog');
	}
	
	
	# to manage the product list
	function manage()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'view_catalog');
		
		$data['list'] = $this->_catalog->lists(array());

		$this->load->view('catalog/manage', $data);
	}

	
	# list actions
	function list_actions()
	{
		$data = filter_forwarded_data($this);
		echo get_option_list($this, 'catalog_list_actions', 'div');
	}
	
	
	
	# filter catalog
	function list_filter()
	{
		$data = filter_forwarded_data($this);
		$this->load->view('catalog/list_filter', $data);
	}

	
	# Add a product
	function add()
	{
		$data = filter_forwarded_data($this);

		# user has posted the new product information
		if(!empty($_POST)){

            logout_invalid_user($this, 'add_product');

			if(!empty($_POST['productid'])) $_POST['productid'] = decrypt_value($_POST['productid']);
						
			$result = $this->_catalog->add($_POST);

			$msg = (!$result['boolean']? "ERROR: The product could not be added. ".$result['reason']: 'The product has been added');

			$this->native_session->set('msg', $msg);

			if(!$result['boolean']):

                $data['msg'] = $msg;

			    $data['area'] = 'basic_msg';

                $this->load->view('addons/basic_addons', $data);

			endif;

		}
		# just coming to the form
		else
		{
            logout_invalid_user($this, 'view_catalog');

			if(!empty($data['p'])):
			
				$product_id = decrypt_value($data['p']);
				
				$data['formdata'] = $this->_catalog->product_details($product_id);
								
				$this->db->order_by('display_order');
				$data['formdata']['photos'] = $this->db->get_where('photos', array('_product_id'=>$product_id, 'isactive'=>'Y'))->result_array();
				
				$categories = $this->db->get_where('product_category_mapping', array('_product_id'=>$product_id, 'isactive'=>'Y'))->result_array();				
				if(!empty($categories)) $categories = array_column($categories, '_product_category_id');
				$data['formdata']['categories'] = $categories;
								
			endif;
			
			$this->load->model('_category');
			
			$data['categories'] = $this->_category->lists(array('search_string'=>'PC.isactive = "Y"', 'order_by'=>'PC.title'));
			
			$this->load->view('catalog/new_product', $data);
		}
	}
	
	
	# Upload product photo
	function upload_photo()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'upload_product_photo');
		
		# user has posted the new photo
		if(!empty($_FILES)){
			# Upload the file before you proceed with the rest of the process
			$fileUrls = upload_many_files($_FILES, 'photo__fileurl', 'photo_', 'jpeg,jpg,png');
			$data['photo'] = !empty($fileUrls)? $fileUrls: array();

            $product_id = (!empty($data['p']) ? decrypt_value($data['p']) : 0);

            foreach ($data['photo'] AS $photo):

                # create the photo thumbnail
                thumb_nail(UPLOAD_DIRECTORY . $photo, UPLOAD_DIRECTORY . 'thumb_' . $photo, 176);

                $display_order = 1;
                $this->db->order_by('display_order desc');
                $max_row = $this->db->get_where('photos', array('_product_id' => $product_id))->row_array();

                if (!empty($max_row)) $display_order = ++$max_row['display_order'];

                $data['result'] = $this->_query_reader->run('add_product_photo', array('product_id' => $product_id, 'url' => $photo, 'description' => '', 'author' => $this->native_session->get('__user_id'), 'display_order' => $display_order));

            endforeach;

		}
		$data['area'] = 'json_msg';
		$data['json_data'] = $data;
		$this->load->view('addons/basic_addons', $data);
	}


	# update photo display order
	function update_photo_display_order()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'update_photo_display_order');
		
		$result = 0;

		# user has posted the new photo
		if(!empty($_POST['photoOrders'])):
			
			foreach($_POST['photoOrders'] as $photo_order):
				
				$this->db->update('photos', array('display_order'=>$photo_order['displayOrder']), array('id'=>$photo_order['photoID']));

				$result += $this->db->affected_rows();

			endforeach;

		endif;

		$data['result'] = $result;

		$data['area'] = 'json_msg';
		$data['json_data'] = $data;
		$this->load->view('addons/basic_addons', $data);
	}
	
	
	# Remove product photo
	function remove_photo()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'remove_product_photo');
		
		$data['result'] = false;
		
		if(!empty($data['p'])) $data['result'] = $this->_catalog->remove_photo($data['p']);
		
		$data['area'] = 'json_msg';
		$data['json_data'] = $data;
		$this->load->view('addons/basic_addons', $data);
	}
	
	
	
	
	# View product photo
	function view_product_photo()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'view_product_photo');

		if(!empty($data['p'])) $data['photo_url'] = $data['p'];
		$data['area'] = 'product_photo';
		$this->load->view('addons/basic_addons', $data);
	}
		
	
	# update a product's status
	function update_status()
	{
		$data = filter_forwarded_data($this);

		logout_invalid_user($this, 'update_product_status');

		if(!empty($data['t']) && !empty($data['list'])) $result = $this->_catalog->update_status($data['t'], explode('--',$data['list']));
		
		$data['msg'] = !empty($result['boolean']) && $result['boolean']? 'The product status has been changed.': 'ERROR: The product status could not be changed.';
		
		$data['area'] = 'refresh_list_msg';
		$this->load->view('addons/basic_addons', $data);
	}
			
}

/* End of controller file */