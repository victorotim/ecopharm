<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing bid pages.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/4/2015
 */
class Categories extends CI_Controller 
{
	# constructor to set some default values at class load
	public function __construct()
    {
        parent::__construct();
        $this->load->model('_category');
	}
	
	
	# to manage product categories
	function manage()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this, 'manage_product_categories');
		
		$data['list'] = $this->_category->lists(array());
		
		$this->load->view('categories/manage', $data);
	}
	
	
	# list actions
	function list_actions()
	{
		$data = filter_forwarded_data($this);
		echo get_option_list($this, 'category_list_actions', 'div');
	}
	
	
	
	# filter categories
	function list_filter()
	{
		$data = filter_forwarded_data($this);
		$this->load->view('categories/list_filter', $data);
	}
	
	
	
	# Add a product category
	function add()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this, 'add_product_category');
		
		# user has posted the new product information
		if(!empty($_POST)){
			# Upload the file before you proceed with the rest of the process
			$fileUrls = upload_many_files($_FILES, 'photo__fileurl', 'category_photo_', 'jpeg,jpg,png');
			$_POST['photo'] = !empty($fileUrls)? end($fileUrls): '';
						
			if(!empty($_POST['categoryid'])) $_POST['categoryid'] = decrypt_value($_POST['categoryid']);
			
			$result = $this->_category->add($_POST);
			
			if(!$result['boolean']) echo "ERROR: The category could not be added. ".$result['reason'];
		}
		# just coming to the form
		else {
			if(!empty($data['p'])):
			
				$category_id = decrypt_value($data['p']);
				
				$data['formdata'] = $this->_category->category_details($category_id);
								
				
			endif;
			
			$this->load->view('categories/new_category', $data);
		}
	}

		
	
	# update a category's status
	function update_status()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this, 'update_product_category_status');

		if(!empty($data['t']) && !empty($data['list'])) $result = $this->_category->update_status($data['t'], explode('--',$data['list']));
		
		$data['msg'] = !empty($result['boolean']) && $result['boolean']? 'The category status has been changed.': 'ERROR: The category status could not be changed.';
		
		$data['area'] = 'refresh_list_msg';
		$this->load->view('addons/basic_addons', $data);
	}
			
}

/* End of controller file */