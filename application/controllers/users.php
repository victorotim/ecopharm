<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls managing and viewing users.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/14/2015
 */
class Users extends CI_Controller 
{
	
	#Constructor to set some default values at class load
	public function __construct()
    {
        parent::__construct();
        $this->load->model('_user');
	}
	
	
	# user management list
	function index()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);

		$type = ($this->native_session->get('__user_type') == 'admin'? 'all': 'organization');
		$data['list'] = $this->_user->lists($type);
		
		$this->load->view('users/manage', $data);
	}
	
	
	
	
	
	
	# user list actions
	function list_actions()
	{
		$data = filter_forwarded_data($this);
		echo get_option_list($this, ($this->native_session->get('__user_type') == 'pde'? 'provider_':'').'users_list_actions', 'div');
	}
	
	
	
	
	# user permissions
	function permissions()
	{
		$data = filter_forwarded_data($this);
		$data['list'] = $this->_user->permissions($data['g']);
		$this->load->view('users/permissions', $data);
	}
	
	
	
	
	# update user status
	function update_status()
	{
		$data = filter_forwarded_data($this);
		$result = $this->_user->update_status($data['t'], explode('--', $data['list']));
		$data['msg'] = $result? 'The status for the selected users has been updated.': 'ERROR: The status for the selected users could not be updated.';
		$data['area'] = 'refresh_list_msg';
		$this->load->view('addons/basic_addons', $data);
	}
	
	
	

	
	
	# user settings
	function settings()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);
		
		# user has posted the settings form
		if(!empty($_POST)){
			# Upload the photo if any exists before you proceed with the rest of the process
			$_POST['photo_url'] = !empty($_FILES)? upload_file($_FILES, 'newphoto__fileurl', 'photo_'.$this->native_session->get('__user_id').'_', 'png,jpg,jpeg,tiff'): '';
			$result = $this->_user->settings($_POST);
			
			if($result['boolean']) $this->native_session->set('msg', 'Your settings have been updated');
			else echo "ERROR: The settings could not be updated. ".$result['reason'];
		} 
		# just viewing the form
		else {
			$data['user'] = $this->_user->details();
			$this->load->view('users/settings', $data);
		}
	}
	
	
	
	
	# get user details
	function details()
	{
		$data = filter_forwarded_data($this);
		$data['user'] = $this->_user->details($data['d']);
		$this->load->view('users/user_details', $data);
	}
	
	
	
	# filter users
	function list_filter()
	{
		$data = filter_forwarded_data($this);
		$this->load->view('users/list_filter', $data);
	}
	
	
	
	# update the user permission group
	function update_permissions()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);
		
		# user has posted
		if(!empty($_POST)){
			$response = $this->_user->update_permissions($_POST);
			# there was an error
			if(!(!empty($response) && $response['boolean'])) echo 'ERROR: There was an error updating the user permission group.';
		} 
		# success
		else if(!empty($data['result'])){
			$data['msg'] = 'The user permission group has been updated';
			$data['area'] = 'refresh_list_msg';
			$this->load->view('addons/basic_addons', $data);
		}
		else {
			$data['id_list'] = !empty($data['list'])? implode(',',explode('--',$data['list'])): '';
			$this->load->view('users/update_permissions', $data);
		}
	}
	
	
	
	
	# send a message to a list of selected users
	function message()
	{
		$data = filter_forwarded_data($this);
		
		# user has posted
		if(!empty($_POST)){
			$response = $this->_user->message($_POST);
			# there was an error
			if(!(!empty($response) && $response['boolean'])) echo 'ERROR: There was an error sending to the selected users.';
		} 
		# success
		else if(!empty($data['result'])){
			$data['msg'] = 'The messages have been sent.';
			$data['area'] = 'refresh_list_msg';
			$this->load->view('addons/basic_addons', $data);
		}
		
		# simply going to a form to send message
		else {
			$data['action'] = 'users/message';
			$data['redirect'] = 'users/message/result/sent';
			$data['id_list'] = implode(',',explode('--',$data['list']));
			$this->load->view('pages/send_from_list', $data);
		}
	}
	
	
	
	
	# add a new user for the same organization
	function add()
	{
		$data = filter_forwarded_data($this);
		
		if($this->native_session->get('__user_type') != 'admin'){ 
			redirect(base_url().'accounts/logout');
		}
		
		# new user is being added
		if(!empty($_POST)){
			$response = $this->_user->add($_POST);
			# there was an error
			if(!(!empty($response) && $response['boolean'])) {
				echo !empty($response['reason'])? $response['reason']: 'ERROR: There was an error saving the user details.';
			}
			else $this->native_session->set('msg','The user details have been saved.');
		} 
		else {
			if(!empty($data['d'])) $data['user'] = $this->_user->details($data['d']);
			$this->load->view('users/new_user', $data);
		}
	}


    # change password
    function change_password()
    {
        $data = filter_forwarded_data($this);

        logout_invalid_user($this, 'change_my_password');

        $user_id = $this->native_session->get('__user_id');

        # new password has been posted
        if(!empty($_POST)){

            $response['boolean'] = false;

            # check if all required fields have been posted
            $required_fields_check = process_fields($this, $_POST, array('current_password', 'new_password', 'confirm_new_password'));

            $data['error_fields'] = $required_fields_check['error_fields'];

            if(!$required_fields_check['boolean']):

                if(empty($_POST['current_password'])):

                    $msg = 'Please enter your current password';

                elseif(empty($_POST['new_password'])):

                    $msg = 'Please enter your new password';

                elseif(empty($_POST['confirm_new_password'])):

                    $msg = 'Please confirm your new password';

                endif;

                $response['reason'] = (!empty($msg)? $msg : 'Please enter all the required fields <br/>'.implode(',', $data['error_fields']));

            elseif($_POST['new_password'] != $_POST['confirm_new_password']):

                $response['reason'] = 'Your new password does not match the confirmed new password';

            elseif (!validatePassword($_POST['confirm_new_password'])['boolean']):

                $response['reason'] = 'ERROR: Your password does not meet the minimum password strength requirements<br/>' .
                    validatePassword($_POST['confirm_new_password'])['msg'];

            else:

                $response = $this->_user->change_password($_POST);

            endif;

            # there was an error
            if(!(!empty($response) && $response['boolean'])) {
                echo !empty($response['reason'])? $response['reason']: 'ERROR: Your password could not be updated.';
            }
            else $this->native_session->set('msg','Your password has been successfully updated.');
        }
        else {
            $this->load->view('users/change_password', $data);
        }
    }
	
}

/* End of controller file */