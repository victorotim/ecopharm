<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls managing and viewing customers.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/14/2015
 */
class Customers extends CI_Controller 
{
	
	#Constructor to set some default values at class load
	public function __construct()
    {
        parent::__construct();
        $this->load->model('_customer');
        $this->load->model('_user');
	}
	
	
	# customer management list
	function index()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);
		
		$type = ($this->native_session->get('__user_type') == 'admin'? 'all': 'organization');
		$data['list'] = $this->_customer->lists($type);
		
		$this->load->view('customers/manage', $data);
	}
	
	
	
	
	
	
	# customer list actions
	function list_actions()
	{
		$data = filter_forwarded_data($this);
		echo get_option_list($this, ($this->native_session->get('__user_type') == 'pde'? 'provider_':'').'users_list_actions', 'div');
	}	
	
	
	
	# update user status
	function update_status()
	{
		$data = filter_forwarded_data($this);
		$result = $this->_user->update_status($data['t'], explode('--', $data['list']));
		$data['msg'] = $result? 'The status for the selected customers has been updated.': 'ERROR: The status for the selected customers could not be updated.';
		$data['area'] = 'refresh_list_msg';
		$this->load->view('addons/basic_addons', $data);
	}
	
	
	

	
	
	# customer settings
	function settings()
	{
		$data = filter_forwarded_data($this);
		logout_invalid_user($this);
		
		# user has posted the settings form
		if(!empty($_POST)){
			# Upload the photo if any exists before you proceed with the rest of the process
			$_POST['photo_url'] = !empty($_FILES)? upload_file($_FILES, 'newphoto__fileurl', 'photo_'.$this->native_session->get('__user_id').'_', 'png,jpg,jpeg,tiff'): '';
			$result = $this->_user->settings($_POST);
			
			if($result['boolean']) $this->native_session->set('msg', 'Your settings have been updated');
			else echo "ERROR: The settings could not be updated. ".$result['reason'];
		} 
		# just viewing the form
		else {
			$data['user'] = $this->_user->details();
			$this->load->view('users/settings', $data);
		}
	}
	
	
	
	
	# get customer details
	function details()
	{
		$data = filter_forwarded_data($this);
		$data['user'] = $this->_user->details($data['d']);
		$this->load->view('users/user_details', $data);
	}
	
	
	
	# filter users
	function list_filter()
	{
		$data = filter_forwarded_data($this);
		$this->load->view('customers/list_filter', $data);
	}	
	
	
	
	# send a message to a list of selected users
	function message()
	{
		$data = filter_forwarded_data($this);
		
		# user has posted
		if(!empty($_POST)){
			$response = $this->_user->message($_POST);
			# there was an error
			if(!(!empty($response) && $response['boolean'])) echo 'ERROR: There was an error sending to the selected users.';
		} 
		# success
		else if(!empty($data['result'])){
			$data['msg'] = 'The messages have been sent.';
			$data['area'] = 'refresh_list_msg';
			$this->load->view('addons/basic_addons', $data);
		}
		
		# simply going to a form to send message
		else {
			$data['action'] = 'users/message';
			$data['redirect'] = 'users/message/result/sent';
			$data['id_list'] = implode(',',explode('--',$data['list']));
			$this->load->view('pages/send_from_list', $data);
		}
	}
	
	
	
	
	# manually add a customer
	function add()
	{
		$data = filter_forwarded_data($this);
		
		if($this->native_session->get('__user_type') != 'admin'){ 
			redirect(base_url().'accounts/logout');
		}
		
		# new user is being added
		if(!empty($_POST)){
			$response = $this->_user->add($_POST);
			# there was an error
			if(!(!empty($response) && $response['boolean'])) {
				echo !empty($response['reason'])? $response['reason']: 'ERROR: There was an error saving the user details.';
			}
			else $this->native_session->set('msg','The user details have been saved.');
		} 
		else {
			if(!empty($data['d'])) $data['user'] = $this->_user->details($data['d']);
			$this->load->view('users/new_user', $data);
		}
	}
	
}

/* End of controller file */