<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls viewing sub-lists.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 11/12/2015
 */
class Lists extends CI_Controller 
{
	
	# Load a list's data
	function load()
	{
		$data = filter_forwarded_data($this);
					
		if(!empty($data['t']))
		{
			$limit = !empty($data['n'])? $data['n']: NUM_OF_ROWS_PER_PAGE;
			$offset = !empty($data['p'])? ($data['p'] - 1)*$limit: 0;
			
			
			# Dynamic loading of system's lists based on the list type and passed parameters
			switch($data['t'])
			{
				case 'audittrail':
					$this->load->model('_account');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__date', (!empty($data['date'])? $data['date']: ''));
					$this->native_session->set($data['t'].'__user_id', (!empty($data['user_id'])? $data['user_id']: ''));
					$this->native_session->set($data['t'].'__activity_code', (!empty($data['activity_code'])? $data['activity_code']: ''));
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					$this->native_session->set($data['t'].'__name', (!empty($data['name'])? $data['name']: ''));
					
					$data['list'] = $this->_account->audit_trail(array(
						'date'=>$this->native_session->get($data['t'].'__date'), 
						'user_id'=>$this->native_session->get($data['t'].'__user_id'), 
						'activity_code'=>$this->native_session->get($data['t'].'__activity_code'), 
						'phrase'=>$this->native_session->get($data['t'].'__phrase'), 
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('accounts/audit_list', $data);
				break;
				
				
				
				
				
				
				case 'catalog':
					$this->load->model('_catalog');
					$data = restore_bad_chars_in_array($data);
					$data['listtype'] = !empty($data['listtype'])? $data['listtype']: (!empty($data['a'])? $data['a']: 'all');
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
                    $this->native_session->set($data['t'].'__stock_status', (!empty($data['stock_status'])? $data['stock_status']: ''));
                    $this->native_session->set($data['t'].'__quantity_per_unit', (!empty($data['quantity_per_unit'])? $data['quantity_per_unit']: ''));
                    $this->native_session->set($data['t'].'__product_status', (!empty($data['product_status'])? $data['product_status']: ''));
                    $this->native_session->set($data['t'].'__product_category', (!empty($data['product_category'])? $data['product_category']: ''));
                    $this->native_session->set($data['t'].'__product_description', (!empty($data['product_description'])? $data['product_description']: ''));
                    $this->native_session->set($data['t'].'__lower_price_bound', (!empty($data['lower_price_bound'])? $data['lower_price_bound']: ''));
                    $this->native_session->set($data['t'].'__upper_price_bound', (!empty($data['upper_price_bound'])? $data['upper_price_bound']: ''));
                    $this->native_session->set($data['t'].'__start_last_updated', (!empty($data['start_last_updated'])? $data['start_last_updated']: ''));
                    $this->native_session->set($data['t'].'__end_last_updated', (!empty($data['end_last_updated'])? $data['end_last_updated']: ''));


                    $search_terms = array();
                    if(!empty($data['stock_status'])) array_push($search_terms, 'P.stock_status LIKE "'. $data['stock_status'] .'"');
                    if(!empty($data['quantity_per_unit'])) array_push($search_terms, 'P.quantity_per_unit LIKE "'. $data['quantity_per_unit'] .'"');
                    if(!empty($data['product_status'])) array_push($search_terms, 'P.isactive = "'. $data['product_status'] .'"');
                    if(!empty($data['product_category'])) array_push($search_terms, 'PC.categories LIKE "%'. $data['product_category'] .'%"');
                    if(!empty($data['product_description'])) array_push($search_terms, 'P.description LIKE "%'. $data['product_description'] .'%"');
                    if(!empty($data['lower_price_bound'])) array_push($search_terms, 'PX.current_price >= "'. $data['lower_price_bound'] .'"');
                    if(!empty($data['upper_price_bound'])) array_push($search_terms, 'PX.current_price <= "'. $data['upper_price_bound'] .'"');
                    if(!empty($data['start_last_updated'])) array_push($search_terms, 'P.dateadded >= "'. $data['start_last_updated'] .'"');
                    if(!empty($data['end_last_updated'])) array_push($search_terms, 'P.dateadded >= "'. $data['end_last_updated'] .'"');
                    array_push($search_terms, 'P.title LIKE "%'. $this->native_session->get($data['t'].'__phrase') .'%"');

					$search_string = implode(' AND ', $search_terms);

					$data['list'] = $this->_catalog->lists(array(
						'search_string'=>$search_string, 
						'page'=>$offset, 
						'limit'=>$limit
					));

					$data['type'] = $data['listtype'];
					$this->load->view('catalog/product_list', $data);
				break;



                case 'order':
                    $this->load->model('_order');
                    $data = restore_bad_chars_in_array($data);
                    $data['listtype'] = !empty($data['listtype'])? $data['listtype']: (!empty($data['a'])? $data['a']: 'all');

                    # Store in session for the filter to use
                    $this->native_session->set($data['t'].'__order_start_date', (!empty($data['order_start_date'])? $data['order_start_date']: ''));
                    $this->native_session->set($data['t'].'__order_end_date', (!empty($data['order_end_date'])? $data['order_end_date']: ''));
                    $this->native_session->set($data['t'].'__order_no', (!empty($data['order_no'])? $data['order_no']: ''));
                    $this->native_session->set($data['t'].'__customer_name', (!empty($data['customer_name'])? $data['customer_name']: ''));
                    $this->native_session->set($data['t'].'__telephone_number', (!empty($data['telephone_number'])? $data['telephone_number']: ''));
                    $this->native_session->set($data['t'].'__payment_method', (!empty($data['payment_method'])? $data['payment_method']: ''));
                    $this->native_session->set($data['t'].'__payment_status', (!empty($data['payment_status'])? $data['payment_status']: ''));
                    $this->native_session->set($data['t'].'__fulfillment_status', (!empty($data['fulfillment_status'])? $data['fulfillment_status']: ''));

                    $search_terms = array();
                    if(!empty($data['order_no'])) array_push($search_terms, 'O.id = "'. $this->_util->decode($data['order_no']) .'"');

                    if (!empty($data['customer_name'])):
                        $customer_name_parts = explode(' ', $data['customer_name']);
                        $customer_name_filter_str = '';
                        foreach ($customer_name_parts as $customer_name_part):
                            $customer_name_filter_str .=(!empty($customer_name_filter_str)? ' OR ' : '') .' C.first_name LIKE "%'. $customer_name_part .'%" OR C.last_name LIKE "%'. $customer_name_part .'%"';
                        endforeach;
                        array_push($search_terms, '('. $customer_name_filter_str .')');
                    endif;

                    if(!empty($data['order_start_date'])) array_push($search_terms, 'O.dateadded >= "'. $data['order_start_date'] .'"');
                    if(!empty($data['order_end_date'])) array_push($search_terms, 'O.dateadded <= "'. $data['order_end_date'] .'"');
                    if(!empty($data['telephone_number'])) array_push($search_terms, 'O.phone_number LIKE "%'. $data['telephone_number'] .'%"');
                    if(!empty($data['payment_method'])) array_push($search_terms, 'O.payment_method LIKE "%'. $data['payment_method'] .'%"');
                    if(!empty($data['payment_status'])) array_push($search_terms, 'OPS.status LIKE "%'. $data['payment_status'] .'%"');
                    if(!empty($data['fulfillment_status'])) array_push($search_terms, 'OFS.status LIKE "%'. $data['fulfillment_status'] .'%"');

                    $search_string = implode(' AND ', $search_terms);

                    $data['list'] = $this->_order->lists(array(
                        'search_string'=>$search_string,
                        'page'=>$offset,
                        'limit'=>$limit
                    ));

                    $data['type'] = $data['listtype'];
                    $this->load->view('orders/order_list', $data);
                    break;
				
				
				
				
						
				case 'document':
					$this->load->model('_document');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__pde', (!empty($data['pde'])? $data['pde']: ''));
					$this->native_session->set($data['t'].'__pde_id', (!empty($data['pde_id'])? $data['pde_id']: ''));
					$this->native_session->set($data['t'].'__category', (!empty($data['category'])? $data['category']: ''));
					$this->native_session->set($data['t'].'__status', (!empty($data['status'])? $data['status']: ''));
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					
					$data['list'] = $this->_document->lists($data['listtype'], array(
						'pde'=>$this->native_session->get($data['t'].'__pde_id'), 
						'category'=>$this->native_session->get($data['t'].'__category'),
						'phrase'=>$this->native_session->get($data['t'].'__phrase'),
						'status'=>$this->native_session->get($data['t'].'__status'),
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('documents/'.(!empty($data['area'])? 'details_list': 'document_list'), $data);
				break;
				
				
				
				
						
				case 'link':
					$this->load->model('_link');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__pde', (!empty($data['pde'])? $data['pde']: ''));
					$this->native_session->set($data['t'].'__pde_id', (!empty($data['pde_id'])? $data['pde_id']: ''));
					$this->native_session->set($data['t'].'__opentype', (!empty($data['opentype'])? $data['opentype']: ''));
					$this->native_session->set($data['t'].'__status', (!empty($data['status'])? $data['status']: ''));
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					
					$data['list'] = $this->_link->lists(array(
						'pde'=>$this->native_session->get($data['t'].'__pde_id'), 
						'opentype'=>$this->native_session->get($data['t'].'__opentype'),
						'phrase'=>$this->native_session->get($data['t'].'__phrase'),
						'status'=>$this->native_session->get($data['t'].'__status'),
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('links/'.(!empty($data['area'])? 'details_list': 'link_list'), $data);
				break;
				
				
				
				
				
						
				case 'permission':
					$this->load->model('_permission');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__type', (!empty($data['type'])? $data['type']: ''));
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					
					$data['list'] = $this->_permission->lists(array(
						'type'=>$this->native_session->get($data['t'].'__type'),
						'phrase'=>$this->native_session->get($data['t'].'__phrase'),
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('permissions/permission_list', $data);
				break;
				
				
				
				
				
						
				case 'faq':
					$this->load->model('_faq');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					
					$data['list'] = $this->_faq->lists(array(
						'phrase'=>$this->native_session->get($data['t'].'__phrase'),
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('faqs/'.(!empty($data['area'])? 'details_list': 'faq_list'), $data);
				break;
				
				
				
				
				
						
				case 'user':
					$this->load->model('_user');
					$data = restore_bad_chars_in_array($data);
					
					# Store in session for the filter to use
					$this->native_session->set($data['t'].'__group', (!empty($data['group'])? $data['group']: ''));
					$this->native_session->set($data['t'].'__status', (!empty($data['status'])? $data['status']: ''));
					$this->native_session->set($data['t'].'__phrase', (!empty($data['phrase'])? $data['phrase']: ''));
					$type = ($this->native_session->get('__user_type') == 'admin'? 'all': 'organization');
		
					$data['list'] = $this->_user->lists($type, array(
						'group'=>$this->native_session->get($data['t'].'__group'),
						'status'=>$this->native_session->get($data['t'].'__status'),
						'phrase'=>$this->native_session->get($data['t'].'__phrase'),
						'offset'=>$offset, 
						'limit'=>$limit
					));
					
					$this->load->view('users/user_list', $data);
				break;
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
				
			
				default:
				break;
			}
		}
	}
	
	
	
}

/* End of controller file */