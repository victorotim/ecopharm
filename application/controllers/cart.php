<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * This class controls user cart.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 10/20/2015
 */
class Cart extends CI_Controller
{

    #Constructor to set some default values at class load
    public function __construct()
    {
        parent::__construct();
        $this->load->model('_cart');
        $this->load->model('_catalog');
    }


    # Update the user cart and show the cart modal window
    function update()
    {
        $data = filter_forwarded_data($this);

        # check if cart session already exists
        if (!empty($data['p'])):

            $product_id = decrypt_value($data['p']);

            $product_details = $this->_catalog->product_details($product_id);


            if (!empty($product_details)):

                $item_details['product_id'] = $product_id;
                $item_details['product_title'] = $product_details['product_title'];
                $item_details['price'] = $product_details['current_price'];
                $item_details['qty'] = (!empty($data['qty']) ? $data['qty'] : 1);
                $item_details['photo_url'] = $product_details['photo_url'];

                $data['cart_items'] = $this->_cart->add_item($item_details);

            endif;

        endif;

        $this->load->view('cart/modal', $data);
    }


    # Update the user cart and show the cart modal window
    function remove_item()
    {
        $data = filter_forwarded_data($this);

        $data['result'] = false;

        # check if cart session already exists
        if (!empty($_POST['item'])):

            $product_id = decrypt_value($_POST['item']);

            $data['result'] = $this->_cart->remove_item($product_id);

            # calculate the new cart total
            $data['total_amount'] = $this->_cart->total_value();

        endif;

        $data['area'] = 'json_msg';

        $data['json_data'] = $data;

        $this->load->view('addons/basic_addons', $data);
    }


    # to do
    function update_qty()
    {
        $data = filter_forwarded_data($this);

        $data['result'] = false;

        # check if cart session already exists
        if (!empty($_POST['item'])):

            $product_id = decrypt_value($_POST['item']);

            $new_qty = $_POST['qty'];

            $data['result'] = $this->_cart->update_qty($product_id, $new_qty);

            # calculate the new cart total
            $data['total_amount'] = $this->_cart->total_value();

            # get the updated details
            $data['cart_row'] = $this->_cart->get($product_id);

        endif;

        $data['area'] = 'json_msg';

        $data['json_data'] = $data;

        $this->load->view('addons/basic_addons', $data);
    }


    # View the user cart and show the cart modal window
    function view()
    {
        $data = filter_forwarded_data($this);

        # check if cart session already exists
        if (!empty($this->native_session->get('__cart'))) $data['cart_items'] = $this->native_session->get('__cart');

        $this->load->view('cart/modal', $data);
    }


    # Check out form
    function checkout()
    {
        $data = filter_forwarded_data($this);

        # if user is buying now, add item to the cart
        if (!empty($_POST['buy_now']) && !empty($_POST['product_id'])):

            $product_id = decrypt_value($_POST['product_id']);

            $product_details = $this->_catalog->product_details($product_id);

            $item_details['product_id'] = $product_id;
            $item_details['product_title'] = $product_details['product_title'];
            $item_details['price'] = $product_details['current_price'];
            $item_details['qty'] = (!empty($_POST['qty']) ? $_POST['qty'] : 1);
            $item_details['photo_url'] = $product_details['photo_url'];

            $this->_cart->add_item($item_details);

        endif;

        # check if cart session already exists
        $data['cart_items'] = $this->native_session->get('__cart');


        $this->db->order_by('title');
        $data['towns'] = $this->db->get_where('towns', array('isactive' => 'Y'))->result_array();

        $data['delivery_fee'] = 2000;

        $this->load->view('cart/checkout', $data);
    }


    function check_payment_status()
    {
        $data = filter_forwarded_data($this);

        if ($this->native_session->get('__momo_reference_id') && $this->native_session->get('__momo_access_token')):

            $reference_id = $this->native_session->get('__momo_reference_id');
            $access_token = $this->native_session->get('__momo_access_token');

            $payment_status = $this->_cart->check_momo_payment_status($access_token, $reference_id);

            $data['json_data'] = array('result' => true, 'msg' => '', 'response' => $payment_status);

        else:

            $data['json_data'] = array('result' => false, 'msg' => 'Could not retrieve payment status');

        endif;

        $data['area'] = 'json_msg';

        $this->load->view('addons/basic_addons', $data);
    }


    # Show payment processing page
    function request_to_pay()
    {
        $data = filter_forwarded_data($this);

        $data['result'] = true;
        $data['msg'] = 'Waiting for you';

        if ($this->native_session->get('__momo_order')):

            $momo_order_details = $this->native_session->get('__momo_order');

            # __momo_reference_id session variable being empty means no pending request to pay, so make a request to pay
            if (!$this->native_session->get('__momo_reference_id')):

                $cart_total = 0;
                $cart_items = $momo_order_details['cart_items'];

                for ($i = 0; $i < count($cart_items); $i++) $cart_total += $cart_items[$i]['price'] * $cart_items[$i]['qty'];

                $pay_data['amount'] = $cart_total + $momo_order_details['delivery_fee'];

                $pay_data['customer_phone_number'] = $momo_order_details['telephone_number'];

                $result = $this->_cart->request_momo_pay($pay_data);

                $data['result'] = $result['result'];

                if ($result['result']):

                    # clear the cart session
                    $this->native_session->delete('__cart');
                    $data['msg'] = 'The mobile money payment request has been initiated. Waiting for you..';

                else:
                    $msg = (!empty($result['msg']) ? $result['msg'] : '');
                    $this->native_session->set('msg', 'ERROR: The mobile money payment request could not be initiated. ' . $msg . '<br/>Please try placing the order again or use the "Cash on delivery" payment method');

                    redirect(base_url() . 'cart/checkout');

                endif;

            else:

                # check if the request to pay has been successful, still pending or failed
                $reference_id = $this->native_session->get('__momo_reference_id');
                $access_token = $this->native_session->get('__momo_access_token');

                $data['payment_status'] = $payment_status = $this->_cart->check_momo_payment_status($access_token, $reference_id);

                # if payment is successful, place the order and redirect to the place order method
                if (!empty($payment_status->status) && $payment_status->status == 'SUCCESSFUL'):

                    # Update payment status to paid
                    $momo_order_details['momo_status'] = 'SUCCESSFUL';

                    # Update momo order session
                    $this->native_session->set('__momo_order', $momo_order_details);

                    redirect(base_url() . 'cart/place_order');

                elseif (!empty($payment_status['status']) && $payment_status['status'] == 'PENDING' && !in_array($payment_status['reason'], array('PAYEE_NOT_FOUND', 'PAYER_NOT_FOUND'))):
                    $data['msg'] = 'WARNING: Payment is being processed.' . (!empty($payment_status['reason']) ? $payment_status['reason'] : '');

                else:

                    $msg = !empty($payment_status['reason']) ? $payment_status['reason'] : '';

                    $this->native_session->set('msg', 'The payment request failed. ' . $msg . '<br/> Please try placing the order again');

                    # refill the user cart session and redirect them to the checkout page so they can try again or use another payment method
                    $cart_items = $momo_order_details['cart_items'];
                    $this->native_session->set('__cart', $cart_items);

                    redirect(base_url() . 'cart/checkout');

                endif;

            endif;

        endif;

        $this->load->view('cart/request_to_pay', $data);
    }


    # Place order
    function place_order()
    {
        $data = filter_forwarded_data($this);

        $result = false;

        $msg = '';

        # check if cart or momo payment session exists
        if (!$this->native_session->get('__cart') && !$this->native_session->get('__momo_order')):

            $data['msg'] = 'Please add some items to your shopping cart';

        elseif (!empty($_POST) || $this->native_session->get('__momo_order')):

            if ($this->native_session->get('__momo_order')) $_POST = $this->native_session->get('__momo_order');

            $data['formdata'] = $_POST;

            # check if all required fields have been submitted
            $required_fields_check = process_fields($this, $_POST, array('email_address', 'first_name', 'last_name', 'town', 'telephone_number', 'address_line_1', 'payment_method'));

            $data['error_fields'] = $required_fields_check['error_fields'];

            if (!$required_fields_check['boolean']):

                if (empty($_POST['payment_method'])):

                    $msg = 'Please select a payment method';

                elseif (empty($_POST['town'])):

                    $msg = 'Please select your delivery town';

                elseif (empty($_POST['region'])):

                    $msg = 'Please select your delivery region';

                endif;

                $data['msg'] = 'ERROR:' . (!empty($msg) ? $msg : 'Please enter all the required fields <br/>' . implode(',', $data['error_fields']));

            else:

                # Get items in the cart
                if ($this->native_session->get('__cart')) $_POST['cart_items'] = $this->native_session->get('__cart');

                # check if user has logged in
                if ($this->native_session->get('__user_id')):

                    $_POST['customer_id'] = $customer_id = $this->native_session->get('__user_id');

                else:

                    # if the provided email address exists on system, get the user_id
                    $user_details = $this->_user->get_by_email_address($_POST['email_address']);

                    if (!empty($user_details)):
                        $_POST['customer_id'] = $customer_id = $user_details['user_id'];

                    else:
                        # User does not exist, create an account for the user and get the new user id
                        # Add other details to the user - user_name and status
                        $_POST['firstname'] = $_POST['first_name'];
                        $_POST['lastname'] = $_POST['last_name'];
                        $_POST['emailaddress'] = $_POST['email_address'];
                        $_POST['telephone'] = $_POST['telephone_number'];
                        $_POST['user_name'] = $_POST['email_address'];
                        $_POST['userstatus'] = 'active';

                        $account_result = $this->_user->add($_POST);

                        if($account_result['boolean']) $_POST['customer_id'] = $customer_id = $account_result['user_id'];

                    endif;

                endif;

                # delivery fee
                $delivery_fee = $this->db->get_where('delivery_fees', array('_town_id'=>$_POST['town'], '_branch_id'=>1))->row_array();
                $_POST['delivery_fee'] = (!empty($delivery_fee)? $delivery_fee['amount'] : 2000);

                # For mobile money payments, if payment is not yet successful, redirect user to request for payment
                if ($_POST['payment_method'] == 'mobile_money' && (empty($_POST['momo_status']) || (!empty($_POST['momo_status']) && $_POST['momo_status'] !== 'SUCCESSFUL'))):

                    # save post information in session and set the order status to pending
                    $_POST['momo_status'] = 'pending_request';
                    $this->native_session->set('__momo_order', $_POST);

                    redirect(base_url() . 'cart/request_to_pay');

                elseif (!empty($customer_id)):

                    $this->load->model('_order');

                    $response = $this->_order->add($_POST);

                    # there was an error
                    if (!(!empty($response) && $response['boolean'])):

                        $data['msg'] = !empty($response['reason']) ? $response['reason'] : 'ERROR: There was an error saving the order details.';

                    # The order has been successfully created
                    else:

                        # clear the cart session
                        $this->native_session->delete('__cart');

                        # clear momo payment sessions as well
                        # TODO: Keep all momo variable in single session array variable
                        $this->native_session->delete('__momo_order');
                        $this->native_session->delete('__momo_access_token');
                        $this->native_session->delete('__momo_reference_id');

                        $this->native_session->set('msg', 'Your order has been successfully placed. <br/>Use Order No: <b>' . $this->_util->encode($response['order_id']) . '</b> to track the order status from your account');

                        # Notify the customer
                        $this->_order->send_order_received_email($response['order_id']);

                        # Notify system admins
                        $this->_order->notify_admin_new_order($response['order_id']);

                        # redirect the user to the confirmation page
                        redirect(base_url() . 'cart/order_confirmed/i/' . encrypt_value($response['order_id']));

                    endif;

                else:
                    $data['msg'] = 'ERROR: The customer details could not be created. Please try again.';

                endif;

            endif;

        endif;

        $data['cart_items'] = $this->native_session->get('__cart');

        $this->db->order_by('title');
        $data['towns'] = $this->db->get_where('towns', array('isactive' => 'Y'))->result_array();

        $this->load->view('cart/checkout', $data);
    }


    # Order confirmed
    function order_confirmed()
    {
        $data = filter_forwarded_data($this);

        if (!empty($data['i'])):

            $order_id = decrypt_value($data['i']);

            $this->load->model('_order');

            $data['order_summary'] = $this->_order->summary($order_id);

        endif;

        $data['catalog'] = $this->_catalog->lists(array('searchstring' => 'P.isactive="Y"', 'limit' => 5));

        $this->load->view('cart/order_confirmed', $data);
    }


    # delivery charges
    function delivery_fees()
    {
        $data = filter_forwarded_data($this);

        $data['result'] = false;

        # check if town id has been posted
        if (!empty($_POST['town'])):

            $town_id = $_POST['town'];

            # to do, get branch
            $delivery_fee = $this->db->get_where('delivery_fees', array('_town_id'=>$town_id, '_branch_id'=>1))->row_array();

            $data['delivery_fee'] = (!empty($delivery_fee)? $delivery_fee['amount'] : 2000);

            $data['total_amount'] = $this->_cart->total_value() + $data['delivery_fee'];

        endif;

        $data['area'] = 'json_msg';

        $data['json_data'] = $data;

        $this->load->view('addons/basic_addons', $data);
    }

}

/* End of controller file */