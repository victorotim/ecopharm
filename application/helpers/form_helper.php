<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

# Process data from the form
function process_fields($obj, $data, $required=array(), $allowChars=array())
{
    $disallowChars = array("'", "\"", "\\", "(", ")", "/", "<", ">", "!", "#", "%", "&", "?", "$", ":", ";", "=", "*");
    if(!empty($allowChars)) $disallowChars = array_diff($disallowChars, $allowChars);
    # Did the data set pass the requried check
    $hasPassed = true;
    $finalData = array();
	$error_fields = array();
    $msg = "";
    $checked_required_fields = $required;

    foreach($data AS $key=>$value)
    {

        if(($check_required_key = array_search($key, $checked_required_fields)) !== false)
            unset($checked_required_fields[$check_required_key]);

        # Do not validate arrays
        if(!is_array($value))
        {
            $value = htmlentities(trim($value), ENT_QUOTES);
            # Add if the string is not empty and does not contain any of the disallowed characters
            if(!empty($value) && !(0 < count(array_intersect(str_split(strtolower($value)), $disallowChars))) )
            {
                
                $obj->native_session->set($key, $value);
                $finalData[$key] = $value;
            }
            # Catch the case where a required field was not entered
            else if(in_array($key, $required))
            {
                $hasPassed = false;
				$error_fields[] = $key;
				
				if(count(array_intersect(str_split(strtolower($value)), $disallowChars))):
					$msg = 'Some fields contain disallowed characters';
				endif;
            }
        }
    }

    # check if some of the required fields were not passed in data array
    if(!empty($required) && !empty($checked_required_fields))
    {
        $hasPassed = false;
        $error_fields = $checked_required_fields;
    }

    return array('boolean'=>$hasPassed, 'error_fields'=> $error_fields, 'data'=>$finalData, 'msg'=>$msg);
}


// Validate a password string
function validatePassword($password){
    $errors = [];
    if (strlen($password) < 8) {
        array_push($errors, "Your password must be at least 8 characters");
    }
    if (!preg_match("/[a-z]/", $password)) {
        array_push($errors,"Your password must contain at least one letter.");
    }
	if (!preg_match("/[0-9]/", $password)) {
        array_push($errors,"Your password must contain at least one digit.");
    }

	return array('boolean'=>!count($errors), 'msg'=>implode('<br/>', $errors));
}



