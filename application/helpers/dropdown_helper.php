<?php
/**
 * This file contains functions that are used in a number of classes or views.
 *
 * @author Victor Otim <sirotim@gmail.com>
 * @version 1.0.0
 * @copyright Ecopharm Uganda Ltd
 * @created 10/31/2015
 */




# Get a list of options 
# Allowed return values: [div, option]
function get_option_list($obj, $list_type, $return = 'select', $searchBy="", $more=array())
{
	$optionString = '';
	$types = array();
	
	switch($list_type)
	{
		case "contactreason":
			$types = array('Issues With Creating An Account', 'Report Error On System', 'Can Not Find Product', 'Other - Details in Message');
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row."'>".$row."</div>";
				else $optionString .= "<option value='".$row."'>".$row."</option>";
			}
		break;
				
		
		case "countries":
			$types = $obj->_query_reader->get_list('get_countries_list');
			
			if($return == 'div') $optionString .= "<div data-value=''>Select Country</div>";
			else $optionString .= "<option value=''>Select Country</option>";
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['id']."'>".$row['name']."</div>";
				else $optionString .= "<option value='".$row['id']."' ".(!empty($more['selected']) && $more['selected'] == $row['id']? 'selected': '').">".$row['name']."</option>";
			}
		break;



        case "regions":
            $obj->db->order_by('title');
            $types = $obj->db->get_where('regions', array('isactive'=>'Y'))->result_array();

            if($return == 'div') $optionString .= "<div data-value=''>Select Region</div>";
            else $optionString .= "<option value=''>-Select Region-</option>";

            foreach($types AS $row)
            {
                if($return == 'div') $optionString .= "<div data-value='".$row['id']."'>".$row['title']."</div>";
                else $optionString .= "<option value='".$row['id']."' ".(!empty($more['selected']) && $more['selected'] == $row['id']? 'selected': '').">".$row['title']."</option>";
            }
            break;



        case "towns":
            $obj->db->order_by('title');
            $types = $obj->db->get_where('towns', array('isactive'=>'Y'))->result_array();

            if($return == 'div') $optionString .= "<div data-value=''>Select Town</div>";
            else $optionString .= "<option value=''>Select Town</option>";

            foreach($types AS $row)
            {
                if($return == 'div') $optionString .= "<div data-value='".$row['id']."'>".$row['title']."</div>";
                else $optionString .= "<option value='".$row['id']."' ".(!empty($more['selected']) && $more['selected'] == $row['id']? 'selected': '').">".$row['title']."</option>";
            }
            break;

		case "secretquestions":
			$types = $obj->_query_reader->get_list('get_secret_questions');
			
			if($return == 'div') $optionString .= "<div data-value=''>Select Secret Question</div>";
			else $optionString .= "<option value=''>Select Secret Question</option>";
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['id']."'>".$row['question']."</div>";
				else $optionString .= "<option value='".$row['id']."' ".(!empty($more['selected']) && $more['selected'] == $row['id']? 'selected': '').">".$row['question']."</option>";
			}
		break;
		
		
		case "users":
			$types = $obj->_query_reader->get_list('search_user_list', array('phrase'=>htmlentities($searchBy, ENT_QUOTES), 'limit_text'=>' LIMIT '.NUM_OF_ROWS_PER_PAGE));
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['user_id']."' onclick=\"universalUpdate('user_id','".$row['user_id']."')\">".$row['name']."</div>";
				else $optionString .= "<option value='".$row['user_id']."' onclick=\"universalUpdate('user_id','".$row['user_id']."'>".$row['name']."</option>";
			}
		break;
		
		
		case "activitycodes":
			$types = $obj->_query_reader->get_list('get_activity_codes');
			
			if($return == 'div') $optionString .= "<div data-value=''>Select Activity Code</div>";
			else $optionString .= "<option value=''>Select Activity Code</option>";
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['code']."'>".ucwords($row['display_code'])."</div>";
				else $optionString .= "<option value='".$row['code']."' ".(!empty($more['selected']) && $more['selected'] == $row['code']? 'selected': '').">".ucwords($row['display_code'])."</option>";
			}
		break;



        case "product_categories":
            $ci=& get_instance();
            $ci->load->model('_category');
            $types = $ci->_category->lists(array('limit'=>1000));

            if($return == 'div') $optionString .= "<div data-value=''>Select product category</div>";
            else $optionString .= "<option value=''>-Select product category-</option>";

            foreach($types AS $row)
            {
                if($return == 'div') $optionString .= "<div data-value='".$row['category_id']."'>".ucwords($row['category_title'])."</div>";
                else $optionString .= "<option value='".$row['category_id']."' ".(!empty($more['selected']) && $more['selected'] == $row['category_id']? 'selected': '').">".ucwords($row['category_title'])."</option>";
            }
            break;
		
		
		
		
		
		
		case "users_list_actions":
			
			if($list_type == "users_list_actions") $types = array('message'=>'Message', 'activate'=>'Activate', 'deactivate'=>'Deactivate', 'update_permission_group'=>'Update Permission Group');
			else if($list_type == "provider_users_list_actions") $types = array('message'=>'Message');
			
			foreach($types AS $key=>$row)
			{
				if($key == 'message') $url = 'users/message';
				else if(in_array($key, array('activate', 'deactivate'))) $url = 'users/update_status/t/'.$key;
				else if($key == 'update_permission_group') $url = 'users/update_permissions';
				
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$url."'>".$row."</div>";
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		
		break;
		
		
		
		
		case "catalog_list_actions":
			
			$types = array('activate'=>'Activate', 'deactivate'=>'Deactivate', 'delete'=>'Delete', 'out_of_stock'=>'Out of stock', 'available'=>'Available/In stock');
						
			$url = 'catalog/update_status';
			foreach($types AS $key=>$row)
			{
				$url .= '/t/'.$key;
				
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$url."'>".$row."</div>";
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		
		break;
						
		
		
		case "category_list_actions":
			
			$types = array('activate'=>'Activate', 'deactivate'=>'Deactivate', 'delete'=>'Delete');
						
			$url = 'categories/update_status';
			foreach($types AS $key=>$row)
			{
				$url .= '/t/'.$key;
				
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$url."'>".$row."</div>";
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		
		break;

		
		
		
		
		case "documentstatus":
		case "linkstatus":
		case "userstatus":
		
			if($list_type == 'contractstatus') {
				if($obj->native_session->get('__user_type') == 'provider') $types = array('active'=>'Active','endorsed'=>'Endorsed by MoJ','cancelled'=>'Cancelled','complete'=>'Complete','terminated'=>'Terminated','archived'=>'Archived');
				else if($obj->native_session->get('__user_type') == 'pde') $types = array('active'=>'Active','complete'=>'Complete','terminated'=>'Terminated','cancelled'=>'Cancelled','commenced'=>'Commenced','final_payment'=>'Final Payment','archived'=>'Archived','saved'=>'Saved');
				else $types = array('active'=>'Active','complete'=>'Complete','cancelled'=>'Cancelled','commenced'=>'Commenced','final_payment'=>'Final Payment','terminated'=>'Terminated','archived'=>'Archived');
			}
			else if($list_type == 'bidstatus') $types = array('saved'=>'Saved', 'submitted'=>'Submitted');
			else if($list_type == 'mybidstatus') $types = array(''=>'Select Bid Status', 'saved'=>'Saved', 'submitted'=>'Submitted','under_review'=>'Under Review', 'short_list'=>'Short Listed', 'rejected'=>'Rejected', 'completed'=>'Completed', 'won'=>'Won', 'awarded'=>'Awarded', 'archived'=>'Archived');
			else if($list_type == 'providerstatus') $types = array('active'=>'Active', 'inactive'=>'Inactive', 'suspended'=>'Suspended');
			else if( in_array($list_type, array('documentstatus','linkstatus','trainingstatus')) ) $types = array('active'=>'Active', 'inactive'=>'Inactive');
			else if($list_type == 'userstatus') $types = array(''=>'Select User Status', 'pending'=>'Pending', 'active'=>'Active', 'inactive'=>'Inactive', 'deleted'=>'Deleted');
			else if($list_type == 'procurementplanstatus') $types = array('saved'=>'Saved', 'published'=>'Published');
			else if($list_type == 'tenderstatus') $types = array('saved'=>'Saved', 'cancelled'=>'Cancelled', 'extended'=>'Extended', 'published'=>'Published/Issued');
			else $types = array('saved'=>'Saved', 'published'=>'Published', 'archived'=>'Archived');
			
			
			foreach($types AS $key=>$row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$key."'>".$row."</div>";
				else $optionString .= "<option value='".$key."' ".(!empty($more['selected']) && $more['selected'] == $key? 'selected': '').">".$row."</option>";
			}
		break;


        case "paymentstatus":
        case "fulfillmentstatus":

            if($list_type == 'paymentstatus') $types = array('awaiting payment' => 'Awaiting payment', 'paid' => 'Paid', 'cancelled' => 'Cancelled', 'refunded' => 'Refunded');
            elseif ($list_type == 'fulfillmentstatus') $types = array('awaiting processing'=>'awaiting processing', 'processing order'=>'processing order', 'awaiting delivery'=>'awaiting delivery', 'dispatched'=>'dispatched', 'delivered'=>'delivered', 'delivery canceled'=>'delivery canceled', 'returned'=>'returned');

            foreach ($types AS $key => $row) {
                if ($return == 'div') $optionString .= "<div data-value='" . $key . "'>" . $row . "</div>";
                else $optionString .= "<option value='" . $key . "' " . (!empty($more['selected']) && $more['selected'] == $key ? 'selected' : '') . ">" . $row . "</option>";
            }
            break;
		
		
		case "currencies":
			$types = $obj->_query_reader->get_list('get_currency_list', array('phrase'=>htmlentities($searchBy, ENT_QUOTES), 'limit_text'=>' LIMIT '.NUM_OF_ROWS_PER_PAGE));
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['currency_code']."' onclick=\"universalUpdate('currency_code','".$row['currency_code']."')\">".$row['display']."</div>";
				else $optionString .= "<option value='".$row['currency_code']."' onclick=\"universalUpdate('currency_code','".$row['currency_code']."'>".$row['display']."</option>";
			}
		break;
		
		
		
		
		
		
		case "admingroups":
			$types = $obj->_query_reader->get_list('get_permission_group_list', array('type_condition'=>" AND type!=''", 'phrase_condition'=>'', 'limit_text'=>''));
		
			foreach($types AS $key=>$row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['group_id']."'>".$row."</div>";
				else $optionString .= "<option value='".$row['group_id']."' ".(!empty($more['selected']) && $more['selected'] == $row['group_id']? 'selected': '').">".$row['name']."</option>";
			}
		break;

		
		
		
		
		
		case "document_list_actions":
		case "link_list_actions":
		
			$types = array('archive'=>'Archive');
			if($obj->native_session->get('__user_type') == 'admin') {
				$types = array_merge($types, array('reactivate'=>'Re-Activate', 'delete'=>'Delete'));
			}
			
			if($list_type == "link_list_actions") $urlStub = 'links';
			else if($list_type == "document_list_actions") $urlStub = 'documents';
			else if($list_type == "training_list_actions") $urlStub = 'training';
			
			foreach($types AS $key=>$row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$urlStub."/update_status/t/".$key."'>".$row."</div>"; 
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		
		break;

		
		
		
		
		
		case "grouptypes":
			 $types = array('admin'=>'Admin', 'pde'=>'PDE', 'provider'=>'Provider');
			if($return == 'div') $optionString .= "<div data-value=''>Select Group Type</div>";
			else $optionString .= "<option value=''>Select Group Type</option>";
			
			foreach($types AS $key=>$row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$key."'>".$row."</div>";
				else $optionString .= "<option value='".$key."' ".(!empty($more['selected']) && $more['selected'] == $key? 'selected': '').">".$row."</option>";
			}
			
		break;
		
		
		
		
		case "permission_list_actions":
			$types = array('delete'=>'Delete');
			
			foreach($types AS $key=>$row)
			{
				$url = 'permissions/update_status/t/'.$key;
				
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$url."'>".$row."</div>";
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		break;
		
		
		
		
		case "faq_list_actions":
		case "forum_list_actions":
		
			$types = array('archive'=>'Archive');
			if($obj->native_session->get('__user_type') == 'admin') {
				$types = array_merge($types, array('reactivate'=>'Re-Activate', 'delete'=>'Delete'));
			}
			
			if($list_type == "forum_list_actions") $url = 'forums/update_status';
			else if($list_type == "faq_list_actions") $url = 'faqs/update_status';
			
			foreach($types AS $key=>$row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$key."' data-url='".$url."/t/".$key."'>".$row."</div>"; 
				else $optionString .= "<option value='".$key."'>".$row."</option>";
			}
		
		break;
		
		
		
		
		case "faqs":
			$types = $obj->_query_reader->get_list('search_simple_faq_list', array('phrase'=>htmlentities($searchBy, ENT_QUOTES), 'limit_text'=>' LIMIT '.NUM_OF_ROWS_PER_PAGE));
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['faq_id']."' onclick=\"universalUpdate('show_after','".$row['faq_id']."')\">".$row['question']."</div>";
				else $optionString .= "<option value='".$row['faq_id']."' onclick=\"universalUpdate('show_after','".$row['faq_id']."'>".$row['question']."</option>";
			}
		break;

				
		
		
		
		
		case "permissiongroups":
			$types = $obj->_query_reader->get_list('get_permission_groups');
			
			if($return == 'div') $optionString .= "<div data-value=''>Select Permission Group</div>";
			else $optionString .= "<option value=''>Select Permission Group</option>";
			
			foreach($types AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['group_id']."'>".$row['name']."</div>";
				else $optionString .= "<option value='".$row['group_id']."' ".(!empty($more['selected']) && $more['selected'] == $row['group_id']? 'selected': '').">".$row['name']."</option>";
			}
		break;


		case "searchsecureadmin":

			$types['admin'][] = array('url'=>'accounts/admin_dashboard', 'display'=>'Audit Trail');
			$types['admin'][] = array('url'=>'procurement_plans/manage', 'display'=>'Procurement Plans');
			$types['admin'][] = array('url'=>'tenders/manage', 'display'=>'Invitation for Bids/Quotations');
			$types['admin'][] = array('url'=>'bids/manage', 'display'=>'Bids Received');
			$types['admin'][] = array('url'=>'bids/manage/a/best_bidders', 'display'=>'Best Evaluated Bidders');
			$types['admin'][] = array('url'=>'bids/manage/a/awards', 'display'=>'Contract Awards');
			$types['admin'][] = array('url'=>'providers/manage', 'display'=>'Providers');
			$types['admin'][] = array('url'=>'contracts/manage', 'display'=>'Contracts');
			$types['admin'][] = array('url'=>'documents/manage', 'display'=>'Documents');
			$types['admin'][] = array('url'=>'links/manage', 'display'=>'Important Links');
			$types['admin'][] = array('url'=>'documents/manage/a/standard', 'display'=>'Standards');
			$types['admin'][] = array('url'=>'training/manage', 'display'=>'Training Activities');
			$types['admin'][] = array('url'=>'forums/manage', 'display'=>'Forums');
			$types['admin'][] = array('url'=>'reports/manage', 'display'=>'Reports');
			$types['admin'][] = array('url'=>'users/index', 'display'=>'Users');
			$types['admin'][] = array('url'=>'permissions/manage', 'display'=>'Permission Groups');
			$types['admin'][] = array('url'=>'faqs/manage', 'display'=>'FAQs');

			
			# select the search list to show
			if($list_type == "searchpublic") $list = $types['public'];
			else if($list_type == "searchsecureadmin") $list = $types['admin'];
			else if($list_type == "searchsecurepde") $list = $types['pde'];
			else if($list_type == "searchsecureprovider") $list = $types['provider'];
		
			if($return == 'div') $optionString .= "<div data-value=''>Select Search Area</div>";
			else $optionString .= "<option value=''>Select Search Area</option>";
			
			foreach($list AS $row)
			{
				if($return == 'div') $optionString .= "<div data-value='".$row['url']."'>".$row['display']."</div>";
				else $optionString .= "<option value='".$row['url']."'>".$row['display']."</option>";
			}
		break;
		
		
		
		
		
		
		
		
		
	}
	
	
	# Determine which value to return
	if($return == 'objects'){
		return $types;
	}
	else if($return == 'div'){
		return !empty($optionString)? $optionString: "<div data-value=''>No Options</div>";
	}
	else {
		return !empty($optionString)? $optionString: "<option value=''>No Options</option>";
	}
	
}

	



# Get an item from a position in a drop down list
function get_list_item($array, $current, $direction, $return='key')
{
	if(array_key_exists($current, $array)){
		$keys = array_keys($array);
		$length = count($keys);
		$position = array_search($current, $keys);
		
		$newPosition = $position;
		if($direction == 'next') {
			$newPosition = ($position + 1) == $length? 0: ($position + 1);
		} else {
			$newPosition = ($position - 1) < 0? ($length - 1): ($position - 1);
		}
		
		# return the new list item
		return $return == 'value'? $array[$keys[$newPosition]]: $keys[$newPosition];
	}
	return '';
}




?>