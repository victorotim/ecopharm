var towns = new Array();
(function ($) {
	"use strict";

		//Get system base URL
		function getBaseURL()
		{
		   var pageURL = document.location.href;
		   var urlArray = pageURL.split("/");  
		   var BaseURL = urlArray[0]+"//"+urlArray[2]+"/";
		   //Dev environments have the installation sitting in a separate folder
		   if(urlArray[2] == '127.0.0.1' || urlArray[2] == '0.0.0.0' || urlArray[2].indexOf("localhost") > -1)
		   {
				BaseURL = 'http://localhost/ecopharm/';
		
		   }
		   
		
		   return BaseURL;
		}

		////////////////////////////////////////////////////
		// 07. Scroll To Top Js
		function smoothSctollTop() {
			$('.smooth-scroll a').on('click', function (event) {
				var target = $(this.getAttribute('href'));
				if (target.length) {
					event.preventDefault();
					$('html, body').stop().animate({
						scrollTop: target.offset().top - 0
					}, 1500);
				}
			});
		}
		smoothSctollTop();
	
		// Show or hide the sticky footer button
		$(window).on('scroll', function(event) {
			if($(this).scrollTop() > 600){
				$('#scroll').fadeIn(200)
			} else{
				$('#scroll').fadeOut(200)
			}
		});
	
		//Animate the scroll to yop
		$('#scroll').on('click', function(event) {
			event.preventDefault();
	
			$('html, body').animate({
				scrollTop: 0,
			}, 1500);
		});

	
		////////////////////////////////////////////////////
		// 10. Carousel Slider Js
		$('.carousel-slider-active').owlCarousel({
			loop:true,
			margin:30,
			autoplay:true,
			autoplayTimeout:3000,
			smartSpeed:500,
			items:6,
			navText:['<button><i class="fa fa-angle-left"></i>PREV</button>','<button>NEXT<i class="fa fa-angle-right"></i></button>'],
			nav:false,
			dots:false,
			responsive:{
				0:{
					items:1
				},
				576:{
					items:2
				},
				767:{
					items:2
				},
				992:{
					items:2
				},
				1200:{
					items:3
				}
			}
		});
		
		////////////////////////////////////////////////////
		// 02. Info Bar Js
		$(".extra__info-btn").on("click", function () {
			$(".extra__info").addClass("info-opened");
			$(".body-overlay").addClass("opened");
		});
		$(".extra__info-close-btn").on("click", function () {
			$(".extra__info").removeClass("info-opened");
			$(".body-overlay").removeClass("opened");
		});
		$(".body-overlay").on("click", function () {
			$(".extra__info").removeClass("info-opened");
			$(".body-overlay").removeClass("opened");
			$(".header__search-wrapper").removeClass("opened");
		});
		
		$('.dec.qtybutton, .inc.qtybutton').on('click',function(){
			var curQty = $('.product-quantity #qty').val();
			if($(this).hasClass('dec')) $('.product-quantity #qty').val(--curQty)
			else $('.product-quantity #qty').val(++curQty);
		});
		
		$('.add-to-cart, .view-cart').on('click',function(){
			var cartUrl = 'cart/view';
			
			if($(this).hasClass('add-to-cart')){
				var productID = $("#product-id").val();
				var qty = $("#qty").val();
				cartUrl = 'cart/update/p/'+productID+'/qty/'+qty;
			}
			
			$('.modal-content').load(getBaseURL()+cartUrl,function(){
				$('#checkout-modal').modal({show:true});
			});
		});
		
		$('.checkout-modal').on('click', '.remove', function(){
			var itemID = $(this).attr('id').replace('item-', '');	
			var parentDiv = $(this).parents('div.row').first();
			var totalSpan = $('.modal-footer p.total span');
			
			$.ajax({
				type: "POST",
				url: getBaseURL()+'cart/remove_item',
				data: {'item': itemID},
				beforeSend: function() {
					
				},
				error:function( xhr, textStatus, errorThrown) {
					console.log(xhr.responseText);
				},
				success: function(data) {
					if(data.match(/php error/i)) {	
						console.log(data);
					}
					else
					{
						var data = $.parseJSON(data);
						if(data.result == true)
						{			
							var totalAmount = 'ush&nbsp;'+(data.total_amount).toLocaleString();
							$(parentDiv).fadeOut('slow');
							$(parentDiv).remove();
							$(totalSpan).fadeOut('slow').html(totalAmount).show('slow');
						}
						else 'ERROR: Something went wrong. The item could not be removed.';
					}
											
				}
			});
		});
		
		$('.checkout-modal').on('click', '.dec.qtybutton, .inc.qtybutton', function(){
				var parentDiv = $(this).parents('div.row').first();	
				var inputQty = $(this).parent('div').find('input[name=qty]');
				var curQty = $(inputQty).val();
				var newQty = ($(this).hasClass('dec')? --curQty: ++curQty);
				var itemID = $(parentDiv).attr('id').replace('product-row-', '');
				var spinnerHtml = '<div class="spinner-grow text-primary" role="status"><span class="sr-only">Updating cart...</span></div>';
				var productPrice = $(parentDiv).find('.product-price');
				var cartTotal = $('.checkout-modal .modal-footer p.total span');
				var checkOutBtn = $('.checkout-modal .checkout-btn');
				
				$.ajax({
					type: "POST",
					url: getBaseURL()+'cart/update_qty',
					data: {'item': itemID, 'qty': newQty},
					beforeSend: function() {						
						$(productPrice).html(spinnerHtml);
						$(cartTotal).html(spinnerHtml);
						$(checkOutBtn).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> updating cart...');
						$(checkOutBtn).prop("disabled", true)
					},
					error:function( xhr, textStatus, errorThrown) {
						console.log(xhr.responseText);
					},
					success: function(data) {
						if(data.match(/php error/i)) {	
							console.log(data);
						}
						else
						{
							data = $.parseJSON(data);
							if(data.result == true)
							{		
								var newProductTotal = 'ush&nbsp;'+(data.cart_row.qty * data.cart_row.price).toLocaleString();
								var totalAmount = 'ush&nbsp;'+(data.total_amount).toLocaleString();
								$(inputQty).val(newQty);
								$(productPrice).html(newProductTotal);
								$(cartTotal).fadeOut('slow').html(totalAmount).show('slow');
								$(checkOutBtn).fadeOut('slow').html('Checkout').show('slow');
								$(checkOutBtn).prop("disabled", false)
							}
							else 'ERROR: Something went wrong. The item could not be removed.';
						}
												
					}
				});
			
			});

		$('#search-catalog').on('keyup', function(e){
			var searchTerm = $(this).val().trim();
			var formdata = {};

			if(!(searchTerm == null || searchTerm === '')){
				$('#search-results').fadeIn();
			} else {
				$('#search-results').fadeOut();
				return false;
			}

			formdata['search_term'] = searchTerm;

			$.ajax({
				type: "POST",
				url: getBaseURL()+'pages/search_catalog',
				data: formdata,
				beforeSend: function() {
				},
				error:function( xhr, textStatus, errorThrown) {
					console.log(xhr.responseText);
				},
				success: function(data) {
					if(data.match(/php error/i)) {
						console.log(data);
					}
					else
					{
						$('#search-results').html(data);
					}

				}
			});

		});

		$('select#region').on('change', function(){
			var townOptionsHTML = '<option selected value="">-Select Town-</option>';
			var thisRegion = $(this).val();
			for(var i=0; i<towns.length; i++)
				if(towns[i]['region_id'] == thisRegion) townOptionsHTML += '<option value="'+towns[i]['id']+'">'+towns[i]['title']+'</option>';

			$('select#select-town').html(townOptionsHTML);

		});

		$('select#select-town').on('change', function () {
			if (!isNullOrEmpty($(this).val())){
				$.ajax({
					type: "POST",
					url: getBaseURL()+'cart/delivery_fees',
					data: {'town': $(this).val()},
					beforeSend: function() {
						$('.delivery_fee span, .summary_delivery_fee span').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> calculating delivery fees...');
					},
					error:function( xhr, textStatus, errorThrown) {
						console.log(xhr.responseText);
					},
					success: function(data) {
						if(data.match(/php error/i)) {
							console.log(data);
						}
						else
						{
							data = $.parseJSON(data);
							if(data.delivery_fee)
							{
								$('.delivery_fee>span, .summary_delivery_fee>span').html('ush&nbsp;'+data.delivery_fee.toLocaleString());
								$('.summary_total_cost span, .order_total span').html('ush&nbsp;'+data.total_amount.toLocaleString());
							}
							else showFadingMessage('Something went wrong. Delivery charges could not be calculated.');
						}

					}
				});
			}
			else {
				$('.summary_delivery_fee, .delivery_fee').html('<span style="font-size: small">Calculated from selected town</span>');
			}

		});


		$('.product_category_pagination li>a').on('click', function () {
			$.ajax({
				type: "GET",
				url: $(location).attr("href") + '/page/' + $(this).html(),
				beforeSend: function () {
					$('.category_products').html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading data...');
				},
				error: function (xhr, textStatus, errorThrown) {
					console.log(xhr.responseText);
				},
				success: function (data) {
					if (data.match(/php error/i)) {
						console.log(data);
					} else {
						$('.category_products').html(data);
					}

				}
			});
		});



		$('input#mobile-money, input#cash-on-delivery, input#telephone-number').on('change', function(){

			if($('input#mobile-money').is(':checked')) {
				$('div#momo-phone-number').show();
				$('#place-order').text('PAY NOW');

			}else {
				$('div#momo-phone-number').hide();
				$('#place-order').text('PLACE 0RDER');
			}


			if(this.id == 'telephone-number'){
				$('input[name=momo_phone_number]').val($(this).val());
			} else {
				$('input#mobile-money').parent('div').css('border', 'none').css('border-bottom', '1px solid #dee2e6!important');
				$('input#cash-on-delivery').parent('div').css('border', 'none');
			}

		});

		var stickyOffset = $('.site-header').next('div.page-content').offset().top;
		var headerPlaceHolder = '';

		$(window).scroll(function(){
			var sticky = $('.site-header'),
				scroll = $(window).scrollTop();

			if (scroll >= stickyOffset){
				sticky.addClass('fixed-top');
				if(!$('#header-place-holder').length) {
					headerPlaceHolder = '<div id="header-place-holder" style="display: block; height: ' + stickyOffset + 'px"></div>';
					sticky.after(headerPlaceHolder);
				}
			}else {
				sticky.removeClass('fixed-top');
				$('#header-place-holder').remove();
			}
		});

		$('#label-terms-conditions a').click(function () {
			$('#privacy-policy-text').toggle();
		});

})(jQuery);